package br.com.sondait.eventos;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.mail.EmailConstants;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.dao.DAOHistoricosAntigos;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.dao.DAOTreinamento;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Aprovado;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Historico;
import br.com.sondait.model.HistoricosAntigos;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.Treinamento;
import br.com.sondait.util.Reader;

@Component // Para classificar a classe como componente
@EnableScheduling // Para habilitar o agendamento por meio desta anota��o
public class Notificacoes {

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	@Autowired
	@Qualifier("JPAHistorico")
	private DAOHistorico daoHistorico;

	@Autowired
	@Qualifier("JPAHistoricosAntigos")
	private DAOHistoricosAntigos daoHistoricoAntigo;

	@Autowired
	@Qualifier("JPATreinamento")
	private DAOTreinamento daoTreinamento;

	@Autowired
	@Qualifier("JPAAplicacao")
	private DAOAplicacao daoAplicacao;

	@Autowired
	private ServletContext servletContext;

	// Setando fuso hor�rio
	private static final String TIME_ZONE = "America/Sao_Paulo";

	// M�todo que envia o email avisando o funcion�rio da reciclagem
	@Scheduled(cron = "0 0 23 * * *", zone = TIME_ZONE)
	public void notificacaoReciclagem() throws EmailException {
		for (Programa programa : daoPrograma.programasReciclagem()) {
			for (Funcionario funcionario : programa.getFuncionarios()) {
				Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), programa.getId());

				Calendar prazoReciclagem = Calendar.getInstance();
				prazoReciclagem.setTime(historico.getDataConclusao());
				prazoReciclagem.add(Calendar.MONTH, programa.getPrazoReciclagem());

				Calendar doisDias = Calendar.getInstance();
				doisDias.setTime(historico.getDataConclusao());
				doisDias.add(Calendar.MONTH, programa.getPrazoReciclagem());
				doisDias.add(Calendar.DAY_OF_MONTH, -2);

				Calendar dataAtual = Calendar.getInstance();
				dataAtual.setTime(new Date());

				Calendar diasEmail = Calendar.getInstance();
				diasEmail.setTime(new Date());
				diasEmail.add(Calendar.DAY_OF_MONTH, 1);

				if (doisDias.getTime().after(dataAtual.getTime()) && doisDias.getTime().before(diasEmail.getTime())) {
					for (Treinamento treinamento : daoTreinamento.treinamentosReciclagem(programa.getId())) {
						Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(funcionario.getId(),
								treinamento.getId());

						// Altera as aplicacoes
						aplicacao.setFuncionario(funcionario);
						aplicacao.setTreinamento(treinamento);
						Calendar dataInicio = Calendar.getInstance();
						dataInicio.setTime(historico.getDataConclusao());
						dataInicio.add(Calendar.MONTH, treinamento.getPrograma().getPrazoReciclagem());
						aplicacao.setDataInicio(dataInicio.getTime());
						aplicacao.setAprovado(Aprovado.PENDENTE);
						daoAplicacao.alterar(aplicacao);
					}

					// Altera o historico Atual
					Calendar dataInicio1 = Calendar.getInstance();
					dataInicio1.setTime(historico.getDataConclusao());
					dataInicio1.add(Calendar.MONTH, programa.getPrazoReciclagem());
					historico.setDataInicio(dataInicio1.getTime());
					historico.setFuncionario(funcionario);
					Calendar dataConclusao = Calendar.getInstance();
					dataConclusao.setTime(historico.getDataInicio());
					dataConclusao.add(Calendar.DAY_OF_YEAR, programa.getDuracao());
					historico.setDataConclusao(dataConclusao.getTime());
					historico.setPrograma(programa);
					daoHistorico.alterar(historico);

					// Altera o historico antigo

					HistoricosAntigos antigos = daoHistoricoAntigo.historicoFuncionario(funcionario.getId(),
							programa.getId());

					if (antigos != null) {
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(new Date());
						antigos.setDataConclusao(historico.getDataConclusao());
						antigos.setPrograma(programa);
						daoHistoricoAntigo.alterar(antigos);

					} else {
						HistoricosAntigos novohistoricoAntigo = new HistoricosAntigos();
						novohistoricoAntigo.setFuncionario(funcionario);
						novohistoricoAntigo.setDataInicio(new Date());
						novohistoricoAntigo.setDataConclusao(historico.getDataConclusao());
						novohistoricoAntigo.setPrograma(programa);
						daoHistoricoAntigo.inserir(novohistoricoAntigo);
					}

					List<Treinamento> treinamentosPrograma = daoTreinamento.treinamentosReciclagem(programa.getId());
					String treinamentosNome = "";
					for (Treinamento treinamentos : treinamentosPrograma) {
						treinamentosNome += treinamentos.getConteudo().getNome() + ",";
					}
					final String treinamentoEmail = treinamentosNome;

					new Thread() {
						public void run() {
							try {
								HtmlEmail email = new HtmlEmail();
								email.setHostName("smtp.gmail.com");
								email.setAuthentication("mateustetel1999@gmail.com", "roseni12");
								email.setSmtpPort(587);
								email.setStartTLSEnabled(true);
								email.setStartTLSRequired(true);
								email.setFrom("sondaittreinamentos@gmail.com", "Sonda Treinamentos");
								email.addTo(historico.getFuncionario().getEmail(),
										historico.getFuncionario().getNome());
								email.setCharset(EmailConstants.UTF_8);

								String paginaEmail = Reader
										.readFile(servletContext.getRealPath("files/template_email/emailRecl.html"));
								String[] emailPart = paginaEmail.split("!");
								String logoId = email.embed(new File(
										servletContext.getRealPath("files/template_email/sondaitlogo-branco.png")));

								email.setHtmlMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
										+ antigos.getFuncionario().getNome().split(" ")[0] + emailPart[2]
										+ antigos.getPrograma().getNome() + emailPart[3] + treinamentoEmail
										+ emailPart[4]).setSubject("Sistema de Gest�o de Treinamento - Reciclagem");
								email.send();
							} catch (Exception e) {
								e.printStackTrace();
							}

						};
					}.start();
				}
			}

		}
	}

	// M�todo que envia o email avisando o funcion�rio de atraso
	@Scheduled(cron = "0 22 17 * * *", zone = TIME_ZONE)
	public void noticacaoAtraso() throws EmailException {

		new Thread() {
			// Mensagem que ser� usada no email
			String msg = "";

			HashMap<Long, String> operacoesAtraso = new HashMap<>();

			public void run() {
				try {

					// For para pegar todos os programas
					for (Programa programa : daoPrograma.buscarAtivos()) {

						// CRIA O EMAIL
						HtmlEmail email = new HtmlEmail();
						email.setHostName("smtp.gmail.com");

						// Autentica��o do email (usuario e
						// senha)
						email.setAuthentication("sondaittreinamentos@gmail.com", "S0nda_132");
						email.setSmtpPort(587);
						email.setStartTLSEnabled(true);
						email.setStartTLSRequired(true);
						email.setFrom("sondaittreinamentos@gmail.com", "Sonda Treinamentos ");
						email.setCharset(EmailConstants.UTF_8);

						// S� entra nesse if se o programa for de cargo ou
						// opera��o
						if (programa.getCoordenador() != null) {
							// Setando destinat�rio
							email.addTo(programa.getCoordenador().getEmail(), programa.getCoordenador().getNome());

							int atrasados = 0;
							for (Historico historico : daoHistorico.historicosPendentesPrograma(programa.getId())) {
								try {

									HtmlEmail emailIndividual = new HtmlEmail();
									emailIndividual.setHostName("smtp.gmail.com");

									// Autentica��o do email (usuario e
									// senha)
									emailIndividual.setAuthentication("sondaittreinamentos@gmail.com", "S0nda_132");
									emailIndividual.setSmtpPort(587);
									emailIndividual.setStartTLSEnabled(true);
									emailIndividual.setStartTLSRequired(true);
									emailIndividual.setFrom("sondaittreinamentos@gmail.com", "Sonda ");
									emailIndividual.setCharset(EmailConstants.UTF_8);

									String paginaEmail = Reader.readFile(
											servletContext.getRealPath("files/template_email/emailAtraso.html"));
									String[] emailPart = paginaEmail.split("!");
									String logoId = emailIndividual.embed(new File(
											servletContext.getRealPath("files/template_email/sondaitlogo-branco.png")));

									// Setando destinat�rio

									emailIndividual.addTo(historico.getFuncionario().getEmail(),
											historico.getFuncionario().getNome());

									// Data de Notificacao
									Calendar dataNotificacao = Calendar.getInstance();
									dataNotificacao.setTime(historico.getDataInicio());
									dataNotificacao.add(Calendar.DAY_OF_YEAR,
											programa.getDuracao() - programa.getPrazoNotificacao());

									// Data de hoje
									Calendar dataAtual = Calendar.getInstance();
									dataAtual.setTime(new Date());

									// Dia do ultimo email
									Calendar diasEmail = Calendar.getInstance();
									diasEmail.setTime(dataNotificacao.getTime());
									diasEmail.add(Calendar.DAY_OF_MONTH, 2);

									if (dataAtual.getTime().after(dataNotificacao.getTime())
											&& dataAtual.getTime().before(diasEmail.getTime())) {

										// Setando msg
										emailIndividual.setHtmlMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
												+ historico.getFuncionario().getNome().split(" ")[0] + emailPart[2]
												+ historico.getPrograma().getNome() + emailPart[3])
												.setSubject("Sistema de Gest�o de Treinamento - Atraso");

										// Enviando email individuaL
										emailIndividual.send();
									}
								} catch (EmailException e) {
									e.printStackTrace();
								}

								// Data de Notificacao
								Calendar dataNotificacao = Calendar.getInstance();
								dataNotificacao.setTime(historico.getDataInicio());
								dataNotificacao.add(Calendar.DAY_OF_YEAR,
										programa.getDuracao() - programa.getPrazoNotificacao());

								// Data de hoje
								Calendar dataAtual = Calendar.getInstance();
								dataAtual.setTime(new Date());

								// Dia do ultimo email
								Calendar diasEmail = Calendar.getInstance();
								diasEmail.setTime(dataNotificacao.getTime());
								diasEmail.add(Calendar.DAY_OF_MONTH, 2);

								if (dataAtual.getTime().after(dataNotificacao.getTime())
										&& dataAtual.getTime().before(diasEmail.getTime())) {
									// Teste para saber se ele est� atrasado
									msg += historico.getFuncionario().getNome() + ", ";
									atrasados++;
								}
							}

							if (atrasados > 0) {
								String paginaEmail = Reader.readFile(
										servletContext.getRealPath("files/template_email/emailAtrasoCoord.html"));
								String[] emailPart = paginaEmail.split("!");
								String logoId = email.embed(new File(
										servletContext.getRealPath("files/template_email/sondaitlogo-branco.png")));

								// Setando msg
								email.setMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
										+ programa.getCoordenador().getNome().split(" ")[0] + emailPart[2] + msg
										+ emailPart[3] + programa.getNome() + emailPart[4])
										.setSubject("Sistema de Gest�o de Treinamento - Atraso");

								// Enviando o email
								email.send();
							}

						} else if (programa.getCargo() == null && programa.getOperacao() == null) {
							Operacao operacaoComparar = new Operacao();
							for (Historico historico : daoHistorico.historicosPendentesPrograma(programa.getId())) {

								operacaoComparar = daoOperacao
										.operacoesFuncionarioParticipa(historico.getFuncionario().getId());

								String idFunc = null;

								if (operacaoComparar != null) {
									if (operacoesAtraso.containsKey(operacaoComparar.getId())) {
										idFunc = operacoesAtraso.get(operacaoComparar.getId());
										idFunc += historico.getFuncionario().getId() + ";";
									} else {
										idFunc = historico.getFuncionario().getId().toString() + ";";
									}
									operacoesAtraso.put(operacaoComparar.getId(), idFunc);
								}

								try {

									HtmlEmail emailIndividual = new HtmlEmail();
									emailIndividual.setHostName("smtp.gmail.com");

									// Autentica��o do email (usuario e
									// senha)
									emailIndividual.setAuthentication("sondaittreinamentos@gmail.com", "S0nda_132");
									emailIndividual.setSmtpPort(587);
									emailIndividual.setStartTLSEnabled(true);
									emailIndividual.setStartTLSRequired(true);
									emailIndividual.setFrom("sondaittreinamentos@gmail.com", "Sonda ");
									emailIndividual.setCharset(EmailConstants.UTF_8);

									String paginaEmail = Reader.readFile(
											servletContext.getRealPath("files/template_email/emailAtraso.html"));
									String[] emailPart = paginaEmail.split("!");
									String logoId = emailIndividual.embed(new File(
											servletContext.getRealPath("files/template_email/sondaitlogo-branco.png")));

									// Setando destinat�rio
									emailIndividual
											.addTo(historico.getFuncionario().getEmail(),
													historico.getFuncionario().getNome())
											.setSubject("Sistema de Gest�o de Treinamento - Atraso");

									// Data de Notificacao
									Calendar dataNotificacao = Calendar.getInstance();
									dataNotificacao.setTime(historico.getDataInicio());
									dataNotificacao.add(Calendar.DAY_OF_YEAR,
											programa.getDuracao() - programa.getPrazoNotificacao());

									// Data de hoje
									Calendar dataAtual = Calendar.getInstance();
									dataAtual.setTime(new Date());

									// Dia do ultimo email
									Calendar diasEmail = Calendar.getInstance();
									diasEmail.setTime(dataNotificacao.getTime());
									diasEmail.add(Calendar.DAY_OF_MONTH, 2);

									if (dataAtual.getTime().after(dataNotificacao.getTime())
											&& dataAtual.getTime().before(diasEmail.getTime())) {

										// Setando mensagem no email para o
										// funcion�rio
										emailIndividual.setHtmlMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
												+ historico.getFuncionario().getNome().split(" ")[0] + emailPart[2]
												+ historico.getPrograma().getNome() + emailPart[3])
												.setSubject("Sistema de Gest�o de Treinamento - Atraso");

										// Enviando o email para o funcion�rio
										emailIndividual.send();
									}

								} catch (EmailException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
							// CRIA O EMAIL
							HtmlEmail emailCoordenador = new HtmlEmail();
							operacoesAtraso.forEach((k, v) -> {

								Long idOperacao = k;

								Operacao operacao = daoOperacao.buscar(idOperacao);

								try {
									emailCoordenador.setHostName("smtp.gmail.com");

									// Autentica��o do email (usuario e
									// senha)
									emailCoordenador.setAuthentication("sondaittreinamentos@gmail.com", "S0nda_132");
									emailCoordenador.setSmtpPort(587);
									emailCoordenador.setStartTLSEnabled(true);
									emailCoordenador.setStartTLSRequired(true);
									emailCoordenador.setFrom("sondaittreinamentos@gmail.com", "Sonda ");
									emailCoordenador.setCharset(EmailConstants.UTF_8);

									// Setando destinat�rio
									if (operacao.getCoordenador() != null) {
										emailCoordenador.addTo(operacao.getCoordenador().getEmail(),
												operacao.getCoordenador().getNome());
									}
									return;

								} catch (EmailException e) {
									e.printStackTrace();
								}
								int atrasados = 0;
								String[] idFuncionarios = v.split(";");
								for (String id : idFuncionarios) {
									Historico hist;

									Object[] historicos = daoHistorico.historicosPendentesPrograma(programa.getId())
											.stream().filter(h -> h.getFuncionario().getId() == Long.parseLong(id))
											.toArray();

									if (historicos.length > 0) {
										hist = (Historico) historicos[0];

										// Data de Notificacaoo
										Calendar dataNotificacao = Calendar.getInstance();
										dataNotificacao.setTime(hist.getDataInicio());
										dataNotificacao.add(Calendar.DAY_OF_YEAR,
												programa.getDuracao() - programa.getPrazoNotificacao());

										// Data de hoje
										Calendar dataAtual = Calendar.getInstance();
										dataAtual.setTime(new Date());

										// Dia do ultimo email
										Calendar diasEmail = Calendar.getInstance();
										diasEmail.setTime(dataNotificacao.getTime());
										diasEmail.add(Calendar.DAY_OF_MONTH, 2);

										if (dataAtual.getTime().after(dataNotificacao.getTime())
												&& dataAtual.getTime().before(diasEmail.getTime())) {
											msg += hist.getFuncionario().getNome() + ", ";
											atrasados++;
										}
									}

								}

								// Setando msg
								try {

									if (atrasados > 0) {
										String paginaEmail;
										try {
											paginaEmail = Reader.readFile(servletContext
													.getRealPath("files/template_email/emailAtrasoCoord.html"));
											String[] emailPart = paginaEmail.split("!");
											String logoId = email.embed(new File(servletContext
													.getRealPath("files/template_email/sondaitlogo-branco.png")));

											// Setando msg
											email.setMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
													+ operacao.getCoordenador().getNome().split(" ")[0] + emailPart[2]
													+ msg + emailPart[3] + programa.getNome() + emailPart[4])
													.setSubject("Sistema de Gest�o de Treinamento - Atraso");

											// Enviando o email
											emailCoordenador.send();
										} catch (IOException e) {
											e.printStackTrace();
										}

									}
								} catch (EmailException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							});
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			};
		}.start();
	}

}