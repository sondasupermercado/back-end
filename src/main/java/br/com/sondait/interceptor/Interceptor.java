package br.com.sondait.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.controller.ControllerFuncionario;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.model.Tipo;

public class Interceptor extends HandlerInterceptorAdapter {

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if (!(handler instanceof HandlerMethod)) {
			return true;
		}

		if (request.getMethod().equals("OPTIONS")) {
			return true;
		}
		HandlerMethod methodInfo = (HandlerMethod) handler;

		if (methodInfo.getMethod().getName().equals("logar") || methodInfo.getMethod().getName().equals("esqueciSenha")
				|| methodInfo.getMethod().getName().equals("redefinirSenha")) {
			System.out.println("ESTOU NO INTERCEPTOR");
			return true;
		} else {
			String token = null;
			try {
				// Pegando informações do token que veio do front
				token = request.getHeader("Authorization");

				// Criando um verificador e trazendo a senha do controller para
				// descriptografar o token
				JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

				// Passando as informações do token para um map para fazer
				// compações
				Map<String, Object> claims = verifier.verify(token);
				request.setAttribute("claims", claims);
				String tipoDeUsuario = claims.get("tipo").toString();
				Tipo tipoDoFuncionario = Tipo.valueOf(tipoDeUsuario);

				if (request.getRequestURI().contains("cargo") || request.getRequestURI().contains("conteudo")) {

					if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
						return true;
					} else {
						response.setStatus(HttpStatus.UNAUTHORIZED.value());
						return true;
					}
				} else if (request.getRequestURI().contains("chamada") || request.getRequestURI().contains("programa")
						|| request.getRequestURI().contains("operacao") || request.getRequestURI().contains("avaliacao")
						|| request.getRequestURI().contains("aplicacao")
						|| request.getRequestURI().contains("treinamento")
						|| request.getRequestURI().contains("funcionario")
						|| request.getRequestURI().contains("historico")
						|| request.getRequestURI().contains("historicosAntigos")
						|| request.getRequestURI().contains("turma")) {
					return true;
				}

			} catch (JWTExpiredException e) {
				response.setStatus(565);
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				if (token == null) {
					response.setStatus(HttpStatus.UNAUTHORIZED.value());
				} else {
					response.setStatus(HttpStatus.FORBIDDEN.value());
				}
				return false;
			}

		}

		return false;
	}
}
