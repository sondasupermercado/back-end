package br.com.sondait.controller;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.dao.DAOCargo;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.model.Cargo;

@CrossOrigin
@RequestMapping("/cargo")
@RestController
public class ControllerCargo {

	@Autowired
	@Qualifier("JPACargo")
	private DAOCargo daoCargo;

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@Autowired
	@Qualifier("JPAAplicacao")
	private DAOAplicacao daoAplicacao;

	@Autowired
	@Qualifier("JPAHistorico")
	private DAOHistorico daoHistorico;


	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Cargo> criarCargo(@RequestBody Cargo cargo, HttpServletResponse response) {

		if (response.getStatus() == 565) {
			return ResponseEntity.status(565).body(null);
		} else if (response.getStatus() == 401) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		try {

			for (Cargo cargoComparar : daoCargo.buscarTodos()) {
				if (cargo.getNome().trim().toLowerCase().equals(cargoComparar.getNome().trim().toLowerCase())
						&& cargoComparar.isAtivo() == false) {
					cargo.setId(cargoComparar.getId());
					daoCargo.alterar(cargo);
					return ResponseEntity.status(HttpStatus.OK).body(cargo);
				} else if (cargo.getNome().trim().toLowerCase().equals(cargoComparar.getNome().trim().toLowerCase())
						&& cargoComparar.isAtivo() == true) {
					return ResponseEntity.status(560).body(null);
				}
			}

			daoCargo.inserir(cargo);
			return ResponseEntity.created(URI.create("/cargo/" + cargo.getId())).body(cargo);
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Cargo>(HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			return new ResponseEntity<Cargo>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Cargo>> buscarCargos(HttpServletResponse response) {

		if (response.getStatus() == 565) {
			return ResponseEntity.status(565).body(null);
		} else if (response.getStatus() == 401) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		try {
			return ResponseEntity.ok(daoCargo.buscarAtivos());
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Cargo>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<List<Cargo>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/desativos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Cargo>> buscarDesativos(HttpServletResponse response) {

		if (response.getStatus() == 565) {
			return ResponseEntity.status(565).body(null);
		} else if (response.getStatus() == 401) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		try {
			return ResponseEntity.ok(daoCargo.buscarDesativos());
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Cargo>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<List<Cargo>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// Cargo Desativar
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> desativar(@PathVariable Long id, HttpServletResponse response) {

		if (response.getStatus() == 565) {
			return ResponseEntity.status(565).body(null);
		} else if (response.getStatus() == 401) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		try {
			if (daoFuncionario.funcioriosCargo(id).size() == 0 && daoPrograma.ProgramasDoCargo(id).size() == 0) {
				daoCargo.deletar(id);
				return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
			} else {

				// Codigo quando o Cargo j� tem funcionarios relacionados
				return ResponseEntity.status(566).body(null);
			}

		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/ativar/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> ativar(@PathVariable Long id, HttpServletResponse response) {

		if (response.getStatus() == 565) {
			return ResponseEntity.status(565).body(null);
		} else if (response.getStatus() == 401) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		try {
			daoCargo.ativar(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Cargo> buscarCargo(@PathVariable Long id, HttpServletResponse response) {

		if (response.getStatus() == 565) {
			return ResponseEntity.status(565).body(null);
		} else if (response.getStatus() == 401) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		try {
			return ResponseEntity.ok(daoCargo.buscar(id));
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Cargo>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Cargo>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Cargo> alterarCargo(@RequestBody Cargo cargoAlterar, @PathVariable Long id,
			HttpServletResponse response) {

		if (response.getStatus() == 565) {
			return ResponseEntity.status(565).body(null);
		} else if (response.getStatus() == 401) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}

		try {
			cargoAlterar.setId(id);
			daoCargo.alterar(cargoAlterar);
			return ResponseEntity.status(HttpStatus.OK).body(cargoAlterar);
		} catch (DataIntegrityViolationException e) {
			return ResponseEntity.status(564).body(null);
		} catch (Exception e) {
			return new ResponseEntity<Cargo>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
