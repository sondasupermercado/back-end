package br.com.sondait.controller;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.dao.DAOConteudo;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.dao.DAOTreinamento;
import br.com.sondait.dao.DAOTurma;
import br.com.sondait.model.Conteudo;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.Tipo;
import br.com.sondait.model.Treinamento;
import br.com.sondait.model.Turma;

@CrossOrigin // Serve para fazer um acesso cross em dois servidores(servidor
				// back e servido front por exemplo)
@RequestMapping("/treinamento")
@RestController
public class ControllerTreinamento {

	@Autowired
	@Qualifier("JPATreinamento")
	private DAOTreinamento daoTreinamento;

	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@Autowired
	@Qualifier("JPAConteudo")
	private DAOConteudo daoConteudo;

	@Autowired
	@Qualifier("JPATurma")
	private DAOTurma daoTurma;

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	@Autowired
	private ServletContext servletContext;

	String token = null;

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");
			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			return null;
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Treinamento> criarTreinamento(@RequestBody Treinamento treinamento,
			HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Lista para saber se ele coordena algum
			Programa programa = daoPrograma.buscar(treinamento.getPrograma().getId());
			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| programa.getCoordenador() != null && programa.getCoordenador().getId() == idDoFuncionario
					|| programa.getCoordenador() == null && !operacoesCoordena.isEmpty()) {
				Conteudo conteudo = daoConteudo.buscar(treinamento.getConteudo().getId());

				if (conteudo.isPresencial() && treinamento.getTurmas() != null) {
					List<Turma> turmas = new ArrayList<>();
					for (Turma turma : treinamento.getTurmas()) {
						Turma t = daoTurma.buscar(turma.getId());
						turmas.add(t);
					}
					treinamento.setTurmas(new HashSet<>(turmas));

				}

				treinamento.setPrograma(programa);
				treinamento.setConteudo(conteudo);
				daoTreinamento.inserir(treinamento);
				return ResponseEntity.created(URI.create("/treinamento/" + treinamento.getId())).body(treinamento);

			} else {
				return new ResponseEntity<Treinamento>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Treinamento>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Treinamento>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Treinamento>> buscarTreinamentos(HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || !operacoesCoordena.isEmpty()) {
				return ResponseEntity.ok(daoTreinamento.buscarAtivos());
			} else if (!operacoesCoordena.isEmpty()) {
				List<Treinamento> treinamentos = new ArrayList<>();
				for (Treinamento treinamento : daoTreinamento.buscarAtivos()) {
					if (treinamento.getPrograma().getCoordenador() == null
							|| treinamento.getPrograma().getCoordenador().getId() == idDoFuncionario) {
						treinamentos.add(treinamento);
					} else {
						continue;
					}
				}
				return ResponseEntity.status(HttpStatus.OK).body(treinamentos);
			} else {
				return new ResponseEntity<List<Treinamento>>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<List<Treinamento>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Treinamento>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> desativar(@PathVariable Long id, HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || !operacoesCoordena.isEmpty()) {

				daoTreinamento.deletar(id);
				return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
			} else {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Treinamento> buscarTreinamento(@PathVariable Long id, HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			Treinamento treinamento = daoTreinamento.buscar(id);

			if (treinamento.getPrograma().isAtivo()) {
				
				if (tipoDoFuncionario == Tipo.ADMINISTRADOR
						|| treinamento.getPrograma().getCoordenador() != null
								&& treinamento.getPrograma().getCoordenador().getId() == idDoFuncionario
						|| treinamento.getPrograma().getCoordenador() == null && !operacoesCoordena.isEmpty()) {
					File chamada = new File(servletContext.getRealPath("/files/chamada"));

					Set<String> chamadas = new HashSet<String>();
					if (chamada.listFiles() != null) {
						for (File c : chamada.listFiles()) {
							for (Turma t : treinamento.getTurmas()) {
								System.out.println("ESTAMOS AQUI >>>>>>>>>>>>>>>>>>>>>>>>>>> " + t.getNome());
								System.out.println("ESTAMOS AQUI >>>>>>>>>>>>>>>>>>>>>>>>>>> " + t.getId());
								System.out.println("ESTAMOS AQUI >>>>>>>>>>>>>>>>>>>>>>>>>>> " + c.getName());
								System.out.println("ESTAMOS AQUI >>>>>>>>>>>>>>>>>>>>>>>>>>> " + c.getName().split("-")[1].replace(".pdf", ""));
								if (c.getName().split("-")[1].replace(".pdf", "").equals(String.valueOf(t.getId()))) {
									chamadas.add(File.separator + "files/chamada/" + c.getName());
								}
							}
						}
						treinamento.setChamadas(chamadas);
						return ResponseEntity.status(HttpStatus.OK).body(treinamento);
					} else {
						return ResponseEntity.status(HttpStatus.OK).body(treinamento);
					}
				} else {
					return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
				}

				
			} else {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
			}
			
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Treinamento>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Treinamento>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Treinamento> alterarTreinamento(@PathVariable Long id, @RequestBody Treinamento treinamento,
			HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| treinamento.getPrograma().getCoordenador() != null
							&& treinamento.getPrograma().getCoordenador().getId() == idDoFuncionario
					|| treinamento.getPrograma().getCoordenador() == null && !operacoesCoordena.isEmpty()) {
				treinamento.getId();
				daoTreinamento.alterar(treinamento);
				return ResponseEntity.status(HttpStatus.OK).body(treinamento);

			} else {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Treinamento>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Treinamento>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
