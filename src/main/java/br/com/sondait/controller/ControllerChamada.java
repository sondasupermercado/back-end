package br.com.sondait.controller;

import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.dao.DAOChamada;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOTurma;
import br.com.sondait.model.Chamada;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Tipo;
import br.com.sondait.model.Turma;

@CrossOrigin // Serve para fazer um acesso cross em dois servidores(servidor
				// back e servido front por exemplo)
@RequestMapping("/chamada")
@RestController
public class ControllerChamada {

	@Autowired
	@Qualifier("JPAChamada")
	private DAOChamada daoChamada;

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPATurma")
	private DAOTurma daoTurma;

	private String token;

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");

			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			return null;
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Chamada> criarChamada(@RequestBody Chamada chamada, HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| idDoFuncionario == chamada.getTurma().getTreinamento().getPrograma().getCoordenador().getId()) {
				Funcionario funcionario = daoFuncionario.buscar(chamada.getFuncionario().getId());
				Turma turma = daoTurma.buscar(chamada.getTurma().getId());

				chamada.setFuncionario(funcionario);
				chamada.setTurma(turma);
				daoChamada.inserir(chamada);
				return ResponseEntity.created(URI.create("/chamada/" + chamada.getId())).body(chamada);
			} else {
				return new ResponseEntity<Chamada>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Chamada>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Chamada>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Chamada>> buscarChamadas(HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				return ResponseEntity.ok(daoChamada.buscarAtivos());
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Chamada>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<List<Chamada>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Chamada> buscarChamada(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Busca avalia��o
			Chamada chamadaBuscada = daoChamada.buscar(id);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || idDoFuncionario == chamadaBuscada.getTurma().getTreinamento()
					.getPrograma().getCoordenador().getId()) {
				return ResponseEntity.ok(chamadaBuscada);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Chamada> alterarChamada(@PathVariable Long id, @RequestBody Chamada chamada,
			HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Chamada chamadaBuscada = daoChamada.buscar(chamada.getId());

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || idDoFuncionario == chamadaBuscada.getTurma().getTreinamento()
					.getPrograma().getCoordenador().getId()) {
				Funcionario funcionario = daoFuncionario.buscar(chamadaBuscada.getFuncionario().getId());
				Turma turma = daoTurma.buscar(chamadaBuscada.getTurma().getId());

				chamadaBuscada.setFuncionario(funcionario);
				chamadaBuscada.setTurma(turma);
				daoChamada.alterar(chamadaBuscada);
				return ResponseEntity.status(HttpStatus.OK).body(chamadaBuscada);
			} else {
				return new ResponseEntity<Chamada>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Chamada>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Chamada>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
