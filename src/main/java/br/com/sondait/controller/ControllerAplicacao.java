package br.com.sondait.controller;

import java.beans.PropertyEditorSupport;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOTreinamento;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Treinamento;

@CrossOrigin // Serve para fazer um acesso cross em dois servidores(servidor
				// back e servido front por exemplo)
@RequestMapping("/aplicacao")
@RestController
public class ControllerAplicacao {

	@Autowired
	@Qualifier("JPAAplicacao")
	private DAOAplicacao daoAplicacao;

	@Autowired
	@Qualifier("JPATreinamento")
	private DAOTreinamento daoTreinamento;

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	@InitBinder
	public void binder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			public void setAsText(String value) {
				if (value != null) {
					try {
						setValue(new SimpleDateFormat("yyyy-MM-dd").parse(value));
					} catch (ParseException e) {
						setValue(null);
						e.printStackTrace();
					}
				}
			}

			public String getAsText() {
				if (getValue() != null) {
					return new SimpleDateFormat("yyyy-MM-dd").format((Date) getValue());
				}
				return null;
			}
		});
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Aplicacao> criarAplicacao(@RequestBody Aplicacao aplicacao) {
		try {
			Treinamento treinamento = daoTreinamento.buscar(aplicacao.getTreinamento().getId());
			Funcionario funcionario = daoFuncionario.buscar(aplicacao.getFuncionario().getId());

			aplicacao.setTreinamento(treinamento);
			aplicacao.setFuncionario(funcionario);
			daoAplicacao.inserir(aplicacao);
			return ResponseEntity.created(URI.create("/aplicacao/" + aplicacao.getId())).body(aplicacao);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Aplicacao>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Aplicacao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Aplicacao> buscarAplicacoes() {
		return daoAplicacao.buscarAtivos();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Aplicacao buscarAplicacao(@PathVariable Long id) {
		Aplicacao aplicacao = daoAplicacao.buscar(id);

		if (aplicacao != null)
			aplicacao.getFuncionario()
					.setOperacao(daoOperacao.operacoesFuncionarioParticipa(aplicacao.getFuncionario().getId()));

		return aplicacao;
	}

	@RequestMapping(value = "/{idFuncionario}/funcionario", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Aplicacao> buscarAplicacaoDoFuncionario(@PathVariable Long idFuncionario) {
		return daoAplicacao.buscarTodos(idFuncionario);
	}

	@RequestMapping(value = "/{idFuncionario}/{idPrograma}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Aplicacao buscarAplicacaoDoFuncionario(@PathVariable Long idFuncionario, @PathVariable Long idPrograma) {
		return daoAplicacao.aplicacaoPorProgramaEFuncionarios(idPrograma, idFuncionario);
	}
}
