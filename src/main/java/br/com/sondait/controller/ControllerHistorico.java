package br.com.sondait.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.model.Historico;

@RestController
@CrossOrigin
@RequestMapping("/historico")
public class ControllerHistorico {

	@Autowired
	@Qualifier("JPAHistorico")
	private DAOHistorico daoHistorico;
	
	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Historico> listarHistoricos(@PathVariable Long id) {
		return daoHistorico.historico(id);
	}	
}
