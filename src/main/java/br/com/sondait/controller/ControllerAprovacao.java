package br.com.sondait.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.dao.DAOAprovacao;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.Tipo;
import br.com.sondait.util.Aprovacao;

@RequestMapping("/aplicar")
@RestController
public class ControllerAprovacao {

	@Autowired
	@Qualifier("JPAAprovacao")
	private DAOAprovacao daoAprovacao;

	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;
	
	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	private String token = null;

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");
			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			return null;
		}
	}

	@RequestMapping(value = "/dataInicio/{idPrograma}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Aprovacao>> aplicacoesDataInicio(@PathVariable Long idPrograma,
			HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			List<Operacao> operacaoCoordena = daoOperacao.funcionarioCoordena(idDoFuncionario);
			
			Programa prog = daoPrograma.buscar(idPrograma);
			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || (prog.getCoordenador() == null && !operacaoCoordena.isEmpty())
					|| prog.getCoordenador().getId() == idDoFuncionario) {

				Programa programa = daoPrograma.buscar(idPrograma);
				List<Aprovacao> listaAprovacao = new ArrayList<>();

				for (Funcionario funcionario : programa.getFuncionarios()) {
					Aprovacao aprovacao = daoAprovacao.registroAprovacao(funcionario.getId(), idPrograma);
					listaAprovacao.add(aprovacao);
				}
				return ResponseEntity.status(HttpStatus.OK).body(listaAprovacao);
			} else {
				return new ResponseEntity<List<Aprovacao>>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Aprovacao>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Aprovacao>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
