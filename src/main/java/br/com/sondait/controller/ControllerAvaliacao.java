package br.com.sondait.controller;

import java.beans.PropertyEditorSupport;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.dao.DAOAvaliacao;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.model.Avaliacao;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.Tipo;

@CrossOrigin // Serve para fazer um acesso cross em dois servidores(servidor
				// back e servido front por exemplo)
@RequestMapping("/avaliacao")
@RestController
public class ControllerAvaliacao {

	@Autowired
	@Qualifier("JPAAvaliacao")
	private DAOAvaliacao daoAvaliacao;

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@Autowired
	@Qualifier("JPAAplicacao")
	private DAOAplicacao daoAplicacao;

	@Autowired
	@Qualifier("JPAHistorico")
	private DAOHistorico daoHistorico;

	private String token = null;

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");
			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			return null;
		}
	}

	@InitBinder
	public void binder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			public void setAsText(String value) {
				if (value != null) {
					try {
						setValue(new SimpleDateFormat("yyyy-MM-dd").parse(value));
					} catch (ParseException e) {
						setValue(null);
						e.printStackTrace();
					}
				}
			}

			public String getAsText() {
				if (getValue() != null) {
					return new SimpleDateFormat("yyyy-MM-dd").format((Date) getValue());
				}
				return null;
			}
		});
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Avaliacao> criarAvaliacao(@RequestBody Avaliacao avaliacao, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			List<Operacao> operacoesCoordenadas = daoOperacao.funcionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| avaliacao.getPrograma().getCoordenador() != null
							&& idDoFuncionario == avaliacao.getPrograma().getCoordenador().getId()
					|| !operacoesCoordenadas.isEmpty()) {

				Funcionario funcionarioAvaliado = daoFuncionario.buscar(avaliacao.getFuncionarioAvaliado().getId());
				Funcionario responsavelAvaliacao = daoFuncionario.buscar(avaliacao.getResponsavelAvaliacao().getId());
				Programa programa = daoPrograma.buscar(avaliacao.getPrograma().getId());

				avaliacao.setFuncionarioAvaliado(funcionarioAvaliado);
				avaliacao.setResponsavelAvaliacao(responsavelAvaliacao);
				avaliacao.setPrograma(programa);
				daoAvaliacao.inserir(avaliacao);

				return ResponseEntity.created(URI.create("/avaliacao" + avaliacao.getId())).body(avaliacao);
			} else {
				return new ResponseEntity<Avaliacao>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Avaliacao>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Avaliacao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * 
	 * CAGUEI ESSE M�TODO
	 * 
	 */
	@RequestMapping(value = "/eficacia/{idPrograma}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Avaliacao>> buscarAvaliacoes(@PathVariable Long idPrograma, HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Programa programa = daoPrograma.buscar(idPrograma);
			List<Operacao> operacaoCoordena = daoOperacao.funcionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| (programa.getCoordenador() != null && programa.getCoordenador().getId() == idDoFuncionario)) {
				return ResponseEntity.ok(daoAvaliacao.buscarAtivosPorPrograma(idPrograma));

			} else if (programa.getCoordenador() == null && !operacaoCoordena.isEmpty()) {
				List<Avaliacao> listaDeAvaliacoes = new ArrayList<>();
				for (Operacao operacao : operacaoCoordena) {
					for (Funcionario funcionario : operacao.getFuncionarios()) {
						listaDeAvaliacoes.addAll(daoAvaliacao.buscarAtivosPorProgramasEFuncionario(idPrograma, funcionario.getId()));
					}
				}
				return ResponseEntity.ok(listaDeAvaliacoes);
			} else {
				return new ResponseEntity<List<Avaliacao>>(HttpStatus.UNAUTHORIZED);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Avaliacao> buscarAvaliacao(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Busca avalia��o
			Avaliacao avaliacaoBuscada = daoAvaliacao.buscar(id);

			avaliacaoBuscada.getFuncionarioAvaliado().setOperacao(
					daoOperacao.operacoesFuncionarioParticipa(avaliacaoBuscada.getFuncionarioAvaliado().getId()));
			List<Operacao> operacoesCoordenadas = daoOperacao.funcionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || avaliacaoBuscada.getPrograma().getCoordenador() != null
					&& idDoFuncionario == avaliacaoBuscada.getPrograma().getCoordenador().getId() || avaliacaoBuscada.getPrograma().getCoordenador() == null && !operacoesCoordenadas.isEmpty()) {
				return ResponseEntity.ok(avaliacaoBuscada);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Avaliacao> alterarAvaliacao(@PathVariable Long id, @RequestBody Avaliacao avaliacao,
			HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			avaliacao.setId(id);

			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || avaliacao.getPrograma().getCoordenador() == null
					|| idDoFuncionario == avaliacao.getPrograma().getCoordenador().getId()) {

				Funcionario funcionarioAvaliado = daoFuncionario.buscar(avaliacao.getFuncionarioAvaliado().getId());
				Funcionario responsavelAvaliacao = daoFuncionario.buscar(avaliacao.getResponsavelAvaliacao().getId());
				Programa programa = daoPrograma.buscar(avaliacao.getPrograma().getId());

				avaliacao.setFuncionarioAvaliado(funcionarioAvaliado);
				avaliacao.setResponsavelAvaliacao(responsavelAvaliacao);
				avaliacao.setPrograma(programa);

				daoAvaliacao.alterar(avaliacao);
				return ResponseEntity.status(HttpStatus.OK).body(avaliacao);
			} else if (!operacoesCoordena.isEmpty()) {
				for (Operacao operacao : operacoesCoordena) {
					for (Funcionario funcionario : operacao.getFuncionarios()) {

						Funcionario funcionarioAvaliado = daoFuncionario
								.buscar(avaliacao.getFuncionarioAvaliado().getId());
						if (funcionario.getId() != funcionarioAvaliado.getId()) {
							continue;
						}
						Funcionario responsavelAvaliacao = daoFuncionario
								.buscar(avaliacao.getResponsavelAvaliacao().getId());
						Programa programa = daoPrograma.buscar(avaliacao.getPrograma().getId());

						avaliacao.setFuncionarioAvaliado(funcionarioAvaliado);
						avaliacao.setResponsavelAvaliacao(responsavelAvaliacao);
						avaliacao.setPrograma(programa);

						daoAvaliacao.alterar(avaliacao);
						return ResponseEntity.status(HttpStatus.OK).body(avaliacao);
					}
				}
				return null;
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Avaliacao>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Avaliacao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
