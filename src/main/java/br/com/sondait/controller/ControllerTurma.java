package br.com.sondait.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.dao.DAOChamada;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.dao.DAOTreinamento;
import br.com.sondait.dao.DAOTurma;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.Tipo;
import br.com.sondait.model.Treinamento;
import br.com.sondait.model.Turma;

@CrossOrigin // Serve para fazer um acesso cross em dois servidores(servidor
				// back e servido front por exemplo)
@RequestMapping("/turma")
@RestController
public class ControllerTurma {

	@Autowired
	@Qualifier("JPATurma")
	private DAOTurma daoTurma;

	@Autowired
	@Qualifier("JPAChamada")
	private DAOChamada daoChamada;

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	@Autowired
	@Qualifier("JPATreinamento")
	private DAOTreinamento daoTreinamento;
	
	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@Autowired
	private ServletContext servletContext;

	private String token;

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");

			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			return null;
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Turma> criarTurma(@RequestBody Turma turma, HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Treinamento treinamento = daoTreinamento.buscar(turma.getTreinamento().getId());

			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| treinamento.getPrograma().getCoordenador() != null
							&& treinamento.getPrograma().getCoordenador().getId() == idDoFuncionario
					|| treinamento.getPrograma().getCoordenador() == null && !operacoesCoordena.isEmpty()) {
				daoTurma.inserir(turma);
				return ResponseEntity.created(URI.create("/turma/" + turma.getId())).body(turma);
			} else {
				return new ResponseEntity<Turma>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Turma>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Turma>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Turma>> buscarTurmas(HttpServletRequest request) {
		String[] descripitacaoToken = interceptor(request);

		// Verificando se o token est� expirado
		if (descripitacaoToken[0].equals("565")) {
			return ResponseEntity.status(565).body(null);
		}

		// Pega o tipo do funcionario enviado pelo token
		Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

		if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
			return ResponseEntity.ok(daoTurma.buscarAtivos());
		} else {
			return new ResponseEntity<List<Turma>>(HttpStatus.UNAUTHORIZED);
		}

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Turma> desativarTurma(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Turma turma = daoTurma.buscar(id);

			Treinamento treinamento = daoTreinamento.buscar(turma.getTreinamento().getId());

			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| treinamento.getPrograma().getCoordenador() != null
							&& treinamento.getPrograma().getCoordenador().getId() == idDoFuncionario
					|| treinamento.getPrograma().getCoordenador() == null && !operacoesCoordena.isEmpty()) {
				daoTurma.deletar(turma.getId());
				return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Turma>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Turma>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Turma> buscarTurma(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Turma turma = daoTurma.buscar(id);

			Treinamento treinamento = daoTreinamento.buscar(turma.getTreinamento().getId());

			List<Programa> programaCoordena = daoPrograma.funcionarioCoordenaGeral(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| treinamento.getPrograma().getCoordenador() != null
							&& treinamento.getPrograma().getCoordenador().getId() == idDoFuncionario
					|| treinamento.getPrograma().getCoordenador() == null && !programaCoordena.isEmpty()) {
				return ResponseEntity.ok(daoTurma.buscar(id));
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Turma>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Turma>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Turma> alterarTurma(@PathVariable Long id, @RequestBody Turma turma,
			HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Treinamento treinamento = daoTreinamento.buscar(turma.getTreinamento().getId());

			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| treinamento.getPrograma().getCoordenador() != null
							&& treinamento.getPrograma().getCoordenador().getId() == idDoFuncionario
					|| treinamento.getPrograma().getCoordenador() == null && !operacoesCoordena.isEmpty()) {
				turma.setId(id);
				daoTurma.alterar(turma);
				return ResponseEntity.status(HttpStatus.OK).body(turma);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Turma>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Turma>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/chamada/{idTurma}", method = RequestMethod.POST)
	public ResponseEntity<String> uploadChamada(@PathVariable Long idTurma, @RequestParam("file") MultipartFile file,
			HttpServletRequest request) throws IOException {

		Turma turma = daoTurma.buscar(idTurma);

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			List<Operacao> operacaoCoordena = daoOperacao.funcionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| (turma.getTreinamento().getPrograma().getCoordenador() == null && !operacaoCoordena.isEmpty())
					|| idDoFuncionario == turma.getTreinamento().getPrograma().getCoordenador().getId()) {
				if (!file.isEmpty()) {
					String caminho = servletContext.getRealPath("/files/chamada");
					byte[] bytes = file.getBytes();
					String arquivoName = File.separator + "chamada-" + idTurma + ".pdf";
					String arquivo = caminho + arquivoName;
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(arquivo));
					stream.write(bytes);
					stream.close();

					return ResponseEntity.status(HttpStatus.OK).body("/files/chamada" + arquivoName);
				} else {
					return ResponseEntity.status(562).body(null);
				}
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/chamada/{idTurma}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteChamada(@PathVariable Long idTurma, HttpServletRequest request)
			throws IOException {

		Turma turma = daoTurma.buscar(idTurma);

		String[] descripitacaoToken = interceptor(request);

		// Verificando se o token est� expirado
		if (descripitacaoToken[0].equals("565")) {
			return ResponseEntity.status(565).body(null);
		}

		// Pega o id do funcionario enviado pelo token
		Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

		// Pega o tipo do funcionario enviado pelo token
		Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

		Treinamento treinamento = daoTreinamento.buscar(turma.getTreinamento().getId());

		List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

		if (tipoDoFuncionario == Tipo.ADMINISTRADOR
				|| treinamento.getPrograma().getCoordenador() != null
						&& treinamento.getPrograma().getCoordenador().getId() == idDoFuncionario
				|| treinamento.getPrograma().getCoordenador() == null && !operacoesCoordena.isEmpty()) {
			String caminho = servletContext.getRealPath("files/chamada");
			File chamada = new File(caminho + "/chamada-" + idTurma + ".pdf");
			chamada.delete();
			return ResponseEntity.noContent().build();
		} else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

	}

}
