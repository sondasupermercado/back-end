package br.com.sondait.controller;

import java.beans.PropertyEditorSupport;
import java.io.File;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.apache.commons.mail.EmailConstants;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.dao.DAOAvaliacao;
import br.com.sondait.dao.DAOConteudo;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.dao.DAOHistoricosAntigos;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.dao.DAOTreinamento;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Aprovado;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Historico;
import br.com.sondait.model.HistoricosAntigos;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.StatusAvaliacao;
import br.com.sondait.model.Tipo;
import br.com.sondait.model.Treinamento;
import br.com.sondait.util.Aprovacao;
import br.com.sondait.util.Reader;

@CrossOrigin
@RequestMapping("/programa")
@RestController
public class ControllerPrograma {

	// DAO para ser utilizado
	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@Autowired
	@Qualifier("JPAHistorico")
	private DAOHistorico daoHistorico;

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	@Autowired
	@Qualifier("JPAAplicacao")
	private DAOAplicacao daoAplicacao;

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPATreinamento")
	private DAOTreinamento daoTreinamento;

	@Autowired
	@Qualifier("JPAConteudo")
	private DAOConteudo daoConteudo;

	@Autowired
	@Qualifier("JPAAvaliacao")
	private DAOAvaliacao daoAvaliacao;

	@Autowired
	@Qualifier("JPAHistoricosAntigos")
	private DAOHistoricosAntigos daoHistoricosAntigos;
	
	@Autowired
	@Qualifier("JPAHistoricosAntigos")
	private DAOHistoricosAntigos daoHistoricoAntigo;

	@Autowired
	private ServletContext servletContext;

	private String token = null;

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");
			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			return null;
		}
	}

	@InitBinder
	public void binder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			public void setAsText(String value) {
				if (value != null) {
					try {
						setValue(new SimpleDateFormat("yyyy-MM-dd").parse(value));
					} catch (ParseException e) {
						setValue(null);
						e.printStackTrace();
					}
				}
			}

			public String getAsText() {
				if (getValue() != null) {
					return new SimpleDateFormat("yyyy-MM-dd").format((Date) getValue());
				}
				return null;
			}
		});
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Programa> criarPrograma(@RequestBody Programa prog, HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				for (Programa programaComparar : daoPrograma.buscarTodos()) {
					if (prog.getNome().equals(programaComparar.getNome()) && programaComparar.isAtivo() == true) {
						return ResponseEntity.status(560).body(null);
					}
				}

				if (prog.getCargo() == null && prog.getOperacao() != null) {
					Operacao operacaoBuscada = daoOperacao.buscar(prog.getOperacao().getId());
					Funcionario coordenador = operacaoBuscada.getCoordenador();
					prog.setOperacao(operacaoBuscada);
					operacaoBuscada.getFuncionarios().forEach(func -> prog.getFuncionarios().add(func));

					prog.setDataInsercao(new Date());
					prog.getTreinamentos().forEach(trei -> trei.setPrograma(prog));

					if (coordenador == null) {
						return ResponseEntity.status(561).body(null);
					} else {
						prog.setCoordenador(coordenador);
					}

					if (prog.getPrazoReciclagem() == 0) {
						prog.setReciclagem(false);
					} else {
						prog.setReciclagem(true);
					}

					daoPrograma.inserir(prog);

					for (Funcionario funcionario : operacaoBuscada.getFuncionarios()) {
						for (Treinamento treinamentos : prog.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionario);
							aplicacao.setDataInicio(new Date());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionario);
						historico.setDataInicio(new Date());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(prog);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(historico.getDataInicio());
						antigos.setDataConclusao(historico.getDataConclusao());
						antigos.setPrograma(prog);
						daoHistoricosAntigos.inserir(antigos);

					}

					return ResponseEntity.created(URI.create("/programa/" + prog.getId())).body(prog);

				} else if (prog.getCargo() != null && prog.getOperacao() == null) {
					List<Funcionario> funcionarios = daoFuncionario.funcioriosCargo(prog.getCargo().getId());
					prog.setFuncionarios(new HashSet<>(funcionarios));
					
					if (prog.getPrazoReciclagem() == 0) {
						prog.setReciclagem(false);
					} else {
						prog.setReciclagem(true);
					}

					prog.setDataInsercao(new Date());
					prog.getTreinamentos().forEach(trein -> trein.setPrograma(prog));
					daoPrograma.inserir(prog);

					for (Funcionario funcionario : funcionarios) {
						for (Treinamento treinamentos : prog.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionario);
							aplicacao.setDataInicio(new Date());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionario);
						historico.setDataInicio(new Date());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(prog);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(historico.getDataInicio());
						antigos.setDataConclusao(historico.getDataConclusao());
						antigos.setPrograma(prog);
						daoHistoricosAntigos.inserir(antigos);

					}

					return ResponseEntity.created(URI.create("/programa/" + prog.getId())).body(prog);

				} else if (prog.getCargo() == null && prog.getOperacao() == null) {
					List<Funcionario> funcionarios = daoFuncionario.buscarAtivos();
					prog.setFuncionarios(new HashSet<>(funcionarios));

					if (prog.getPrazoReciclagem() == 0) {
						prog.setReciclagem(false);
					} else {
						prog.setReciclagem(true);
					}
					
					prog.setDataInsercao(new Date());
					prog.getTreinamentos().forEach(trei -> trei.setPrograma(prog));
					daoPrograma.inserir(prog);

					for (Funcionario funcionario : funcionarios) {
						for (Treinamento treinamentos : prog.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionario);
							aplicacao.setDataInicio(new Date());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionario);
						historico.setDataInicio(new Date());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(prog);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(historico.getDataInicio());
						antigos.setDataConclusao(historico.getDataConclusao());
						antigos.setPrograma(prog);
						daoHistoricosAntigos.inserir(antigos);

					}
					return ResponseEntity.created(URI.create("/programa/" + prog.getId())).body(prog);
				} else {
					return new ResponseEntity<Programa>(HttpStatus.BAD_REQUEST);
				}
			} else {
				return new ResponseEntity<Programa>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Programa>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Programa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Programa>> buscarProgramas(HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				return ResponseEntity.ok(daoPrograma.buscarAtivos());
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Programa>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Programa>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/desativados", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Programa>> buscarDesativados(HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				return ResponseEntity.ok(daoPrograma.buscarDesativados());
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Programa>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<List<Programa>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> desativar(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Busca avalia��o
			Programa programaDeletar = daoPrograma.buscar(id);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| idDoFuncionario == programaDeletar.getCoordenador().getId()) {
				daoPrograma.deletar(programaDeletar.getId());

				for (Historico historico : daoHistorico.historicoPrograma(id)) {
					daoHistorico.remover(historico.getId());
				}

				for (Aplicacao aplicacao : daoAplicacao.aplicacoesPrograma(id)) {
					daoAplicacao.remover(aplicacao.getId());
				}

				return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Programa> buscarPrograma(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Funcionario buscarFuncionario = daoFuncionario.buscar(idDoFuncionario);

			// Busca avalia��o
			Programa programaBuscado = daoPrograma.buscar(id);

			// Lista para saber se o funcionario coordena alguma opera��o para
			// poder verificar os programas para todos
			List<Operacao> operacoesCoordenadas = daoOperacao.funcionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| programaBuscado.getCoordenador() == null && operacoesCoordenadas.size() > 0
					|| buscarFuncionario.getId() == programaBuscado.getCoordenador().getId()) {
				return ResponseEntity.ok(programaBuscado);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Programa> alterarPrograma(@PathVariable Long id, @RequestBody Programa prog,
			HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Funcionario buscarFuncionario = daoFuncionario.buscar(idDoFuncionario);

			Programa programa = daoPrograma.buscar(id);
			prog.setTreinamentos(programa.getTreinamentos());

			// Lista para saber se o funcionario coordena alguma opera��o para
			// poder verificar os programas para todos
			List<Operacao> operacoesCoordenadas = daoOperacao.funcionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| buscarFuncionario.getId() == programa.getCoordenador().getId()
					|| programa.getCoordenador() == null && operacoesCoordenadas.size() > 0) {
				if (programa.getCargo() != null && prog.getCargo() != null
						&& programa.getCargo().getId() == prog.getCargo().getId()
						|| prog.getOperacao() != null && programa.getOperacao() != null
								&& programa.getOperacao().getId() == prog.getOperacao().getId()
						|| programa.getCargo() == null && programa.getOperacao() == null && prog.getCargo() == null
								&& prog.getOperacao() == null) {

					prog.setFuncionarios(programa.getFuncionarios());
					prog.setId(id);
					daoPrograma.alterar(prog);
					return ResponseEntity.status(HttpStatus.OK).body(prog);
				} else {

					for (Funcionario funcionario : programa.getFuncionarios()) {
						if (!prog.getFuncionarios().contains(funcionario)) {
							Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(),
									programa.getId());
							if (historico != null) {
								daoHistorico.remover(historico.getId());
							}

							Aplicacao aplicacao = daoAplicacao.aplicacaoPorProgramaEFuncionarios(programa.getId(),
									funcionario.getId());
							if (aplicacao != null) {
								daoAplicacao.remover(aplicacao.getId());
							}
						}

					}

					if (prog.getCargo() == null && prog.getOperacao() != null) {
						Operacao operacaoBuscada = daoOperacao.buscar(prog.getOperacao().getId());
						Funcionario coordenador = operacaoBuscada.getCoordenador();
						prog.setOperacao(operacaoBuscada);
						operacaoBuscada.getFuncionarios().forEach(func -> prog.getFuncionarios().add(func));

						if (coordenador == null) {
							return ResponseEntity.status(561).body(null);
						} else {
							prog.setCoordenador(coordenador);
						}

						prog.setId(id);
						daoPrograma.alterar(prog);

						for (Funcionario funcionario : operacaoBuscada.getFuncionarios()) {
							Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), id);
							for (Treinamento treinamentos : prog.getTreinamentos()) {
								Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(funcionario.getId(),
										treinamentos.getId());
								if (aplicacao != null) {
									aplicacao.setFuncionario(funcionario);
									aplicacao.setTreinamento(treinamentos);
									if (historico != null) {
										Calendar dataInicio = Calendar.getInstance();
										dataInicio.setTime(historico.getDataConclusao());
										dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
										aplicacao.setDataInicio(dataInicio.getTime());
										aplicacao.setAprovado(Aprovado.PENDENTE);
									} else if (historico == null && funcionario.getDataAdmissao().getTime() < prog
											.getDataInsercao().getTime()) {
										aplicacao.setDataInicio(prog.getDataInsercao());
									} else if (historico == null && funcionario.getDataAdmissao().getTime() > prog
											.getDataInsercao().getTime()) {
										aplicacao.setDataInicio(funcionario.getDataAdmissao());
									}
									daoAplicacao.alterar(aplicacao);

								} else {
									Aplicacao novaAplicacao = new Aplicacao();
									novaAplicacao.setFuncionario(funcionario);
									novaAplicacao.setTreinamento(treinamentos);
									if (historico != null) {
										Calendar dataInicio = Calendar.getInstance();
										dataInicio.setTime(historico.getDataConclusao());
										dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
										novaAplicacao.setDataInicio(dataInicio.getTime());
									} else if (historico == null && funcionario.getDataAdmissao().getTime() < prog
											.getDataInsercao().getTime()) {
										novaAplicacao.setDataInicio(prog.getDataInsercao());
									} else if (historico == null && funcionario.getDataAdmissao().getTime() > prog
											.getDataInsercao().getTime()) {
										novaAplicacao.setDataInicio(funcionario.getDataAdmissao());

									}
									daoAplicacao.inserir(novaAplicacao);
								}
							}

							if (historico != null) {
								Calendar dataInicio = Calendar.getInstance();
								dataInicio.setTime(historico.getDataConclusao());
								dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
								historico.setDataInicio(dataInicio.getTime());
								historico.setFuncionario(funcionario);
								Calendar dataConclusao = Calendar.getInstance();
								dataConclusao.setTime(historico.getDataInicio());
								dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
								historico.setDataConclusao(dataConclusao.getTime());
								historico.setPrograma(prog);
								daoHistorico.alterar(historico);

								HistoricosAntigos antigos = new HistoricosAntigos();
								antigos.setFuncionario(funcionario);
								antigos.setDataInicio(new Date());
								antigos.setDataConclusao(historico.getDataConclusao());
								antigos.setPrograma(prog);
								daoHistoricosAntigos.inserir(antigos);

							} else {
								Historico novoHistorico = new Historico();
								novoHistorico.setFuncionario(funcionario);
								novoHistorico.setPrograma(prog);

								if (funcionario.getDataAdmissao().getTime() < prog.getDataInsercao().getTime()) {
									novoHistorico.setDataInicio(prog.getDataInsercao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(novoHistorico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
									novoHistorico.setDataConclusao(dataConclusao.getTime());
								} else if (funcionario.getDataAdmissao().getTime() > prog.getDataInsercao().getTime()) {
									novoHistorico.setDataInicio(funcionario.getDataAdmissao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(novoHistorico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
									novoHistorico.setDataConclusao(dataConclusao.getTime());
								}
								daoHistorico.inserir(novoHistorico);

								HistoricosAntigos antigos = new HistoricosAntigos();
								antigos.setFuncionario(funcionario);
								antigos.setDataInicio(novoHistorico.getDataInicio());
								antigos.setDataConclusao(novoHistorico.getDataConclusao());
								antigos.setPrograma(prog);
								daoHistoricosAntigos.inserir(antigos);
							}
						}

						return ResponseEntity.status(HttpStatus.OK).body(prog);

					} else if (prog.getCargo() != null && prog.getOperacao() == null) {
						List<Funcionario> funcionarios = daoFuncionario.funcioriosCargo(prog.getCargo().getId());
						prog.setFuncionarios(new HashSet<>(funcionarios));
						prog.setTreinamentos(programa.getTreinamentos());

						prog.setDataInsercao(prog.getDataInsercao());
						prog.setId(id);
						daoPrograma.alterar(prog);

						for (Funcionario funcionario : funcionarios) {
							Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), id);
							for (Treinamento treinamentos : programa.getTreinamentos()) {
								Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(funcionario.getId(),
										treinamentos.getId());
								if (aplicacao != null) {
									aplicacao.setFuncionario(funcionario);
									aplicacao.setTreinamento(treinamentos);
									if (historico != null) {
										Calendar dataInicio = Calendar.getInstance();
										dataInicio.setTime(historico.getDataConclusao());
										dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
										aplicacao.setDataInicio(dataInicio.getTime());
										aplicacao.setTreinamento(treinamentos);
										aplicacao.setAprovado(Aprovado.PENDENTE);
									} else if (historico == null && funcionario.getDataAdmissao().getTime() < prog
											.getDataInsercao().getTime()) {
										aplicacao.setDataInicio(prog.getDataInsercao());
									} else if (historico == null && funcionario.getDataAdmissao().getTime() > prog
											.getDataInsercao().getTime()) {
										aplicacao.setDataInicio(funcionario.getDataAdmissao());
									}
									daoAplicacao.alterar(aplicacao);

								} else {
									Aplicacao novaAplicacao = new Aplicacao();
									novaAplicacao.setFuncionario(funcionario);
									novaAplicacao.setTreinamento(treinamentos);
									if (historico != null) {
										Calendar dataInicio = Calendar.getInstance();
										dataInicio.setTime(historico.getDataConclusao());
										dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
										novaAplicacao.setDataInicio(dataInicio.getTime());
										novaAplicacao.setTreinamento(treinamentos);
									} else if (historico == null && funcionario.getDataAdmissao().getTime() < prog
											.getDataInsercao().getTime()) {
										novaAplicacao.setDataInicio(prog.getDataInsercao());
									} else if (historico == null && funcionario.getDataAdmissao().getTime() > prog
											.getDataInsercao().getTime()) {
										novaAplicacao.setDataInicio(funcionario.getDataAdmissao());
									}
									daoAplicacao.inserir(novaAplicacao);
								}
							}
							if (historico != null) {
								Calendar dataInicio = Calendar.getInstance();
								dataInicio.setTime(historico.getDataConclusao());
								dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
								historico.setDataInicio(dataInicio.getTime());
								historico.setFuncionario(funcionario);
								Calendar dataConclusao = Calendar.getInstance();
								dataConclusao.setTime(historico.getDataInicio());
								dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
								historico.setDataConclusao(dataConclusao.getTime());
								historico.setPrograma(prog);
								daoHistorico.alterar(historico);

								HistoricosAntigos antigos = new HistoricosAntigos();
								antigos.setFuncionario(funcionario);
								antigos.setDataInicio(new Date());
								antigos.setDataConclusao(historico.getDataConclusao());
								antigos.setPrograma(prog);
								daoHistoricosAntigos.inserir(antigos);

							} else {
								Historico novoHistorico = new Historico();
								novoHistorico.setFuncionario(funcionario);
								novoHistorico.setPrograma(prog);

								if (funcionario.getDataAdmissao().getTime() < prog.getDataInsercao().getTime()) {
									novoHistorico.setDataInicio(prog.getDataInsercao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(novoHistorico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
									novoHistorico.setDataConclusao(dataConclusao.getTime());
								} else if (funcionario.getDataAdmissao().getTime() > prog.getDataInsercao().getTime()) {
									novoHistorico.setDataInicio(funcionario.getDataAdmissao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(novoHistorico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
									novoHistorico.setDataConclusao(dataConclusao.getTime());
								}
								daoHistorico.inserir(novoHistorico);

								HistoricosAntigos antigos = new HistoricosAntigos();
								antigos.setFuncionario(funcionario);
								antigos.setDataInicio(novoHistorico.getDataInicio());
								antigos.setDataConclusao(novoHistorico.getDataConclusao());
								antigos.setPrograma(prog);
								daoHistoricosAntigos.inserir(antigos);
							}
						}
						return ResponseEntity.status(HttpStatus.OK).body(prog);

					} else if (prog.getCargo() == null && prog.getOperacao() == null) {
						List<Funcionario> funcionarios = daoFuncionario.buscarAtivos();
						prog.setFuncionarios(new HashSet<>(funcionarios));
						prog.setTreinamentos(programa.getTreinamentos());

						prog.setDataInsercao(prog.getDataInsercao());
						prog.setId(id);
						daoPrograma.alterar(prog);

						for (Funcionario funcionario : funcionarios) {
							Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), id);
							for (Treinamento treinamentos : programa.getTreinamentos()) {
								Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(funcionario.getId(),
										treinamentos.getId());
								if (aplicacao != null) {
									aplicacao.setFuncionario(funcionario);
									aplicacao.setTreinamento(treinamentos);
									if (historico != null) {
										Calendar dataInicio = Calendar.getInstance();
										dataInicio.setTime(historico.getDataConclusao());
										dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
										aplicacao.setDataInicio(dataInicio.getTime());
										aplicacao.setTreinamento(treinamentos);
										aplicacao.setAprovado(Aprovado.PENDENTE);
									} else if (historico == null && funcionario.getDataAdmissao().getTime() < prog
											.getDataInsercao().getTime()) {
										aplicacao.setDataInicio(prog.getDataInsercao());
									} else if (historico == null && funcionario.getDataAdmissao().getTime() > prog
											.getDataInsercao().getTime()) {
										aplicacao.setDataInicio(funcionario.getDataAdmissao());
									}
									daoAplicacao.alterar(aplicacao);

								} else {
									Aplicacao novaAplicacao = new Aplicacao();
									novaAplicacao.setFuncionario(funcionario);
									novaAplicacao.setTreinamento(treinamentos);
									if (historico != null) {
										Calendar dataInicio = Calendar.getInstance();
										dataInicio.setTime(historico.getDataConclusao());
										dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
										novaAplicacao.setDataInicio(dataInicio.getTime());
										novaAplicacao.setTreinamento(treinamentos);
									} else if (historico == null && funcionario.getDataAdmissao().getTime() < prog
											.getDataInsercao().getTime()) {
										novaAplicacao.setDataInicio(prog.getDataInsercao());
									} else if (historico == null && funcionario.getDataAdmissao().getTime() > prog
											.getDataInsercao().getTime()) {
										novaAplicacao.setDataInicio(funcionario.getDataAdmissao());
									}
									daoAplicacao.inserir(novaAplicacao);
								}
							}
							if (historico != null) {
								Calendar dataInicio = Calendar.getInstance();
								dataInicio.setTime(historico.getDataConclusao());
								dataInicio.add(Calendar.MONTH, prog.getPrazoReciclagem());
								historico.setDataInicio(dataInicio.getTime());
								historico.setFuncionario(funcionario);
								Calendar dataConclusao = Calendar.getInstance();
								dataConclusao.setTime(historico.getDataInicio());
								dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
								historico.setDataConclusao(dataConclusao.getTime());
								historico.setPrograma(prog);
								daoHistorico.alterar(historico);

								HistoricosAntigos antigos = new HistoricosAntigos();
								antigos.setFuncionario(funcionario);
								antigos.setDataInicio(new Date());
								antigos.setDataConclusao(historico.getDataConclusao());
								antigos.setPrograma(prog);
								daoHistoricosAntigos.inserir(antigos);

							} else {
								Historico novoHistorico = new Historico();
								novoHistorico.setFuncionario(funcionario);
								novoHistorico.setPrograma(prog);

								if (funcionario.getDataAdmissao().getTime() < prog.getDataInsercao().getTime()) {
									novoHistorico.setDataInicio(prog.getDataInsercao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(novoHistorico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
									novoHistorico.setDataConclusao(dataConclusao.getTime());
								} else if (funcionario.getDataAdmissao().getTime() > prog.getDataInsercao().getTime()) {
									novoHistorico.setDataInicio(funcionario.getDataAdmissao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(novoHistorico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, prog.getDuracao());
									novoHistorico.setDataConclusao(dataConclusao.getTime());
								}
								daoHistorico.inserir(novoHistorico);

								HistoricosAntigos antigos = new HistoricosAntigos();
								antigos.setFuncionario(funcionario);
								antigos.setDataInicio(novoHistorico.getDataInicio());
								antigos.setDataConclusao(novoHistorico.getDataConclusao());
								antigos.setPrograma(prog);
								daoHistoricosAntigos.inserir(antigos);
							}
						}
					}
					return ResponseEntity.status(HttpStatus.OK).body(prog);
				}
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(564).body(null);
		}
	}

	@RequestMapping(value = "/participa/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Programa> funcionarioParticipa(@PathVariable Long id) {
		return daoPrograma.funcionarioProgramaParticipa(id);
	}

	@RequestMapping(value = "/coordena/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Programa>> funcionarioCoordena(@PathVariable Long id, HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			if (daoOperacao.funcionarioCoordena(id).size() > 0) {
				List<Programa> programasFuncionarioCoordena = new ArrayList<>();
				programasFuncionarioCoordena = daoPrograma.funcionarioCoordena(id);
				programasFuncionarioCoordena.addAll(daoPrograma.BuscarProgramasParaTodos());

				return ResponseEntity.status(HttpStatus.OK).body(programasFuncionarioCoordena);
			} else {
				return ResponseEntity.status(HttpStatus.OK).body(daoPrograma.funcionarioCoordena(id));
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(564).body(null);
		}
	}

	@RequestMapping(value = "/funcionario/{id}/aplicacao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Aplicacao>> funcionariosParticipam(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Funcionario buscarFuncionario = daoFuncionario.buscar(idDoFuncionario);
			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || buscarFuncionario.getId() == id) {
				return ResponseEntity.status(HttpStatus.OK).body(daoPrograma.aplicacoesPrograma(id));
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(564).body(null);
		}
	}

	@RequestMapping(value = "/avaliacoes/{idFuncionario}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Programa>> programasPendentes(@PathVariable Long idFuncionario,
			HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			List<Aprovacao> aprovacoes = new ArrayList<>();
			List<Operacao> operacaoCoordena = daoOperacao.funcionarioCoordena(idDoFuncionario);
			List<Programa> programaAtivos = daoPrograma.buscarAtivos();
			List<Programa> programasCoordenador = daoPrograma.funcionarioCoordena(idDoFuncionario);
			List<Programa> programasTodos = daoPrograma.BuscarProgramasParaTodos();
			programasCoordenador.addAll(programasTodos);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {

				List<Aprovacao> aprovacoesPrograma = new ArrayList<>();

				for (Programa programa : programaAtivos) {
					for (Funcionario funcionario : programa.getFuncionarios()) {
						Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), programa.getId());
						if (historico != null) {

							Calendar calendar = Calendar.getInstance();
							calendar.setTime(historico.getDataInicio());
							calendar.add(Calendar.DAY_OF_MONTH, 1);

							List<Aplicacao> aplicacao = daoAplicacao.aplicacoesAprovadas(
									historico.getPrograma().getId(), funcionario.getId(), calendar.getTime());

							Calendar dataConclusao = Calendar.getInstance();
							dataConclusao.setTime(historico.getDataInicio());
							dataConclusao.add(Calendar.DAY_OF_YEAR, historico.getPrograma().getDuracao());

							Aprovacao aprovacao = new Aprovacao();
							aprovacao.setFuncionario(funcionario);
							aprovacao.setAplicacoes(new HashSet<>(aplicacao));
							aprovacao.setDataInicio(historico.getDataInicio());
							aprovacao.setDataConclusao(dataConclusao.getTime());

							Calendar dataAtual = Calendar.getInstance();
							dataAtual.setTime(new Date());

							Calendar prazoEficacia = Calendar.getInstance();
							prazoEficacia.setTime(historico.getDataConclusao());
							prazoEficacia.add(Calendar.DAY_OF_YEAR, programa.getPrazoEficacia());

							Calendar prazoReciclagem = Calendar.getInstance();
							prazoReciclagem.setTime(historico.getDataConclusao());
							prazoReciclagem.add(Calendar.MONTH, programa.getPrazoReciclagem());

							if (historico.isAtivo() && dataAtual.after(prazoEficacia)
									&& dataAtual.before(prazoReciclagem)) {

								if (daoAvaliacao.avaliacaoEntreDatas(prazoEficacia.getTime(), prazoReciclagem.getTime(),
										programa.getId(), historico.getFuncionario().getId()) != null) {
									aprovacao.setStatus(StatusAvaliacao.REALIZADA);
								} else {
									List<Aplicacao> aplicacoesTreinamento = new ArrayList<>();
									for (Treinamento treinamento : historico.getPrograma().getTreinamentos()) {
										Aplicacao aplicacaoFuncionario = daoAplicacao.aplicacoesTreinamento(
												historico.getFuncionario().getId(), treinamento.getId());
										aplicacoesTreinamento.add(aplicacaoFuncionario);
									}

									List<Aplicacao> aplicacoesAprovadas = new ArrayList<>();
									for (Aplicacao status : aplicacoesTreinamento) {
										if (status.getAprovado() == Aprovado.APROVADO) {
											aplicacoesAprovadas.add(status);
										}
									}

									if (aplicacoesTreinamento.size() == aplicacoesAprovadas.size()) {
										aprovacao.setStatus(StatusAvaliacao.DISPONIVEL);

										aprovacoes.add(aprovacao);
									} else {
										aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
									}
								}
							} else {
								aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
							}

							aprovacoesPrograma.add(aprovacao);
						}
					}
				}

				List<Programa> programaAvaliacao = new ArrayList<>();
				for (Aprovacao aprovacaoPrograma : aprovacoes) {
					for (Aplicacao aplicacao : aprovacaoPrograma.getAplicacoes()) {

						if (!programaAvaliacao.contains(aplicacao.getTreinamento().getPrograma())) {
							Programa programa = daoPrograma.buscar(aplicacao.getTreinamento().getPrograma().getId());
							programaAvaliacao.add(programa);
						}
						break;
					}
				}

				return ResponseEntity.status(HttpStatus.OK).body(programaAvaliacao);

			} else if (!operacaoCoordena.isEmpty() || !programasCoordenador.isEmpty()) {

				List<Aprovacao> aprovacoesOperacao = new ArrayList<>();
				List<Programa> programasCoordena = daoPrograma.funcionarioCoordenaGeral(idDoFuncionario);
				List<Operacao> operacoesCoordena = daoOperacao.funcionarioCoordena(idDoFuncionario);
				List<Programa> programaTodos = daoPrograma.BuscarProgramasParaTodos();
				programasCoordena.addAll(programaTodos);
				List<Funcionario> todosFuncionariosPrograma = new ArrayList<>();

				for (Programa programa : programasCoordena) {
					if (programa.getCoordenador() == null && !programa.getFuncionarios().isEmpty()) {
						for (Funcionario funcionariosPrograma : programa.getFuncionarios()) {
							for (Operacao operacoesFuncionario : operacoesCoordena) {
								if (operacoesFuncionario.getFuncionarios().contains(funcionariosPrograma)) {
									todosFuncionariosPrograma.add(funcionariosPrograma);
								}
							}
						}

						for (Funcionario funcionario : todosFuncionariosPrograma) {
							Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(),
									programa.getId());
							if (historico != null) {
								Calendar calendar = Calendar.getInstance();
								calendar.setTime(historico.getDataInicio());
								calendar.add(Calendar.DAY_OF_MONTH, 1);

								List<Aplicacao> aplicacao = daoAplicacao.aplicacoesAprovadas(
										historico.getPrograma().getId(), funcionario.getId(), calendar.getTime());

								Aprovacao aprovacao = new Aprovacao();
								aprovacao.setFuncionario(funcionario);
								aprovacao.setAplicacoes(new HashSet<>(aplicacao));
								aprovacao.setDataInicio(historico.getDataInicio());
								aprovacao.setDataConclusao(historico.getDataConclusao());

								Calendar dataAtual = Calendar.getInstance();
								dataAtual.setTime(new Date());

								Calendar prazoEficacia = Calendar.getInstance();
								prazoEficacia.setTime(historico.getDataConclusao());
								prazoEficacia.add(Calendar.DAY_OF_YEAR, historico.getPrograma().getPrazoEficacia());

								Calendar prazoReciclagem = Calendar.getInstance();
								prazoReciclagem.setTime(historico.getDataConclusao());
								prazoReciclagem.add(Calendar.MONTH, historico.getPrograma().getPrazoReciclagem());

								if (historico.isAtivo() && dataAtual.after(prazoEficacia)
										&& dataAtual.before(prazoReciclagem)) {

									if (daoAvaliacao.avaliacaoEntreDatas(prazoEficacia.getTime(),
											prazoReciclagem.getTime(), programa.getId(),
											historico.getFuncionario().getId()) != null) {
										aprovacao.setStatus(StatusAvaliacao.REALIZADA);

									} else {
										List<Aplicacao> aplicacoesTreinamento = new ArrayList<>();
										for (Treinamento treinamento : historico.getPrograma().getTreinamentos()) {
											Aplicacao aplicacaoFuncionario = daoAplicacao.aplicacoesTreinamento(
													historico.getFuncionario().getId(), treinamento.getId());
											aplicacoesTreinamento.add(aplicacaoFuncionario);
										}

										List<Aplicacao> aplicacoesAprovadas = new ArrayList<>();
										for (Aplicacao status : aplicacoesTreinamento) {
											if (status.getAprovado() == Aprovado.APROVADO) {
												aplicacoesAprovadas.add(status);
											}
										}

										if (aplicacoesTreinamento.size() == aplicacoesAprovadas.size()) {
											aprovacao.setStatus(StatusAvaliacao.DISPONIVEL);
											aprovacoesOperacao.add(aprovacao);

										} else {
											aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
										}
									}
								} else {
									aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
								}
							}
						}

					} else {
						for (Funcionario funcionario : programa.getFuncionarios()) {
							Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(),
									programa.getId());
							if (historico != null) {
								Calendar calendar = Calendar.getInstance();
								calendar.setTime(historico.getDataInicio());
								calendar.add(Calendar.DAY_OF_MONTH, 1);

								List<Aplicacao> aplicacao = daoAplicacao.aplicacoesAprovadas(
										historico.getPrograma().getId(), funcionario.getId(), calendar.getTime());

								Aprovacao aprovacao = new Aprovacao();
								aprovacao.setFuncionario(funcionario);
								aprovacao.setAplicacoes(new HashSet<>(aplicacao));
								aprovacao.setDataInicio(historico.getDataInicio());
								aprovacao.setDataConclusao(historico.getDataConclusao());

								Calendar dataAtual = Calendar.getInstance();
								dataAtual.setTime(new Date());

								Calendar prazoEficacia = Calendar.getInstance();
								prazoEficacia.setTime(historico.getDataConclusao());
								prazoEficacia.add(Calendar.DAY_OF_YEAR, historico.getPrograma().getPrazoEficacia());

								Calendar prazoReciclagem = Calendar.getInstance();
								prazoReciclagem.setTime(historico.getDataConclusao());
								prazoReciclagem.add(Calendar.MONTH, historico.getPrograma().getPrazoReciclagem());

								if (historico.isAtivo() && dataAtual.after(prazoEficacia)
										&& dataAtual.before(prazoReciclagem)) {

									if (daoAvaliacao.avaliacaoEntreDatas(prazoEficacia.getTime(),
											prazoReciclagem.getTime(), programa.getId(),
											historico.getFuncionario().getId()) != null) {
										aprovacao.setStatus(StatusAvaliacao.REALIZADA);

									} else {
										List<Aplicacao> aplicacoesTreinamento = new ArrayList<>();
										for (Treinamento treinamento : historico.getPrograma().getTreinamentos()) {
											Aplicacao aplicacaoFuncionario = daoAplicacao.aplicacoesTreinamento(
													historico.getFuncionario().getId(), treinamento.getId());
											aplicacoesTreinamento.add(aplicacaoFuncionario);
										}

										List<Aplicacao> aplicacoesAprovadas = new ArrayList<>();
										for (Aplicacao status : aplicacoesTreinamento) {
											if (status.getAprovado() == Aprovado.APROVADO) {
												aplicacoesAprovadas.add(status);
											}
										}

										if (aplicacoesTreinamento.size() == aplicacoesAprovadas.size()) {
											aprovacao.setStatus(StatusAvaliacao.DISPONIVEL);
											aprovacoesOperacao.add(aprovacao);

										} else {
											aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
										}
									}
								} else {
									aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
								}
							}
						}
					}
				}

				List<Programa> programaAvaliacao = new ArrayList<>();
				for (Aprovacao aprovacaoPrograma : aprovacoesOperacao) {
					for (Aplicacao aplicacao : aprovacaoPrograma.getAplicacoes()) {

						if (!programaAvaliacao.contains(aplicacao.getTreinamento().getPrograma())) {
							Programa programa = daoPrograma.buscar(aplicacao.getTreinamento().getPrograma().getId());
							programaAvaliacao.add(programa);
						}
						break;
					}
				}
				return ResponseEntity.status(HttpStatus.OK).body(programaAvaliacao);

			} else {
				return new ResponseEntity<List<Programa>>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Programa>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Programa>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/treinamentos/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Aprovacao>> verPrograma(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			List<Operacao> operacaoCoordena = daoOperacao.funcionarioCoordena(idDoFuncionario);
			Programa programa = daoPrograma.buscar(id);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| programa.getCoordenador() != null && idDoFuncionario == programa.getCoordenador().getId()) {

				List<Aprovacao> aprovacoesPrograma = new ArrayList<>();

				for (Funcionario funcionario : programa.getFuncionarios()) {
					Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), programa.getId());
					if (historico != null) {

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(historico.getDataInicio());
						calendar.add(Calendar.DAY_OF_MONTH, 1);

						List<Aplicacao> aplicacao = daoAplicacao.aplicacoesAprovadas(historico.getPrograma().getId(),
								funcionario.getId(), calendar.getTime());

						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, historico.getPrograma().getDuracao());

						Aprovacao aprovacao = new Aprovacao();
						aprovacao.setFuncionario(funcionario);
						aprovacao.setAplicacoes(new HashSet<>(aplicacao));
						aprovacao.setDataInicio(historico.getDataInicio());
						aprovacao.setDataConclusao(dataConclusao.getTime());

						Calendar dataAtual = Calendar.getInstance();
						dataAtual.setTime(new Date());

						Calendar prazoEficacia = Calendar.getInstance();
						prazoEficacia.setTime(historico.getDataConclusao());
						prazoEficacia.add(Calendar.DAY_OF_YEAR, programa.getPrazoEficacia());

						Calendar prazoReciclagem = Calendar.getInstance();
						prazoReciclagem.setTime(historico.getDataConclusao());
						prazoReciclagem.add(Calendar.MONTH, programa.getPrazoReciclagem());

						if (historico.isAtivo() && dataAtual.after(prazoEficacia)
								&& dataAtual.before(prazoReciclagem)) {

							if (daoAvaliacao.avaliacaoEntreDatas(prazoEficacia.getTime(), prazoReciclagem.getTime(), id,
									historico.getFuncionario().getId()) != null) {
								aprovacao.setStatus(StatusAvaliacao.REALIZADA);
							} else {
								List<Aplicacao> aplicacoesTreinamento = new ArrayList<>();
								for (Treinamento treinamento : historico.getPrograma().getTreinamentos()) {
									Aplicacao aplicacaoFuncionario = daoAplicacao.aplicacoesTreinamento(
											historico.getFuncionario().getId(), treinamento.getId());
									aplicacoesTreinamento.add(aplicacaoFuncionario);
								}

								List<Aplicacao> aplicacoesAprovadas = new ArrayList<>();
								for (Aplicacao status : aplicacoesTreinamento) {
									if (status.getAprovado() == Aprovado.APROVADO) {
										aplicacoesAprovadas.add(status);
									}
								}

								if (aplicacoesTreinamento.size() == aplicacoesAprovadas.size()) {
									aprovacao.setStatus(StatusAvaliacao.DISPONIVEL);
								} else {
									aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
								}
							}
						} else {
							aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
						}

						aprovacoesPrograma.add(aprovacao);
					}
				}
				return ResponseEntity.status(HttpStatus.OK).body(aprovacoesPrograma);

			} else if (!operacaoCoordena.isEmpty()) {
				List<Aprovacao> aprovacoesOperacao = new ArrayList<>();

				for (Operacao operacao : operacaoCoordena) {
					for (Funcionario funcionario : operacao.getFuncionarios()) {
						Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), id);
						if (historico != null) {
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(historico.getDataInicio());
							calendar.add(Calendar.DAY_OF_MONTH, 1);

							List<Aplicacao> aplicacao = daoAplicacao.aplicacoesAprovadas(
									historico.getPrograma().getId(), funcionario.getId(), calendar.getTime());

							Aprovacao aprovacao = new Aprovacao();
							aprovacao.setFuncionario(funcionario);
							aprovacao.setAplicacoes(new HashSet<>(aplicacao));
							aprovacao.setDataInicio(historico.getDataInicio());
							aprovacao.setDataConclusao(historico.getDataConclusao());

							Calendar dataAtual = Calendar.getInstance();
							dataAtual.setTime(new Date());

							Calendar prazoEficacia = Calendar.getInstance();
							prazoEficacia.setTime(historico.getDataConclusao());
							prazoEficacia.add(Calendar.DAY_OF_YEAR, historico.getPrograma().getPrazoEficacia());

							Calendar prazoReciclagem = Calendar.getInstance();
							prazoReciclagem.setTime(historico.getDataConclusao());
							prazoReciclagem.add(Calendar.MONTH, historico.getPrograma().getPrazoReciclagem());

							if (historico.isAtivo() && dataAtual.after(prazoEficacia)
									&& dataAtual.before(prazoReciclagem)) {

								if (daoAvaliacao.avaliacaoEntreDatas(prazoEficacia.getTime(), prazoReciclagem.getTime(),
										id, historico.getFuncionario().getId()) != null) {
									aprovacao.setStatus(StatusAvaliacao.REALIZADA);

								} else {
									List<Aplicacao> aplicacoesTreinamento = new ArrayList<>();
									for (Treinamento treinamento : historico.getPrograma().getTreinamentos()) {
										Aplicacao aplicacaoFuncionario = daoAplicacao.aplicacoesTreinamento(
												historico.getFuncionario().getId(), treinamento.getId());
										aplicacoesTreinamento.add(aplicacaoFuncionario);
									}

									List<Aplicacao> aplicacoesAprovadas = new ArrayList<>();
									for (Aplicacao status : aplicacoesTreinamento) {
										if (status.getAprovado() == Aprovado.APROVADO) {
											aplicacoesAprovadas.add(status);
										}
									}

									if (aplicacoesTreinamento.size() == aplicacoesAprovadas.size()) {
										aprovacao.setStatus(StatusAvaliacao.DISPONIVEL);
									} else {
										aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
									}
								}
							} else {
								aprovacao.setStatus(StatusAvaliacao.INDISPONIVEL);
							}

							aprovacoesOperacao.add(aprovacao);
						}
					}
				}
				return ResponseEntity.status(HttpStatus.OK).body(aprovacoesOperacao);
			} else {
				return new ResponseEntity<List<Aprovacao>>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<List<Aprovacao>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Aprovacao>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/status/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Aprovacao> alterarStatus(@PathVariable Long id, @RequestBody int status,
			HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			Aplicacao aplicacao = daoAplicacao.buscar(id);
			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| aplicacao.getTreinamento().getPrograma().getCoordenador() == null
					|| aplicacao.getTreinamento().getPrograma().getCoordenador().getId() == idDoFuncionario) {

				aplicacao.setId(id);
				aplicacao.setAprovado(Aprovado.values()[status]);
				daoAplicacao.alterar(aplicacao);

				// Busca todas as aplicacoes do programa(programa que esta
				// relacionado a aplicacao acima)
				List<Aplicacao> aplicacoesTreinamento = new ArrayList<>();
				for (Treinamento treinamento : aplicacao.getTreinamento().getPrograma().getTreinamentos()) {
					Aplicacao aplicacaoFuncionario = daoAplicacao
							.aplicacoesTreinamento(aplicacao.getFuncionario().getId(), treinamento.getId());
					aplicacoesTreinamento.add(aplicacaoFuncionario);
				}

				List<Aplicacao> aplicacoesAprovadas = new ArrayList<>();
				for (Aplicacao statusAplicacao : aplicacoesTreinamento) {
					Historico historico = daoHistorico.historicoFuncionario(aplicacao.getFuncionario().getId(),
							aplicacao.getTreinamento().getPrograma().getId());
					if (statusAplicacao.getAprovado() == Aprovado.REPROVADO) {
						if (historico != null) {
							historico.setSituacao(Aprovado.REPROVADO);
							daoHistorico.alterarHistorico(historico);

							HistoricosAntigos antigo = daoHistoricosAntigos.buscar(aplicacao.getFuncionario().getId(),
									historico.getDataInicio());
							antigo.setSituacao(Aprovado.REPROVADO);
							daoHistoricosAntigos.alterarHistorigoAntigo(antigo);
						}
					} else if (statusAplicacao.getAprovado() == Aprovado.APROVADO) {
						for (Aplicacao aplicacaoFuncionario : aplicacoesTreinamento) {
							if (aplicacaoFuncionario.getAprovado() == Aprovado.APROVADO) {
								aplicacoesAprovadas.add(aplicacaoFuncionario);
								continue;
							} else {
								break;
							}
						}

						if (aplicacoesAprovadas.size() == aplicacoesTreinamento.size()) {
							historico.setSituacao(Aprovado.APROVADO);
							daoHistorico.alterarHistorico(historico);

							HistoricosAntigos antigo = daoHistoricosAntigos.buscar(aplicacao.getFuncionario().getId(),
									historico.getDataInicio());

							antigo.setSituacao(Aprovado.APROVADO);
							daoHistoricosAntigos.alterarHistorigoAntigo(antigo);

							int cargaHoraria = 0;
							for (Treinamento treinamento : historico.getPrograma().getTreinamentos()) {
								cargaHoraria += treinamento.getConteudo().getCargaHoraria();
							}

							Calendar dataEmail = Calendar.getInstance();
							dataEmail.setTime(historico.getDataConclusao());
							dataEmail.add(Calendar.DAY_OF_YEAR, historico.getPrograma().getDuracao());

							Calendar dataAtual = Calendar.getInstance();
							dataAtual.setTime(historico.getDataConclusao());
							dataAtual.add(Calendar.DAY_OF_YEAR, historico.getPrograma().getDuracao());
							dataAtual.add(Calendar.DAY_OF_YEAR, 2);

							HtmlEmail email = new HtmlEmail();
							email.setHostName("smtp.gmail.com");
							email.setAuthentication("sondaittreinamentos@gmail.com", "S0nda_132");
							email.setSmtpPort(587);
							email.setStartTLSEnabled(true);
							email.setStartTLSRequired(true);
							email.setFrom("sondaittreinamentos@gmail.com", "Sonda IT ");
							email.setCharset(EmailConstants.UTF_8);
							email.addTo(historico.getFuncionario().getEmail(), historico.getFuncionario().getNome());

							String paginaEmail = Reader
									.readFile(servletContext.getRealPath("files/template_email/emailCertificado.html"));
							String[] emailPart = paginaEmail.split("!");
							String logoId = email.embed(new File(
									servletContext.getRealPath("files/template_email/sondaitlogo-branco.png")));
							email.setHtmlMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
									+ antigo.getFuncionario().getNome() + emailPart[2] + antigo.getPrograma().getNome()
									+ emailPart[3] + cargaHoraria + emailPart[4])
									.setSubject("Sistema de Gest�o de Treinamento - Certifica��o");
							email.setSubject("Certificado " + historico.getPrograma().getNome());
							email.send();

						}

					} else if (statusAplicacao.getAprovado() == Aprovado.PENDENTE) {
						for (Aplicacao aplicacaoFuncionario : aplicacoesTreinamento) {
							if (aplicacaoFuncionario.getAprovado() == Aprovado.PENDENTE) {
								aplicacoesAprovadas.add(aplicacaoFuncionario);
								continue;
							} else {
								break;
							}
						}

						List<Aplicacao> aplicoesStatus = new ArrayList<>();
						if (aplicacoesAprovadas.size() == aplicacoesTreinamento.size()) {
							historico.setSituacao(Aprovado.PENDENTE);
							daoHistorico.alterarHistorico(historico);

							HistoricosAntigos antigo = daoHistoricosAntigos.buscar(aplicacao.getFuncionario().getId(),
									historico.getDataInicio());
							antigo.setSituacao(Aprovado.PENDENTE);
							daoHistoricosAntigos.alterarHistorigoAntigo(antigo);

						} else {
							for (Aplicacao aplicacoes : aplicacoesAprovadas) {
								if (aplicacoes.getAprovado() == Aprovado.APROVADO) {
									aplicoesStatus.add(aplicacoes);
								}
							}

							if (aplicoesStatus.size() != aplicacoesTreinamento.size()) {
								historico.setSituacao(Aprovado.PENDENTE);
								daoHistorico.alterarHistorico(historico);

								HistoricosAntigos antigo = daoHistoricosAntigos
										.buscar(aplicacao.getFuncionario().getId(), historico.getDataInicio());
								antigo.setSituacao(Aprovado.PENDENTE);
								daoHistoricosAntigos.alterarHistorigoAntigo(antigo);
							}
						}
					}
				}
				return ResponseEntity.status(HttpStatus.OK).body(null);
			} else {
				return new ResponseEntity<Aprovacao>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Aprovacao>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Aprovacao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/mateus", method = RequestMethod.POST)
	public ResponseEntity<Void> mateus() {
		
		for (Programa programa : daoPrograma.programasReciclagem()) {
			for (Funcionario funcionario : programa.getFuncionarios()) {
				Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), programa.getId());

				Calendar prazoReciclagem = Calendar.getInstance();
				prazoReciclagem.setTime(historico.getDataConclusao());
				prazoReciclagem.add(Calendar.MONTH, programa.getPrazoReciclagem());

				Calendar doisDias = Calendar.getInstance();
				doisDias.setTime(historico.getDataConclusao());
				doisDias.add(Calendar.MONTH, programa.getPrazoReciclagem());
				doisDias.add(Calendar.DAY_OF_MONTH, -2);

				Calendar dataAtual = Calendar.getInstance();
				dataAtual.setTime(new Date());

				Calendar diasEmail = Calendar.getInstance();
				diasEmail.setTime(new Date());
				diasEmail.add(Calendar.DAY_OF_MONTH, 1);
				
				if(daoTreinamento.treinamentosReciclagem(programa.getId()).isEmpty()){
					return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
				}
				
					for (Treinamento treinamento : daoTreinamento.treinamentosReciclagem(programa.getId())) {
						
						
						Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(funcionario.getId(),
								treinamento.getId());

						// Altera as aplicacoes
						aplicacao.setFuncionario(funcionario);
						aplicacao.setTreinamento(treinamento);
						Calendar dataInicio = Calendar.getInstance();
						dataInicio.setTime(historico.getDataConclusao());
						dataInicio.add(Calendar.MONTH, treinamento.getPrograma().getPrazoReciclagem());
						aplicacao.setDataInicio(dataInicio.getTime());
						aplicacao.setAprovado(Aprovado.PENDENTE);
						daoAplicacao.alterar(aplicacao);
					}

					// Altera o historico Atual
					Calendar dataInicio1 = Calendar.getInstance();
					dataInicio1.setTime(historico.getDataConclusao());
					dataInicio1.add(Calendar.MONTH, programa.getPrazoReciclagem());
					historico.setDataInicio(dataInicio1.getTime());
					historico.setFuncionario(funcionario);
					Calendar dataConclusao = Calendar.getInstance();
					dataConclusao.setTime(historico.getDataInicio());
					dataConclusao.add(Calendar.DAY_OF_YEAR, programa.getDuracao());
					historico.setDataConclusao(dataConclusao.getTime());
					historico.setPrograma(programa);
					daoHistorico.alterar(historico);

					// Altera o historico antigo

					HistoricosAntigos antigos = daoHistoricoAntigo.historicoFuncionario(funcionario.getId(),
							programa.getId());

					if (antigos != null) {
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(new Date());
						antigos.setDataConclusao(historico.getDataConclusao());
						antigos.setPrograma(programa);
						daoHistoricoAntigo.alterar(antigos);

					} else {
						HistoricosAntigos novohistoricoAntigo = new HistoricosAntigos();
						novohistoricoAntigo.setFuncionario(funcionario);
						novohistoricoAntigo.setDataInicio(new Date());
						novohistoricoAntigo.setDataConclusao(historico.getDataConclusao());
						novohistoricoAntigo.setPrograma(programa);
						daoHistoricoAntigo.inserir(novohistoricoAntigo);
					}

					List<Treinamento> treinamentosPrograma = daoTreinamento.treinamentosReciclagem(programa.getId());
					String treinamentosNome = "";
					for (Treinamento treinamentos : treinamentosPrograma) {
						treinamentosNome += treinamentos.getConteudo().getNome() + ",";
					}
					final String treinamentoEmail = treinamentosNome;

					new Thread() {
						public void run() {
							try {
								HtmlEmail email = new HtmlEmail();
								email.setHostName("smtp.gmail.com");
								email.setAuthentication("sondaittreinamentos@gmail.com", "S0nda_132");
								email.setSmtpPort(587);
								email.setStartTLSEnabled(true);
								email.setStartTLSRequired(true);
								email.setFrom("sondaittreinamentos@gmail.com", "Sonda Treinamentos");
								email.addTo(historico.getFuncionario().getEmail(),
										historico.getFuncionario().getNome());
								email.setCharset(EmailConstants.UTF_8);
								String paginaEmail = Reader
										.readFile(servletContext.getRealPath("files/template_email/emailRecl.html"));
								String[] emailPart = paginaEmail.split("!");
								String logoId = email.embed(new File(
										servletContext.getRealPath("files/template_email/sondaitlogo-branco.png")));

								email.setHtmlMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
										+ antigos.getFuncionario().getNome().split(" ")[0] + emailPart[2]
										+ antigos.getPrograma().getNome() + emailPart[3] + treinamentoEmail
										+ emailPart[4]).setSubject("Sistema de Gest�o de Treinamento - Certifica��o");
								email.send();
							} catch (Exception e) {
								e.printStackTrace();
							}

						};
					}.start();
				}
			}
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

}
