package br.com.sondait.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.dao.DAOHistoricosAntigos;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.dao.DAOTreinamento;
import br.com.sondait.gerador.GerarXLS;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Aprovado;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Historico;
import br.com.sondait.model.HistoricosAntigos;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.Relatorio;
import br.com.sondait.model.Tipo;
import br.com.sondait.model.Treinamento;
import br.com.sondait.util.Grafico;

@CrossOrigin // Serve para fazer um acesso cross em dois servidores(servidor
				// back e servido front por exemplo)
@RequestMapping("/operacao")
@RestController
public class ControllerOperacao {

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@Autowired
	@Qualifier("JPAHistorico")
	private DAOHistorico daoHistorico;

	@Autowired
	@Qualifier("JPAAplicacao")
	private DAOAplicacao daoAplicacao;

	@Autowired
	@Qualifier("JPAHistoricosAntigos")
	private DAOHistoricosAntigos daoHistoricosAntigos;
	
	@Autowired
	@Qualifier("JPAHistoricosAntigos")
	private DAOHistoricosAntigos daoHistoricoAntigo;

	@Autowired
	@Qualifier("JPATreinamento")
	private DAOTreinamento daoTreinamento;

	@Autowired
	private ServletContext servletContext;

	private String token;

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");

			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Operacao> criarOperacao(@RequestBody Operacao operacao, HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				for (Operacao operacaoComparar : daoOperacao.buscarTodos()) {
					if (operacao.getNome().toLowerCase().equals(operacaoComparar.getNome().toLowerCase())
							&& operacaoComparar.isAtivo() == false) {
						operacao.setId(operacaoComparar.getId());
						daoOperacao.alterar(operacao);
						return ResponseEntity.status(HttpStatus.OK).body(operacao);
					} else if (operacao.getNome().toLowerCase().equals(operacaoComparar.getNome().toLowerCase())
							&& operacaoComparar.isAtivo() == true) {
						return ResponseEntity.status(560).body(null);
					}
				}

				// Busca o coordenador
				Funcionario coordenador = daoFuncionario.buscar(operacao.getCoordenador().getId());

				// Criando a lista de funcionarios
				Set<Funcionario> funcionarios = new HashSet<>();

				// Fazendo a busca de funcionarios
				for (Funcionario funcionario : operacao.getFuncionarios()) {
					Funcionario f = daoFuncionario.buscar(funcionario.getId());
					funcionarios.add(f);
				}

				// Setando coordenador
				operacao.setCoordenador(coordenador);

				// Setando funcionario
				operacao.setFuncionarios(funcionarios);

				daoOperacao.inserir(operacao);
				return ResponseEntity.created(URI.create("/operacao/" + operacao.getId())).body(operacao);
			} else {
				return new ResponseEntity<Operacao>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Operacao>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Operacao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/relatorio/{idOperacao}", method = RequestMethod.POST)
	public ResponseEntity<ObjectNode> gerarRelatorioXlsOperacao(@PathVariable Long idOperacao,
			HttpServletRequest request) {
		Operacao operacao = daoOperacao.buscar(idOperacao);

		Relatorio relatorio = new Relatorio();
		relatorio.setOperacao(operacao);

		String[] descripitacaoToken = interceptor(request);

		// Verificando se o token est� expirado
		if (descripitacaoToken[0].equals("565")) {
			return ResponseEntity.status(565).body(null);
		}

		// Pega o id do funcionario enviado pelo token
		Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

		// Pega o tipo do funcionario enviado pelo token
		Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

		if (tipoDoFuncionario == Tipo.ADMINISTRADOR || idDoFuncionario == operacao.getCoordenador().getId()) {

			String arquivo = GerarXLS.criarExcelDaOperacao(relatorio, servletContext.getRealPath("files"));

			ObjectMapper mapper = new ObjectMapper();
			ObjectNode objNode = mapper.createObjectNode();
			objNode.put("caminho", arquivo);

			if (arquivo != null) {
				return new ResponseEntity<ObjectNode>(objNode, HttpStatus.OK);
			} else {
				return new ResponseEntity<ObjectNode>(HttpStatus.BAD_REQUEST);
			}

		} else {
			return new ResponseEntity<ObjectNode>(HttpStatus.UNAUTHORIZED);
		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Operacao>> buscarOperacoes(HttpServletRequest request) {
		try {			
			String[] descripitacaoToken = interceptor(request);
			
			// Verificando se o token est� expirado
			if (descripitacaoToken[0].contains("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				return ResponseEntity.ok(daoOperacao.buscarAtivos());
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> desativar(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				Operacao operacao = daoOperacao.buscar(id);
				operacao.getFuncionarios().clear();

				for (Programa programa : daoPrograma.programasOperacao(id)) {
					daoPrograma.deletar(programa.getId());
				}

				daoOperacao.alterar(operacao);
				daoOperacao.deletar(id);

				return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Operacao> buscarOperacao(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Busca opera��o pelo id enviado pela url
			Operacao operacaoBuscada = daoOperacao.buscar(id);

			operacaoBuscada.setProgramasOperacao(new HashSet<>(daoPrograma.programasOperacao(operacaoBuscada.getId())));

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| idDoFuncionario == operacaoBuscada.getCoordenador().getId()) {
				return ResponseEntity.ok(operacaoBuscada);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/desativados", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Operacao>> operacoesDesativadas(HttpServletRequest request) {
		try {			
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				return ResponseEntity.ok(daoOperacao.operacoesDesativadas());
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Operacao> alterarOperacao(@PathVariable Long id, @RequestBody Operacao operacao,
			HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Busca opera��o pelo id enviado pela url
			Operacao operacaoBuscada = daoOperacao.buscar(id);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| idDoFuncionario == operacaoBuscada.getCoordenador().getId()) {

				if (operacao.getCoordenador() != operacaoBuscada.getCoordenador()) {
					List<Programa> programaOperacao = daoPrograma.programasOperacao(id);
					for (Programa programas : programaOperacao) {
						programas.setCoordenador(operacao.getCoordenador());
						daoPrograma.alterar(programas);
					}

					for (Funcionario funcionario : operacaoBuscada.getFuncionarios()) {
						if (!operacao.getFuncionarios().contains(funcionario)) {
							for (Programa programa : programaOperacao) {
								programa.getFuncionarios().remove(funcionario);
								daoPrograma.alterar(programa);
							}
						}
					}

					for (Funcionario funcionario : operacao.getFuncionarios()) {
						if (!operacaoBuscada.getFuncionarios().contains(funcionario)) {
							for (Programa programa : programaOperacao) {
								programa.getFuncionarios().add(funcionario);
								daoPrograma.alterar(programa);
							}
						}
					}

					for (Funcionario funcionario : operacaoBuscada.getFuncionarios()) {
						if (!operacao.getFuncionarios().contains(funcionario)) {
							if (!operacao.getFuncionarios().isEmpty()) {
								for (Programa programa : programaOperacao) {
									for (Treinamento treinamento : programa.getTreinamentos()) {
										Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(funcionario.getId(),
												treinamento.getId());

										if (aplicacao != null) {
											daoAplicacao.remover(aplicacao.getId());
										}

										List<Historico> historico = daoHistorico
												.historicosExcluirFuncionario(funcionario.getId(), id);

										if (historico != null) {
											for (Historico historicos : historico) {
												daoHistorico.remover(historicos.getId());
											}

										}

									}
								}
							}
						}
					}

					for (Funcionario funcionario : operacao.getFuncionarios()) {
						Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), id);
						if (!operacaoBuscada.getFuncionarios().contains(funcionario)) {
							for (Programa programa : programaOperacao) {
								for (Treinamento treinamento : programa.getTreinamentos()) {
									Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(funcionario.getId(),
											treinamento.getId());
									if (aplicacao != null) {
										aplicacao.setFuncionario(funcionario);
										aplicacao.setTreinamento(treinamento);
										aplicacao.setAprovado(Aprovado.PENDENTE);

										if (funcionario.getDataAdmissao() != null && programa.getDataInsercao() != null
												&& programa.getDataInsercao().getTime() > funcionario.getDataAdmissao()
														.getTime()) {
											aplicacao.setDataInicio(programa.getDataInsercao());
										} else if (funcionario.getDataAdmissao() != null
												&& programa.getDataInsercao() != null && programa.getDataInsercao()
														.getTime() < funcionario.getDataAdmissao().getTime()) {
											aplicacao.setDataInicio(funcionario.getDataAdmissao());
										} else {
											aplicacao.setDataInicio(new Date());
										}
										daoAplicacao.alterar(aplicacao);

									} else {

										Aplicacao novaAplicacao = new Aplicacao();
										novaAplicacao.setFuncionario(funcionario);
										novaAplicacao.setTreinamento(treinamento);

										if (funcionario.getDataAdmissao() != null && programa.getDataInsercao() != null
												&& programa.getDataInsercao().getTime() > funcionario.getDataAdmissao()
														.getTime()) {
											novaAplicacao.setDataInicio(programa.getDataInsercao());
										} else if (funcionario.getDataAdmissao() != null
												&& programa.getDataInsercao() != null && programa.getDataInsercao()
														.getTime() < funcionario.getDataAdmissao().getTime()) {
											novaAplicacao.setDataInicio(funcionario.getDataAdmissao());
										} else {
											novaAplicacao.setDataInicio(new Date());
										}
										daoAplicacao.inserir(novaAplicacao);
									}
								}

								if (historico != null) {
									Calendar dataInicio = Calendar.getInstance();
									dataInicio.setTime(historico.getDataConclusao());
									dataInicio.add(Calendar.MONTH, programa.getPrazoReciclagem());
									historico.setDataInicio(dataInicio.getTime());
									historico.setFuncionario(funcionario);
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(historico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, programa.getDuracao());
									historico.setDataConclusao(dataConclusao.getTime());
									historico.setPrograma(programa);
									daoHistorico.alterar(historico);

									HistoricosAntigos antigos = new HistoricosAntigos();
									antigos.setFuncionario(funcionario);
									antigos.setDataInicio(new Date());
									antigos.setDataConclusao(historico.getDataConclusao());
									antigos.setPrograma(programa);
									daoHistoricosAntigos.inserir(antigos);

								} else {
									Historico novoHistorico = new Historico();
									novoHistorico.setFuncionario(funcionario);
									novoHistorico.setDataInicio(new Date());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(novoHistorico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, programa.getDuracao());
									novoHistorico.setDataConclusao(dataConclusao.getTime());
									novoHistorico.setPrograma(programa);
									daoHistorico.inserir(novoHistorico);

									HistoricosAntigos antigos = new HistoricosAntigos();
									antigos.setFuncionario(funcionario);
									antigos.setDataInicio(novoHistorico.getDataInicio());
									antigos.setDataConclusao(novoHistorico.getDataConclusao());
									antigos.setPrograma(programa);
									daoHistoricosAntigos.inserir(antigos);

								}

							}
						}
					}
				}

				operacao.setId(id);
				daoOperacao.alterar(operacao);
				return ResponseEntity.status(HttpStatus.OK).body(operacao);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (

		DataIntegrityViolationException e) {
			return ResponseEntity.status(564).body(null);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Operacao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/adicionarCoord", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Operacao> alterarOperacaoCoodernador(@RequestBody Operacao operacao,
			HttpServletRequest request) {
		String[] descripitacaoToken = interceptor(request);

		// Verificando se o token est� expirado
		if (descripitacaoToken[0].equals("565")) {
			return ResponseEntity.status(565).body(null);
		}

		// Pega o tipo do funcionario enviado pelo token
		Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

		if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
			try {

				Operacao operacaoBuscada = daoOperacao.buscar(operacao.getId());
				operacaoBuscada.setCoordenador(operacao.getCoordenador());

				daoOperacao.alterar(operacaoBuscada);

				return ResponseEntity.status(HttpStatus.OK).body(operacao);
			} catch (ConstraintViolationException e) {
				return new ResponseEntity<Operacao>(HttpStatus.BAD_REQUEST);
			} catch (Exception e) {
				return new ResponseEntity<Operacao>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
	}

	@RequestMapping(value = "/coordena/{idFuncionario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Operacao>> listarOperacoesCoordenadas(@PathVariable Long idFuncionario,
			HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Busca opera��o pelo id do funcionario enviado pela url
			List<Operacao> operacoesBuscadas = daoOperacao.operacoesFuncionarioCoordena(idFuncionario);
			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || idDoFuncionario == idFuncionario) {
				return ResponseEntity.status(HttpStatus.OK).body(operacoesBuscadas);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/funcionario/{idFuncionario}/participa", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Operacao> listarOperacaoParticipa(@PathVariable Long idFuncionario,
			HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || idDoFuncionario == idFuncionario) {
				return ResponseEntity.status(HttpStatus.OK)
						.body(daoOperacao.operacoesFuncionarioParticipa(idDoFuncionario));
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/grafico/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Grafico> graficoOperacao(@PathVariable Long id, HttpServletRequest request) {

		String[] descripitacaoToken = interceptor(request);

		// Verificando se o token est� expirado
		if (descripitacaoToken[0].equals("565")) {
			return ResponseEntity.status(565).body(null);
		}

		// Pega o id do funcionario enviado pelo token
		Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

		// Pega o tipo do funcionario enviado pelo token
		Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

		// Busca opera��o pelo id enviado pela url
		Operacao operacao = daoOperacao.buscar(id);

		if (tipoDoFuncionario == Tipo.ADMINISTRADOR || idDoFuncionario == operacao.getCoordenador().getId()) {

			List<Programa> programas = daoPrograma.programasOperacao(id);

			if (programas.size() > 0) {
				Grafico grafico = new Grafico();
				grafico.setQuantFuncionarios(operacao.getFuncionarios().size());
				grafico.setOperacao(operacao);

				for (Programa programa : programas) {
					List<Historico> historicoAprovados = new ArrayList<>();
					List<Historico> historicoReprovados = new ArrayList<>();
					List<Historico> historicoPendentes = new ArrayList<>();
					for (Funcionario funcionario : programa.getFuncionarios()) {
						Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), programa.getId());
						if (historico != null) {
							if (historico.getSituacao() == Aprovado.APROVADO) {
								historicoAprovados.add(historico);
								programa.setAprovados(historicoAprovados.size());
							} else if (historico.getSituacao() == Aprovado.PENDENTE) {
								historicoPendentes.add(historico);
								programa.setPendentes(historicoPendentes.size());

							} else if (historico.getSituacao() == Aprovado.REPROVADO) {
								historicoReprovados.add(historico);
								programa.setReprovados(historicoReprovados.size());
							}

						} else {
							continue;
						}
					}
				}
				grafico.setProgramas(programas);
				return ResponseEntity.status(HttpStatus.OK).body(grafico);
			} else {
				return new ResponseEntity<Grafico>(HttpStatus.NO_CONTENT);
			}

		} else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}

	}
}
