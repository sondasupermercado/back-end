package br.com.sondait.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.dao.DAOAvaliacaoReacao;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOTurma;
import br.com.sondait.model.AvaliacaoDeReacao;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.Tipo;
import br.com.sondait.model.Turma;

@CrossOrigin
@RequestMapping("/avaliacaoReacao")
@RestController
public class ControllerAvaliacaoDeReacao {

	@Autowired
	@Qualifier("JPAAvaliacaoReacao")
	private DAOAvaliacaoReacao daoAvaliacaoReacao;

	@Autowired
	@Qualifier("JPATurma")
	private DAOTurma daoTurma;
	
	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;
	

	@Autowired
	private ServletContext servletContext;

	private String token;
   
	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informações do token que veio do front
			token = request.getHeader("Authorization");
			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informações do token para um map para fazer compações
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] {"565"};
		} catch (Exception e) {
			return null;
		}
	}

	/*
	 * Upload da avaliação para o servidor e gravar no banco
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity<AvaliacaoDeReacao> salvaAvaliacao(@RequestParam("nome") String nome,
			@RequestParam("turma") String turma, @RequestParam("file") MultipartFile file, HttpServletRequest request) {
		try {
			request.setCharacterEncoding("UTF-8");
			String[] descripitacaoToken = interceptor(request);
			
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}
			
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);
			
			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);
			
			

			String caminho = "files/avaliacaoReacao";
			String caminhoAbsoluto = servletContext.getRealPath(caminho);
			String nomeArquivo = "";
			
			Turma turmaDAAvalicao = daoTurma.buscar(Long.parseLong(turma.trim()));
			
			Programa programaDaturma = turmaDAAvalicao.getTreinamento().getPrograma();
			
			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR
					|| programaDaturma.getCoordenador() != null
					&& programaDaturma.getCoordenador().getId() == idDoFuncionario
			|| programaDaturma.getCoordenador() == null && !operacoesCoordena.isEmpty() ) {
				if (!file.isEmpty()) {
					byte[] bytes = file.getBytes();
					nomeArquivo = nome.replace(" ", "_") + "_" + turma + ".pdf";
					BufferedOutputStream stream = new BufferedOutputStream(
							new FileOutputStream(caminhoAbsoluto + File.separator + nomeArquivo));
					stream.write(bytes);
					stream.close();
				}

				AvaliacaoDeReacao avaliacao = new AvaliacaoDeReacao();
				avaliacao.setTurma(daoTurma.buscar(Long.parseLong(turma.trim())));
				avaliacao.setNome(nome.trim());
				avaliacao.setCaminho(caminho + "/" + nomeArquivo);
				daoAvaliacaoReacao.inserir(avaliacao);

				return ResponseEntity.created(URI.create("/avaliacao" + avaliacao.getId())).body(avaliacao);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<AvaliacaoDeReacao>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<AvaliacaoDeReacao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * Buscar no banco todas as avaliações de um programa
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<AvaliacaoDeReacao> buscaAvaliacoes(@PathVariable Long id) {
		return daoAvaliacaoReacao.avaliacoesTurma(id);
	}

}
