package br.com.sondait.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sondait.dao.DAOHistoricosAntigos;
import br.com.sondait.model.HistoricosAntigos;

@RestController
@RequestMapping("/historicosAntigos")
@CrossOrigin
public class ControllerHistoricosAntigos {
	
	@Autowired
	@Qualifier("JPAHistoricosAntigos")
	private DAOHistoricosAntigos daoHistoricosAntigos;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<HistoricosAntigos> historicosAntigos(@PathVariable Long id){
		return daoHistoricosAntigos.historicosFuncionario(id);
	}
}
