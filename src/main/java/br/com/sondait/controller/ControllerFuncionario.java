package br.com.sondait.controller;

import java.beans.PropertyEditorSupport;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.PersistenceException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.apache.commons.mail.EmailConstants;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.dao.DAOCargo;
import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.dao.DAOHistoricosAntigos;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.dao.FuncionarioDAO;
import br.com.sondait.gerador.GerarXLS;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Cargo;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Historico;
import br.com.sondait.model.HistoricosAntigos;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;
import br.com.sondait.model.RedefinirSenha;
import br.com.sondait.model.Relatorio;
import br.com.sondait.model.Tipo;
import br.com.sondait.model.Treinamento;
import br.com.sondait.util.Reader;

@CrossOrigin // Serve para fazer um acesso cross em dois servidores(servidor
				// back e servido front por exemplo)
@RequestMapping("/funcionario")
@RestController
public class ControllerFuncionario {

	@Autowired
	@Qualifier("JPAFuncionario")
	private DAOFuncionario daoFuncionario;

	@Autowired
	@Qualifier("JPACargo")
	private DAOCargo daoCargo;

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	@Autowired
	@Qualifier("JPAHistorico")
	private DAOHistorico daoHistorico;

	@Autowired
	@Qualifier("JPAPrograma")
	private DAOPrograma daoPrograma;

	@Autowired
	@Qualifier("JPAHistoricosAntigos")
	private DAOHistoricosAntigos daoHistoricosAntigos;

	@Autowired
	@Qualifier("JPAAplicacao")
	private DAOAplicacao daoAplicacao;

	@Autowired
	private FuncionarioDAO funcionarioDao;

	@Autowired
	private ServletContext servletContext;

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	private String token = null;

	private String regexSenha = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&()_\\-])[A-Za-z\\d$@$!%*#?&()_\\-]{8,}$";

	public static final String SECRET = "S0nda_132";

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");
			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			return null;
		}
	}

	@InitBinder
	public void binder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			public void setAsText(String value) {
				if (value != null) {
					try {
						setValue(new SimpleDateFormat("yyyy-MM-dd").parse(value));
					} catch (ParseException e) {
						setValue(null);
						e.printStackTrace();
					}
				}
			}

			public String getAsText() {
				if (getValue() != null) {
					return new SimpleDateFormat("yyyy-MM-dd").format((Date) getValue());
				}
				return null;
			}
		});
	}

	/*
	 * A��es
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Funcionario> logar(@RequestBody Funcionario funcionario) {
		funcionario = daoFuncionario.logar(funcionario);

		if (funcionario != null) {
			HashMap<String, Object> claims = new HashMap<>();
			claims.put("id_funcionario", funcionario.getId());
			claims.put("nome_funcionario", funcionario.getNome());
			claims.put("tipo", funcionario.getTipo());

			// Hora atual em segundos
			long horaAtual = System.currentTimeMillis() / 1000;

			Calendar horaExpiracao = Calendar.getInstance();
			horaExpiracao.set(Calendar.HOUR_OF_DAY, 23);
			horaExpiracao.set(Calendar.MINUTE, 59);
			horaExpiracao.set(Calendar.SECOND, 59);

			claims.put("iat", horaAtual);
			claims.put("exp", horaExpiracao.getTimeInMillis() / 1000);

			JWTSigner signer = new JWTSigner(SECRET);
			funcionario.setApiToken(signer.sign(claims));

			return ResponseEntity.ok(funcionario);
		}

		return new ResponseEntity<Funcionario>(HttpStatus.UNAUTHORIZED);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Funcionario> criarFuncionario(@RequestBody Funcionario funcionario,
			HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				funcionario.setCargo(daoCargo.buscar(funcionario.getCargo().getId()));

				daoFuncionario.inserir(funcionario);

				for (Programa programasCargo : daoPrograma.ProgramasDoCargo(funcionario.getCargo().getId())) {
					programasCargo.getFuncionarios().add(funcionario);
					daoPrograma.alterar(programasCargo);

					if (funcionario.getDataAdmissao().after(programasCargo.getDataInsercao())) {
						for (Treinamento treinamentos : programasCargo.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionario);
							aplicacao.setDataInicio(funcionario.getDataAdmissao());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionario);
						historico.setDataInicio(funcionario.getDataAdmissao());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(programasCargo);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(funcionario.getDataAdmissao());
						Calendar dataConclusaoAntigos = Calendar.getInstance();
						dataConclusaoAntigos.setTime(historico.getDataInicio());
						dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
						antigos.setDataConclusao(dataConclusaoAntigos.getTime());
						antigos.setPrograma(programasCargo);
						daoHistoricosAntigos.inserir(antigos);
					} else {
						for (Treinamento treinamentos : programasCargo.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionario);
							aplicacao.setDataInicio(programasCargo.getDataInsercao());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionario);
						historico.setDataInicio(programasCargo.getDataInsercao());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(programasCargo);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(programasCargo.getDataInsercao());
						Calendar dataConclusaoAntigos = Calendar.getInstance();
						dataConclusaoAntigos.setTime(historico.getDataInicio());
						dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
						antigos.setDataConclusao(dataConclusaoAntigos.getTime());
						antigos.setPrograma(programasCargo);
						daoHistoricosAntigos.inserir(antigos);
					}

				}

				for (Programa programasParaTodos : daoPrograma.BuscarProgramasParaTodos()) {
					programasParaTodos.getFuncionarios().add(funcionario);
					daoPrograma.alterar(programasParaTodos);

					if (funcionario.getDataAdmissao().after(programasParaTodos.getDataInsercao())) {
						for (Treinamento treinamentos : programasParaTodos.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionario);
							aplicacao.setDataInicio(funcionario.getDataAdmissao());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionario);
						historico.setDataInicio(funcionario.getDataAdmissao());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(programasParaTodos);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(funcionario.getDataAdmissao());
						Calendar dataConclusaoAntigos = Calendar.getInstance();
						dataConclusaoAntigos.setTime(historico.getDataInicio());
						dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
						antigos.setDataConclusao(dataConclusaoAntigos.getTime());
						antigos.setPrograma(programasParaTodos);
						daoHistoricosAntigos.inserir(antigos);
					} else {
						for (Treinamento treinamentos : programasParaTodos.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionario);
							aplicacao.setDataInicio(programasParaTodos.getDataInsercao());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionario);
						historico.setDataInicio(programasParaTodos.getDataInsercao());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(programasParaTodos);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionario);
						antigos.setDataInicio(programasParaTodos.getDataInsercao());
						Calendar dataConclusaoAntigos = Calendar.getInstance();
						dataConclusaoAntigos.setTime(historico.getDataInicio());
						dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
						antigos.setDataConclusao(dataConclusaoAntigos.getTime());
						antigos.setPrograma(programasParaTodos);
						daoHistoricosAntigos.inserir(antigos);
					}
				}

				return ResponseEntity.created(URI.create("/funcionario/" + funcionario.getId())).body(funcionario);
			} else {
				return new ResponseEntity<Funcionario>(HttpStatus.UNAUTHORIZED);
			}
		} catch (PersistenceException e) {
			return ResponseEntity.status(560).body(null);
		} catch (Exception e) {
			return new ResponseEntity<Funcionario>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/excel", method = RequestMethod.POST)
	public ResponseEntity<List<Object>> criarFuncionarioExcel(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) throws IOException {

		try {

			String caminho = servletContext.getRealPath("files");

			if (!file.isEmpty()) {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(caminho + File.separator + "arquivo.csv"));
				stream.write(bytes);
				stream.close();

			} else {
				return ResponseEntity.status(562).body(null);
			}

			List<Funcionario> funcionarios = new ArrayList<>();
			String line = "";

			// Cadastrar

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {

				BufferedReader brEmail = new BufferedReader(new FileReader(caminho + File.separator + "arquivo.csv"));
				List<Object> emails = new ArrayList<>();
				int ignorarLinha = 1;
				while ((line = brEmail.readLine()) != null) {

					if (ignorarLinha == 1) {
						ignorarLinha++;
					} else {

						String[] row = line.split(";");
						String email = row[1];

						if (daoFuncionario.buscarEmail(email) != null) {
							emails.add(email);
						} else {
							continue;
						}
					}
				}

				brEmail.close();
				if (emails.size() > 0) {
					return ResponseEntity.status(560).body(emails);
				}

				BufferedReader brCargo = new BufferedReader(new FileReader(caminho + File.separator + "arquivo.csv"));
				ignorarLinha = 1;
				while ((line = brCargo.readLine()) != null) {

					if (ignorarLinha == 1) {
						ignorarLinha++;
					} else {

						String[] row = line.split(";");
						String cargoFuncionario = row[4];

						Cargo cargo = daoCargo.buscarPeloNomeDesativo(cargoFuncionario);
						if (cargo != null) {
							daoCargo.ativar(cargo.getId());
						} else {
							continue;
						}
					}
				}

				brCargo.close();
				
				
				
				

				BufferedReader brOperacao = new BufferedReader(
						new FileReader(caminho + File.separator + "arquivo.csv"));
				ignorarLinha = 1;
				while ((line = brOperacao.readLine()) != null) {

					if (ignorarLinha == 1) {
						ignorarLinha++;
					} else {

						String[] row = line.split(";");
						String operacaoDoFuncionario = row[3];

						Operacao operacaoExcel = daoOperacao.buscarPeloNomeDesativos(operacaoDoFuncionario);
						if (operacaoExcel != null) {
							daoOperacao.ativar(operacaoExcel);
						} else {
							continue;
						}

					}
				}
				brOperacao.close();

				BufferedReader br = new BufferedReader(new FileReader(caminho + File.separator + "arquivo.csv"));
				ignorarLinha = 1;
				SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
				while ((line = br.readLine()) != null) {

					if (ignorarLinha == 1) {

						ignorarLinha++;
					} else {

						// "," ou ";" de acordo com o arquivo
						String[] row = line.split(";");
						String nome = row[0];
						String email = row[1];
						String operacaoDoFuncionario = row[3];
						String dataAdmissao = row[2];
						String cargoFuncionario = row[4];

						Funcionario funcionario = new Funcionario();

						funcionario.setNome(nome);
						funcionario.setEmail(email.toLowerCase());
						funcionario.setTipo(Tipo.COMUM);

						Date data = fmt.parse(dataAdmissao);
						funcionario.setDataAdmissao(data);

						Cargo cargoBuscado = daoCargo.buscarPeloNome(cargoFuncionario.toLowerCase());

						if (cargoBuscado == null) {
							Cargo cargo = new Cargo();
							cargo.setNome(cargoFuncionario.toLowerCase());
							funcionario.setCargo(cargo);
							daoCargo.inserir(cargo);
							cargoBuscado = cargo;
						} else {
							funcionario.setCargo(cargoBuscado);
						}

						daoFuncionario.inserir(funcionario);
						funcionarios.add(funcionario);

						System.out.println("Uma porra: " + cargoBuscado);
						System.out.println("Uma porra id: " + cargoBuscado.getId());
						List<Programa> programaCargo = daoPrograma.ProgramasDoCargo(cargoBuscado.getId());
						if (!programaCargo.isEmpty()) {
							System.out.println("Tem programa do cargo" + programaCargo.size());
							for (Programa programasCargo : programaCargo) {
								programasCargo.getFuncionarios().add(funcionario);
								daoPrograma.alterar(programasCargo);

								if (funcionario.getDataAdmissao().after(programasCargo.getDataInsercao())) {
									for (Treinamento treinamentos : programasCargo.getTreinamentos()) {
										Aplicacao aplicacao = new Aplicacao();
										aplicacao.setFuncionario(funcionario);
										aplicacao.setDataInicio(funcionario.getDataAdmissao());
										aplicacao.setTreinamento(treinamentos);
										daoAplicacao.inserir(aplicacao);
									}

									Historico historico = new Historico();
									historico.setFuncionario(funcionario);
									historico.setDataInicio(funcionario.getDataAdmissao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(historico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
									historico.setDataConclusao(dataConclusao.getTime());
									historico.setPrograma(programasCargo);
									daoHistorico.inserir(historico);

									HistoricosAntigos antigos = new HistoricosAntigos();
									antigos.setFuncionario(funcionario);
									antigos.setDataInicio(funcionario.getDataAdmissao());
									Calendar dataConclusaoAntigos = Calendar.getInstance();
									dataConclusaoAntigos.setTime(historico.getDataInicio());
									dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
									antigos.setDataConclusao(dataConclusaoAntigos.getTime());
									antigos.setPrograma(programasCargo);
									daoHistoricosAntigos.inserir(antigos);
								} else {
									for (Treinamento treinamentos : programasCargo.getTreinamentos()) {
										Aplicacao aplicacao = new Aplicacao();
										aplicacao.setFuncionario(funcionario);
										aplicacao.setDataInicio(programasCargo.getDataInsercao());
										aplicacao.setTreinamento(treinamentos);
										daoAplicacao.inserir(aplicacao);
									}

									Historico historico = new Historico();
									historico.setFuncionario(funcionario);
									historico.setDataInicio(programasCargo.getDataInsercao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(historico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
									historico.setDataConclusao(dataConclusao.getTime());
									historico.setPrograma(programasCargo);
									daoHistorico.inserir(historico);

									HistoricosAntigos antigos = new HistoricosAntigos();
									antigos.setFuncionario(funcionario);
									antigos.setDataInicio(programasCargo.getDataInsercao());
									Calendar dataConclusaoAntigos = Calendar.getInstance();
									dataConclusaoAntigos.setTime(historico.getDataInicio());
									dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
									antigos.setDataConclusao(dataConclusaoAntigos.getTime());
									antigos.setPrograma(programasCargo);
									daoHistoricosAntigos.inserir(antigos);
								}
							}
						}

						List<Programa> programaTodos = daoPrograma.BuscarProgramasParaTodos();
						if (!programaTodos.isEmpty()) {
							for (Programa programasParaTodos : programaTodos) {
								programasParaTodos.getFuncionarios().add(funcionario);
								daoPrograma.alterar(programasParaTodos);

								if (funcionario.getDataAdmissao().after(programasParaTodos.getDataInsercao())) {
									for (Treinamento treinamentos : programasParaTodos.getTreinamentos()) {
										Aplicacao aplicacao = new Aplicacao();
										aplicacao.setFuncionario(funcionario);
										aplicacao.setDataInicio(funcionario.getDataAdmissao());
										aplicacao.setTreinamento(treinamentos);
										daoAplicacao.inserir(aplicacao);
									}

									Historico historico = new Historico();
									historico.setFuncionario(funcionario);
									historico.setDataInicio(funcionario.getDataAdmissao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(historico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
									historico.setDataConclusao(dataConclusao.getTime());
									historico.setPrograma(programasParaTodos);
									daoHistorico.inserir(historico);

									HistoricosAntigos antigos = new HistoricosAntigos();
									antigos.setFuncionario(funcionario);
									antigos.setDataInicio(funcionario.getDataAdmissao());
									Calendar dataConclusaoAntigos = Calendar.getInstance();
									dataConclusaoAntigos.setTime(historico.getDataInicio());
									dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
									antigos.setDataConclusao(dataConclusaoAntigos.getTime());
									antigos.setPrograma(programasParaTodos);
									daoHistoricosAntigos.inserir(antigos);
								} else {
									for (Treinamento treinamentos : programasParaTodos.getTreinamentos()) {
										Aplicacao aplicacao = new Aplicacao();
										aplicacao.setFuncionario(funcionario);
										aplicacao.setDataInicio(programasParaTodos.getDataInsercao());
										aplicacao.setTreinamento(treinamentos);
										daoAplicacao.inserir(aplicacao);
									}

									Historico historico = new Historico();
									historico.setFuncionario(funcionario);
									historico.setDataInicio(programasParaTodos.getDataInsercao());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(historico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
									historico.setDataConclusao(dataConclusao.getTime());
									historico.setPrograma(programasParaTodos);
									daoHistorico.inserir(historico);

									HistoricosAntigos antigos = new HistoricosAntigos();
									antigos.setFuncionario(funcionario);
									antigos.setDataInicio(programasParaTodos.getDataInsercao());
									Calendar dataConclusaoAntigos = Calendar.getInstance();
									dataConclusaoAntigos.setTime(historico.getDataInicio());
									dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
									antigos.setDataConclusao(dataConclusaoAntigos.getTime());
									antigos.setPrograma(programasParaTodos);
									daoHistoricosAntigos.inserir(antigos);
								}

							}
						}

						Operacao operacaoBuscada = daoOperacao.buscarPeloNome(operacaoDoFuncionario.toLowerCase());
						if (operacaoBuscada == null) {
							Operacao operacao = new Operacao();
							operacao.setNome(operacaoDoFuncionario);
							Set<Funcionario> operacaoFuncionarios = new HashSet<>();
							operacaoFuncionarios.add(funcionario);
							operacao.setFuncionarios(operacaoFuncionarios);
							daoOperacao.inserir(operacao);
						} else {

							List<Programa> programaOperacao = daoPrograma.programasOperacao(operacaoBuscada.getId());

							if (!operacaoBuscada.getFuncionarios().contains(funcionario)) {
								for (Programa programa : programaOperacao) {
									programa.getFuncionarios().add(funcionario);
									daoPrograma.alterar(programa);
								}
							}

							if (!operacaoBuscada.getFuncionarios().contains(funcionario)) {
								for (Programa programa : programaOperacao) {
									for (Treinamento treinamento : programa.getTreinamentos()) {

										Aplicacao novaAplicacao = new Aplicacao();
										novaAplicacao.setFuncionario(funcionario);
										novaAplicacao.setTreinamento(treinamento);

										if (funcionario.getDataAdmissao() != null && programa.getDataInsercao() != null
												&& programa.getDataInsercao().getTime() > funcionario.getDataAdmissao()
														.getTime()) {
											novaAplicacao.setDataInicio(programa.getDataInsercao());
										} else if (funcionario.getDataAdmissao() != null
												&& programa.getDataInsercao() != null && programa.getDataInsercao()
														.getTime() < funcionario.getDataAdmissao().getTime()) {
											novaAplicacao.setDataInicio(funcionario.getDataAdmissao());
										} else {
											novaAplicacao.setDataInicio(new Date());
										}
										daoAplicacao.inserir(novaAplicacao);
									}

									Historico novoHistorico = new Historico();
									novoHistorico.setFuncionario(funcionario);
									novoHistorico.setDataInicio(new Date());
									Calendar dataConclusao = Calendar.getInstance();
									dataConclusao.setTime(novoHistorico.getDataInicio());
									dataConclusao.add(Calendar.DAY_OF_YEAR, programa.getDuracao());
									novoHistorico.setDataConclusao(dataConclusao.getTime());
									novoHistorico.setPrograma(programa);
									daoHistorico.inserir(novoHistorico);

									HistoricosAntigos antigos = new HistoricosAntigos();
									antigos.setFuncionario(funcionario);
									antigos.setDataInicio(novoHistorico.getDataInicio());
									antigos.setDataConclusao(novoHistorico.getDataConclusao());
									antigos.setPrograma(programa);
									daoHistoricosAntigos.inserir(antigos);

								}

							}

							operacaoBuscada.getFuncionarios().add(funcionario);
							daoOperacao.alterar(operacaoBuscada);
						}

					}
				}

				File arquivoExcluir = new File(caminho + File.separator + "arquivo");
				arquivoExcluir.delete();
				br.close();

				return new ResponseEntity<List<Object>>(HttpStatus.CREATED);
			} else {
				return new ResponseEntity<List<Object>>(HttpStatus.UNAUTHORIZED);
			}
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			return ResponseEntity.status(500).body(null);
		} catch (ParseException e) {
			e.printStackTrace();
			return ResponseEntity.status(567).body(null);
		} catch (PersistenceException e) {
			e.printStackTrace();
			return ResponseEntity.status(560).body(null);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/relatorio/{idFuncionario}", method = RequestMethod.POST)
	public ResponseEntity<ObjectNode> gerarRelatorioXlsIndividual(@PathVariable Long idFuncionario,
			HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				Funcionario funcionario = daoFuncionario.buscar(idFuncionario);
				Operacao operacao = daoOperacao.operacoesFuncionarioParticipa(idFuncionario);

				Relatorio relatorio = new Relatorio();
				relatorio.setFuncionario(funcionario);
				relatorio.setOperacao(operacao);

				String arquivo = GerarXLS.criarExcelIndividual(relatorio, servletContext.getRealPath("files"));

				ObjectMapper mapper = new ObjectMapper();
				ObjectNode vaila = mapper.createObjectNode();
				vaila.put("caminho", arquivo);

				if (arquivo != null) {
					return new ResponseEntity<ObjectNode>(vaila, HttpStatus.OK);
				} else {
					return new ResponseEntity<ObjectNode>(HttpStatus.BAD_REQUEST);
				}
			} else {
				return new ResponseEntity<ObjectNode>(HttpStatus.UNAUTHORIZED);
			}

		} catch (PersistenceException e) {
			return ResponseEntity.status(560).body(null);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<ObjectNode>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Funcionario>> buscarTodos(HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				return ResponseEntity.ok(daoFuncionario.buscarAtivos());
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/participando/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Funcionario>> buscarTodosParticipando(@PathVariable Long id,
			HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				return ResponseEntity.ok(funcionarioDao.buscarFuncionariosDaOp(id));
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/participando", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Funcionario>> buscarTodosParticipando(HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				return ResponseEntity.ok(funcionarioDao.buscarFuncionariosDaOp());
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<List<Funcionario>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Funcionario> buscar(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Busca funcionario
			Funcionario funcionarioBuscado = daoFuncionario.buscar(id);

			if (funcionarioBuscado != null) {
				funcionarioBuscado.setOperacao(daoOperacao.operacoesFuncionarioParticipa(id));
			}

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || idDoFuncionario == funcionarioBuscado.getId()) {
				return ResponseEntity.ok(funcionarioBuscado);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Funcionario> alterarFuncionario(@RequestBody Funcionario funcionarioAlterar,
			@PathVariable Long id, HttpServletRequest request) {

		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || idDoFuncionario == id) {
				Funcionario funAlterarEmail = daoFuncionario.buscar(id);
				String email = funAlterarEmail.getEmail();
				String senha = funAlterarEmail.getSenha();

				Funcionario funcionarioAtual = daoFuncionario.buscar(id);

				for (Funcionario funcionario : daoFuncionario.buscarTodos()) {
					if (funcionario.getEmail().trim().toLowerCase()
							.equals(funcionarioAlterar.getEmail().trim().toLowerCase())
							&& funcionario.isAtivo() == false && funcionarioAtual.getId() != funcionario.getId()) {
						funAlterarEmail.setCargo(daoCargo.buscar(funAlterarEmail.getCargo().getId()));
						funAlterarEmail.setEmail(email);
						funAlterarEmail.setCriptografar(senha);
						daoFuncionario.alterar(funAlterarEmail);
						return ResponseEntity.status(564).body(null);
					} else if (funcionario.getEmail().trim().toLowerCase()
							.equals(funcionarioAlterar.getEmail().trim().toLowerCase()) && funcionario.isAtivo() == true
							&& funcionarioAtual.getId() != funcionario.getId()) {

						return ResponseEntity.status(560).body(null);
					}
				}

				if (funcionarioAtual.getCargo().getId() != funcionarioAlterar.getCargo().getId()) {
					for (Programa programas : daoPrograma.ProgramasDoFuncionarioporCargo(id)) {
						if (programas.getFuncionarios().contains(funcionarioAtual)) {
							programas.getFuncionarios().remove(funcionarioAtual);
							daoPrograma.alterar(programas);

							Historico historicoRemover = daoHistorico.historicoFuncionario(funcionarioAtual.getId(),
									programas.getId());

							if (historicoRemover != null) {
								daoHistorico.remover(historicoRemover.getId());
							}

							for (Treinamento treinamentoPrograma : programas.getTreinamentos()) {

								Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(id,
										treinamentoPrograma.getId());

								if (aplicacao != null) {
									daoAplicacao.remover(aplicacao.getId());
								}

							}

						}

						for (Programa programasCargo : daoPrograma
								.ProgramasDoCargo(funcionarioAlterar.getCargo().getId())) {

							programasCargo.getFuncionarios().add(funcionarioAtual);
							daoPrograma.alterar(programasCargo);

							if (funcionarioAlterar.getDataAdmissao().after(programasCargo.getDataInsercao())) {
								for (Treinamento treinamentos : programasCargo.getTreinamentos()) {
									Aplicacao aplicacao = new Aplicacao();
									aplicacao.setFuncionario(funcionarioAtual);
									aplicacao.setDataInicio(funcionarioAlterar.getDataAdmissao());
									aplicacao.setTreinamento(treinamentos);
									daoAplicacao.inserir(aplicacao);
								}

								Historico historico = new Historico();
								historico.setFuncionario(funcionarioAtual);
								historico.setDataInicio(funcionarioAlterar.getDataAdmissao());
								Calendar dataConclusao = Calendar.getInstance();
								dataConclusao.setTime(historico.getDataInicio());
								dataConclusao.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
								historico.setDataConclusao(dataConclusao.getTime());
								historico.setPrograma(programasCargo);
								daoHistorico.inserir(historico);

								HistoricosAntigos antigos = new HistoricosAntigos();
								antigos.setFuncionario(funcionarioAtual);
								antigos.setDataInicio(funcionarioAlterar.getDataAdmissao());
								Calendar dataConclusaoAntigos = Calendar.getInstance();
								dataConclusaoAntigos.setTime(historico.getDataInicio());
								dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
								antigos.setDataConclusao(dataConclusaoAntigos.getTime());
								antigos.setPrograma(programasCargo);
								daoHistoricosAntigos.inserir(antigos);
							} else {
								for (Treinamento treinamentos : programasCargo.getTreinamentos()) {
									Aplicacao aplicacao = new Aplicacao();
									aplicacao.setFuncionario(funcionarioAtual);
									aplicacao.setDataInicio(programasCargo.getDataInsercao());
									aplicacao.setTreinamento(treinamentos);
									daoAplicacao.inserir(aplicacao);
								}

								Historico historico = new Historico();
								historico.setFuncionario(funcionarioAtual);
								historico.setDataInicio(programasCargo.getDataInsercao());
								Calendar dataConclusao = Calendar.getInstance();
								dataConclusao.setTime(historico.getDataInicio());
								dataConclusao.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
								historico.setDataConclusao(dataConclusao.getTime());
								historico.setPrograma(programasCargo);
								daoHistorico.inserir(historico);

								HistoricosAntigos antigos = new HistoricosAntigos();
								antigos.setFuncionario(funcionarioAtual);
								antigos.setDataInicio(programasCargo.getDataInsercao());
								Calendar dataConclusaoAntigos = Calendar.getInstance();
								dataConclusaoAntigos.setTime(historico.getDataInicio());
								dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
								antigos.setDataConclusao(dataConclusaoAntigos.getTime());
								antigos.setPrograma(programasCargo);
								daoHistoricosAntigos.inserir(antigos);
							}
						}
					}

				}

				if (funcionarioAtual.isAtivo() == false) {

					for (Programa programasCargo : daoPrograma
							.ProgramasDoCargo(funcionarioAlterar.getCargo().getId())) {
						programasCargo.getFuncionarios().add(funcionarioAtual);
						daoPrograma.alterar(programasCargo);

						for (Treinamento treinamentos : programasCargo.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionarioAtual);
							aplicacao.setDataInicio(new Date());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionarioAtual);
						historico.setDataInicio(new Date());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(programasCargo);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionarioAtual);
						antigos.setDataInicio(new Date());
						Calendar dataConclusaoAntigos = Calendar.getInstance();
						dataConclusaoAntigos.setTime(historico.getDataInicio());
						dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasCargo.getDuracao());
						antigos.setDataConclusao(dataConclusaoAntigos.getTime());
						antigos.setPrograma(programasCargo);
						daoHistoricosAntigos.inserir(antigos);
					}

					for (Programa programasParaTodos : daoPrograma.BuscarProgramasParaTodos()) {
						programasParaTodos.getFuncionarios().add(funcionarioAtual);
						daoPrograma.alterar(programasParaTodos);

						for (Treinamento treinamentos : programasParaTodos.getTreinamentos()) {
							Aplicacao aplicacao = new Aplicacao();
							aplicacao.setFuncionario(funcionarioAtual);
							aplicacao.setDataInicio(new Date());
							aplicacao.setTreinamento(treinamentos);
							daoAplicacao.inserir(aplicacao);
						}

						Historico historico = new Historico();
						historico.setFuncionario(funcionarioAtual);
						historico.setDataInicio(new Date());
						Calendar dataConclusao = Calendar.getInstance();
						dataConclusao.setTime(historico.getDataInicio());
						dataConclusao.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
						historico.setDataConclusao(dataConclusao.getTime());
						historico.setPrograma(programasParaTodos);
						daoHistorico.inserir(historico);

						HistoricosAntigos antigos = new HistoricosAntigos();
						antigos.setFuncionario(funcionarioAtual);
						antigos.setDataInicio(new Date());
						Calendar dataConclusaoAntigos = Calendar.getInstance();
						dataConclusaoAntigos.setTime(historico.getDataInicio());
						dataConclusaoAntigos.add(Calendar.DAY_OF_YEAR, programasParaTodos.getDuracao());
						antigos.setDataConclusao(dataConclusaoAntigos.getTime());
						antigos.setPrograma(programasParaTodos);
						daoHistoricosAntigos.inserir(antigos);
					}

				}

				funcionarioAlterar.setId(id);
				funcionarioAlterar.setCargo(daoCargo.buscar(funcionarioAlterar.getCargo().getId()));
				funcionarioAlterar.setCriptografar(senha);
				daoFuncionario.alterar(funcionarioAlterar);

				return ResponseEntity.status(HttpStatus.OK).body(funcionarioAlterar);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Funcionario>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Funcionario>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> desativar(@PathVariable Long id, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Deleta Funcionario
			Funcionario funcionarioDeletar = daoFuncionario.buscar(id);
			if (tipoDoFuncionario == Tipo.ADMINISTRADOR && id != idDoFuncionario) {

				if (daoOperacao.operacoesFuncionarioCoordena(id).size() > 0
						|| daoPrograma.funcionarioCoordena(id).size() > 0) {
					return ResponseEntity.status(566).body(null);
				}

				for (Historico historico : daoHistorico.historico(id)) {
					daoHistorico.remover(historico.getId());
				}

				for (Aplicacao aplicacao : daoAplicacao.aplicacacoesFuncionario(id)) {
					daoAplicacao.remover(aplicacao.getId());
				}

				for (Programa programas : daoPrograma.funcionarioProgramaParticipaAtivoEDesativos(id)) {
					programas.getFuncionarios().remove(funcionarioDeletar);
					daoPrograma.alterar(programas);
				}

				if (daoOperacao.operacoesFuncionarioParticipa(id) != null) {
					Operacao operacaoDeletar = daoOperacao.operacoesFuncionarioParticipa(id);
					operacaoDeletar.getFuncionarios().remove(funcionarioDeletar);
					daoOperacao.alterar(operacaoDeletar);
				}

				daoFuncionario.deletar(funcionarioDeletar.getId());

				return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/novaSenha", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> esqueciSenha(@RequestBody Funcionario funcionario) throws EmailException {
		try {

			Funcionario funcionarioBuscado = daoFuncionario.buscarEmail(funcionario.getEmail());

			// Data exata do sistema
			Date dataAtual = new Date();

			// Codigo gerado para redefinir senha
			String codigoSistema = String.valueOf(dataAtual.getTime());
			RedefinirSenha redefinir = new RedefinirSenha();

			// Corpo basico para enviar email
			HtmlEmail email = new HtmlEmail();
			email.setHostName("smtp.gmail.com");
			email.setAuthentication("sondaittreinamentos@gmail.com", "S0nda_132");
			email.setSmtpPort(587);
			email.setStartTLSEnabled(true);
			email.setStartTLSRequired(true);
			email.setFrom("sondaittreinamentos@gmail.com", "Sonda Treinamentos");
			email.setCharset(EmailConstants.UTF_8);

			String logoId = email
					.embed(new File(servletContext.getRealPath("files/template_email/sondaitlogo-branco.png")));

			// Gerar Codigo, salvar no banco e enviar por email ao funcionario
			if (funcionarioBuscado != null) {
				RedefinirSenha registro = daoFuncionario.buscarCodigo(funcionario.getEmail());

				if (registro == null) {
					redefinir.setCodigo(codigoSistema.substring(7, 13));

					Calendar expiracao = Calendar.getInstance();
					expiracao.add(Calendar.DAY_OF_MONTH, 1);

					redefinir.setDataExpiracao(expiracao.getTime());
					redefinir.setFuncionario(daoFuncionario.emailCadastrado(funcionario.getEmail()));

					daoFuncionario.inserirCodigo(redefinir);

					// Inserindo codigo no corpo do email
					email.addTo(funcionario.getEmail() + funcionario.getNome());

					try {
						String paginaEmail = Reader
								.readFile(servletContext.getRealPath("files/template_email/emailSenha.html"));
						String[] emailPart = paginaEmail.split("!");
						email.setHtmlMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
								+ funcionarioBuscado.getNome().split(" ")[0] + emailPart[2] + redefinir.getCodigo()
								+ emailPart[3]).setSubject("Sistema de Gest�o de Treinamento - Redefinir Senha");
						email.send();

						return new ResponseEntity<Void>(HttpStatus.OK);
					} catch (IOException e1) {
						e1.printStackTrace();
					}catch (Exception e) {
						e.printStackTrace();
					}

					email.setMsg("O seu codigo para redefinir senha  " + redefinir.getCodigo()).setSubject("Codigo");
					email.send();

					return new ResponseEntity<Void>(HttpStatus.OK);

					// Se o usuario solicitar novamente um codigo depois da data
					// de
					// expiracao do codigo
				} else if (registro != null && registro.getDataExpiracao().getTime() < dataAtual.getTime()) {

					registro.setCodigo(codigoSistema.substring(7, 13));

					Calendar expiracao = Calendar.getInstance();
					expiracao.add(Calendar.DAY_OF_MONTH, 1);

					registro.setDataExpiracao(expiracao.getTime());
					daoFuncionario.alterarCodigo(registro);

					// Inserindo codigo no corpo do email
					email.addTo(funcionario.getEmail(), "Sr. " + funcionario.getNome());

					try {
						String paginaEmail = Reader
								.readFile(servletContext.getRealPath("files/template_email/emailSenha.html"));
						String[] emailPart = paginaEmail.split("!");
						email.setHtmlMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
								+ funcionarioBuscado.getNome().split(" ")[0] + emailPart[2] + registro.getCodigo()
								+ emailPart[3]).setSubject("Sistema de Gest�o de Treinamento - Redefinir Senha");

						email.send();

						return new ResponseEntity<Void>(HttpStatus.OK);
					} catch (IOException e1) {

						e1.printStackTrace();
					}catch (Exception e) {
						e.printStackTrace();
					}

					email.setMsg("O seu codigo para redefinir senha  " + registro.getCodigo()).setSubject("Codigo");
					email.send();

					return new ResponseEntity<Void>(HttpStatus.OK);

				} else if (registro != null && registro.getDataExpiracao().getTime() > dataAtual.getTime()) {

					// Retorna o codigo que ainda esta valido no banco
					email.addTo(funcionario.getEmail(), "Sr. " + funcionario.getNome());

					try {
						String paginaEmail = Reader
								.readFile(servletContext.getRealPath("files/template_email/emailSenha.html"));
						String[] emailPart = paginaEmail.split("!");
						email.setHtmlMsg(emailPart[0] + "cid:" + logoId + emailPart[1]
								+ funcionarioBuscado.getNome().split(" ")[0] + emailPart[2] + registro.getCodigo()
								+ emailPart[3]).setSubject("Sistema de Gest�o de Treinamento - Redefinir Senha");
						email.send();

						return new ResponseEntity<Void>(HttpStatus.OK);
					} catch (IOException e1) {
						e1.printStackTrace();
					}catch (Exception e) {
						e.printStackTrace();
					}

					email.setMsg("O seu codigo para redefinir senha  " + registro.getCodigo()).setSubject("Codigo");
					email.send();

					return new ResponseEntity<Void>(HttpStatus.OK);
				} else {
					return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
				}

			} else {
				return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
			}

		} catch (Exception e) {
			
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/redefinirSenha", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> redefinirSenha(@RequestBody RedefinirSenha redefinirSenhaDados) {
		RedefinirSenha registro = daoFuncionario.buscarCodigoRedefinir(redefinirSenhaDados.getCodigo());

		if (redefinirSenhaDados.getSenha().matches(regexSenha)) {
			if (registro != null) {
				Funcionario funcionarioRegistro = daoFuncionario.emailCadastrado(registro.getFuncionario().getEmail());
				funcionarioRegistro.setSenha(redefinirSenhaDados.getSenha());
				daoFuncionario.alterar(funcionarioRegistro);

				RedefinirSenha alterarSenhaRegistro = daoFuncionario
						.buscarCodigoRedefinir(redefinirSenhaDados.getCodigo());
				alterarSenhaRegistro.setSenha(redefinirSenhaDados.getSenha());
				daoFuncionario.alterarCodigo(alterarSenhaRegistro);

				return new ResponseEntity<Void>(HttpStatus.OK);
			} else {
				return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
			}
		} else {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/desativados", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Funcionario> funcionariosDesativados() {
		return daoFuncionario.buscarDesativos();
	}

	@RequestMapping(value = "/desativadosRepetido", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Funcionario funcionario(@RequestBody Funcionario funcionario) {
		return daoFuncionario.buscarEmail(funcionario.getEmail());
	}

}
