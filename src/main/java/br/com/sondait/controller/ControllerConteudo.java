package br.com.sondait.controller;

import java.beans.PropertyEditorSupport;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTVerifier;

import br.com.sondait.dao.DAOConteudo;
import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.model.Conteudo;
import br.com.sondait.model.Operacao;
import br.com.sondait.model.Tipo;

@CrossOrigin // Serve para fazer um acesso cross em dois servidores(servidor
				// back e servido front por exemplo)
@RequestMapping("/conteudo")
@RestController
public class ControllerConteudo {

	// DAO para ser utilizado
	@Autowired
	@Qualifier("JPAConteudo")
	private DAOConteudo daoConteudo;

	@Autowired
	@Qualifier("JPAOperacao")
	private DAOOperacao daoOperacao;

	String token = null;

	private String[] interceptor(HttpServletRequest request) {
		try {
			// Pegando informa��es do token que veio do front
			token = request.getHeader("Authorization");
			// Criando um verificador e trazendo a senha do controller para
			// descriptografar o token
			JWTVerifier verifier = new JWTVerifier(ControllerFuncionario.SECRET);

			// Passando as informa��es do token para um map para fazer compa��es
			Map<String, Object> claims = verifier.verify(token);

			// Pega o id do funcionario enviado pelo token
			String idString = claims.get("id_funcionario").toString();

			// Pega o tipo do funcionario enviado pelo token
			String tipoDeUsuario = claims.get("tipo").toString();

			return new String[] { idString, tipoDeUsuario };
		} catch (JWTExpiredException e) {
			return new String[] { "565" };
		} catch (Exception e) {
			return null;
		}
	}

	@InitBinder
	public void binder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			public void setAsText(String value) {
				if (value != null) {
					try {
						setValue(new SimpleDateFormat("yyyy-MM-dd").parse(value));
					} catch (ParseException e) {
						setValue(null);
						e.printStackTrace();
					}
				}
			}

			public String getAsText() {
				if (getValue() != null) {
					return new SimpleDateFormat("yyyy-MM-dd").format((Date) getValue());
				}
				return null;
			}
		});
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Conteudo> criarConteudo(@RequestBody Conteudo conteudo, HttpServletRequest request) {
		try {
			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR) {
				for (Conteudo conteudoComparar : daoConteudo.buscarTodos()) {
					if (conteudo.getNome().toLowerCase().equals(conteudoComparar.getNome().toLowerCase())
							&& conteudoComparar.isAtivo() == false) {
						conteudo.setId(conteudoComparar.getId());
						daoConteudo.alterar(conteudo);
						return ResponseEntity.status(HttpStatus.OK).body(conteudo);
					} else if (conteudo.getNome().toLowerCase().equals(conteudoComparar.getNome().toLowerCase())
							&& conteudoComparar.isAtivo() == true) {
						return ResponseEntity.status(560).body(null);
					}
				}
				daoConteudo.inserir(conteudo);
				return ResponseEntity.created(URI.create("/conteudo/" + conteudo.getId())).body(conteudo);

			} else {
				return new ResponseEntity<Conteudo>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Conteudo>(HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Conteudo>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Conteudo>> buscarConteudos(HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Lista para saber se ele coordena algum
			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || !(operacoesCoordena != null)) {
				return ResponseEntity.ok(daoConteudo.buscarAtivos());
			} else {
				return new ResponseEntity<List<Conteudo>>(HttpStatus.UNAUTHORIZED);
			}
		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Conteudo>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Conteudo>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/desativados", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Conteudo>> buscarConteudosDesativados(HttpServletRequest request) {

		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Lista para saber se ele coordena algum
			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || !(operacoesCoordena != null)) {
				return ResponseEntity.ok(daoConteudo.buscarTodosDesativos());
			} else {
				return new ResponseEntity<List<Conteudo>>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<List<Conteudo>>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Conteudo>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> desativar(@PathVariable Long id, HttpServletRequest request) {
		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Lista para saber se ele coordena algum
			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || !(operacoesCoordena != null)) {
				if (daoConteudo.treinamentosConteudo(id).size() > 0) {
					return ResponseEntity.status(566).body(null);
				} else {
					daoConteudo.deletar(id);
					return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
				}
			} else {
				return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Conteudo> buscarConteudo(@PathVariable Long id, HttpServletRequest request) {

		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Lista para saber se ele coordena algum
			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || !(operacoesCoordena != null)) {
				return ResponseEntity.ok(daoConteudo.buscar(id));
			} else {
				return new ResponseEntity<Conteudo>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			return new ResponseEntity<Conteudo>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Conteudo>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Conteudo> alterarConteudo(@PathVariable Long id, @RequestBody Conteudo conteudoAlterar,
			HttpServletRequest request) {

		try {

			String[] descripitacaoToken = interceptor(request);

			// Verificando se o token est� expirado
			if (descripitacaoToken[0].equals("565")) {
				return ResponseEntity.status(565).body(null);
			}

			// Pega o tipo do funcionario enviado pelo token
			Tipo tipoDoFuncionario = Tipo.valueOf(descripitacaoToken[1]);

			// Pega o id do funcionario enviado pelo token
			Long idDoFuncionario = Long.parseLong(descripitacaoToken[0]);

			// Lista para saber se ele coordena algum
			List<Operacao> operacoesCoordena = daoOperacao.operacoesFuncionarioCoordena(idDoFuncionario);

			if (tipoDoFuncionario == Tipo.ADMINISTRADOR || !(operacoesCoordena != null)) {
				conteudoAlterar.setId(id);
				daoConteudo.alterar(conteudoAlterar);
				return ResponseEntity.status(HttpStatus.OK).body(conteudoAlterar);
			} else {
				return new ResponseEntity<Conteudo>(HttpStatus.UNAUTHORIZED);
			}
		} catch (DataIntegrityViolationException e) {
			return ResponseEntity.status(564).body(null);

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Conteudo>(HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Conteudo>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
