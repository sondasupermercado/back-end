package br.com.sondait.gerador;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Aprovado;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.Historico;
import br.com.sondait.model.Programa;
import br.com.sondait.model.Relatorio;
import br.com.sondait.model.Treinamento;

@Component
public class GerarXLS {
	private static DAOPrograma daoPrograma;
	private static DAOHistorico daoHistorico;
	private static DAOAplicacao daoAplicacao;

	@Autowired
	public GerarXLS(DAOPrograma daoProg, DAOHistorico daoHist, DAOAplicacao daoApl) {
		daoPrograma = daoProg;
		daoHistorico = daoHist;
		daoAplicacao = daoApl;
	}

	// M�todo para gerar relat�rio em excel para apenas um funcionario
	public static String criarExcelIndividual(Relatorio relatorio, String nomeArquivo) {
		try {
			// Nome do arquivo
			String nome = File.separator + "Relat�rio" + relatorio.getFuncionario().getNome().replace(" ", "") + ".xls";
			nomeArquivo += nome;

			// Cria��o do nome da planilha
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet planilha = workbook.createSheet(relatorio.getFuncionario().getNome());

			HSSFPalette palette = workbook.getCustomPalette();
			palette.setColorAtIndex(HSSFColor.PALE_BLUE.index, (byte) 222, (byte) 236, (byte) 247);

			// Editando fonte do cabecalho
			HSSFFont fonteCabecalho = workbook.createFont();
			fonteCabecalho.setFontName("Calibri");
			fonteCabecalho.setFontHeightInPoints((short) 14);
			fonteCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			fonteCabecalho.setColor(HSSFColor.WHITE.index);

			// Editando formata��o do cabecalho
			HSSFCellStyle estiloCabecalho = workbook.createCellStyle();
			estiloCabecalho.setAlignment(HSSFCellStyle.ALIGN_CENTER_SELECTION);
			estiloCabecalho.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
			estiloCabecalho.setFillPattern(CellStyle.SOLID_FOREGROUND);
			estiloCabecalho.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			estiloCabecalho.setFont(fonteCabecalho);
			// Configurando borda
			estiloCabecalho.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderTop(HSSFCellStyle.BORDER_THIN);

			// Primeira linha da planilha
			HSSFRow cabecalho = planilha.createRow((short) 0);
			cabecalho.createCell(0).setCellValue("Nome do Funcion�rio");
			cabecalho.createCell(1).setCellValue("Email do Funcion�rio");
			if (relatorio.getOperacao() != null) {
				cabecalho.createCell(2).setCellValue("Opera��o");
				cabecalho.createCell(3).setCellValue("Programa");
				cabecalho.createCell(4).setCellValue("Status do Programa");
				cabecalho.createCell(5).setCellValue("Carga Hor�ria do Programa");
				cabecalho.createCell(6).setCellValue("Treinamento");
				cabecalho.createCell(7).setCellValue("Status do Treinamento");
				for (int i = 0; i <= 7; i++) {
					cabecalho.getCell(i).setCellStyle(estiloCabecalho);
				}
			} else {
				cabecalho.createCell(2).setCellValue("Programa");
				cabecalho.createCell(3).setCellValue("Status do Programa");
				cabecalho.createCell(4).setCellValue("Carga Hor�ria do Programa");
				cabecalho.createCell(5).setCellValue("Treinamento");
				cabecalho.createCell(6).setCellValue("Status do Treinamento");
				for (int i = 0; i <= 6; i++) {
					cabecalho.getCell(i).setCellStyle(estiloCabecalho);
				}
			}

			cabecalho.setHeight((short) 600);

			// Lista de programas usada para saber quantos programas tem
			List<Programa> listaProgramas = daoPrograma
					.funcionarioProgramaParticipa(relatorio.getFuncionario().getId());

			// N�mero da linha que ser� adicionado as informa��es
			int i = 0;

			HSSFFont fontePlanilha = workbook.createFont();
			fontePlanilha.setFontName("Calibri");
			fontePlanilha.setFontHeightInPoints((short) 12);

			HSSFCellStyle estiloPlanilha = workbook.createCellStyle();
			estiloPlanilha.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			estiloPlanilha.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
			estiloPlanilha.setFillPattern(CellStyle.SOLID_FOREGROUND);
			estiloPlanilha.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			estiloPlanilha.setFont(fontePlanilha);
			// Editando Bordas
			estiloPlanilha.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloPlanilha.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloPlanilha.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloPlanilha.setBorderTop(HSSFCellStyle.BORDER_THIN);

			// Preenchendo cada linha da planilha
			for (Programa programa : listaProgramas) {
				Historico historico = daoHistorico.historicoFuncionario(relatorio.getFuncionario().getId(),
						programa.getId());
				for (Treinamento treinamento : programa.getTreinamentos()) {
					i++;
					HSSFRow linha = planilha.createRow((short) i);
					linha.createCell(0).setCellValue(relatorio.getFuncionario().getNome());
					linha.createCell(1).setCellValue(relatorio.getFuncionario().getEmail());
					if (relatorio.getOperacao() != null) {
						linha.createCell(2).setCellValue(relatorio.getOperacao().getNome());
						linha.createCell(3).setCellValue(programa.getNome());

						// Teste para mostrar a situa��o
						if (historico != null && historico.isAtivo() && historico.getSituacao() == Aprovado.APROVADO) {
							linha.createCell(4).setCellValue("Aprovado");
						} else if (historico != null && historico.isAtivo()
								&& historico.getSituacao() == Aprovado.REPROVADO) {
							linha.createCell(4).setCellValue("Reprovado");
						} else {
							linha.createCell(4).setCellValue("Pendente");
						}

						int cargaHorariadoPrograma = 0;
						for (Treinamento treinamento1 : programa.getTreinamentos()) {
							cargaHorariadoPrograma += treinamento1.getConteudo().getCargaHoraria();
						}
						linha.createCell(5).setCellValue(cargaHorariadoPrograma);

						linha.createCell(6).setCellValue(treinamento.getConteudo().getNome());

						Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(relatorio.getFuncionario().getId(),
								treinamento.getId());

						if (aplicacao != null && aplicacao.getAprovado() == Aprovado.APROVADO) {
							linha.createCell(7).setCellValue("Aprovado");
						} else if (aplicacao != null && aplicacao.getAprovado() == Aprovado.REPROVADO) {
							linha.createCell(7).setCellValue("Reprovado");
						} else {
							linha.createCell(7).setCellValue("Pendente");
						}

						for (int j = 0; j <= 7; j++) {
							linha.getCell(j).setCellStyle(estiloPlanilha);
						}
					} else {
						linha.createCell(2).setCellValue(programa.getNome());

						// Teste para mostrar a situa��o
						if (historico != null && historico.isAtivo() && historico.getSituacao() == Aprovado.APROVADO) {
							linha.createCell(3).setCellValue("Aprovado");
						} else if (historico != null && historico.isAtivo()
								&& historico.getSituacao() == Aprovado.REPROVADO) {
							linha.createCell(3).setCellValue("Reprovado");
						} else {
							linha.createCell(3).setCellValue("Pendente");
						}

						int cargaHorariadoPrograma = 0;
						for (Treinamento treinamento1 : programa.getTreinamentos()) {
							cargaHorariadoPrograma += treinamento1.getConteudo().getCargaHoraria();
						}
						linha.createCell(4).setCellValue(cargaHorariadoPrograma);

						linha.createCell(5).setCellValue(treinamento.getConteudo().getNome());

						Aplicacao aplicacao = daoAplicacao.aplicacoesTreinamento(relatorio.getFuncionario().getId(),
								treinamento.getId());

						if (aplicacao != null && aplicacao.getAprovado() == Aprovado.APROVADO) {
							linha.createCell(6).setCellValue("Aprovado");
						} else if (aplicacao != null && aplicacao.getAprovado() == Aprovado.REPROVADO) {
							linha.createCell(6).setCellValue("Reprovado");
						} else {
							linha.createCell(6).setCellValue("Pendente");
						}

						for (int j = 0; j <= 6; j++) {
							linha.getCell(j).setCellStyle(estiloPlanilha);
						}
					}

				}
			}

			for (int j = 0; j <= 7; j++) {
				planilha.autoSizeColumn(j);
			}

			// Local onde � salvo o xls
			FileOutputStream saida = new FileOutputStream(nomeArquivo);
			workbook.write(saida);
			saida.close();

			return "/files" + nome;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String criarExcelDaOperacao(Relatorio relatorio, String nomeArquivo) {
		try {

			// Nome do arquivo
			String nome = File.separator + "Relat�rio" + relatorio.getOperacao().getNome().replace(" ", "") + ".xls";
			nomeArquivo += nome;

			// Cria��o do nome da planilha
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet planilha = workbook.createSheet(relatorio.getOperacao().getNome());

			// Configurando cor para um azul fora dos padr�es da apache poi
			// utilizando RGB
			HSSFPalette palette = workbook.getCustomPalette();
			palette.setColorAtIndex(HSSFColor.PALE_BLUE.index, (byte) 222, (byte) 236, (byte) 247);

			// Editando fonte do cabecalho
			HSSFFont fonteCabecalho = workbook.createFont();
			fonteCabecalho.setFontName("Calibri");
			fonteCabecalho.setFontHeightInPoints((short) 14);
			fonteCabecalho.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			fonteCabecalho.setColor(HSSFColor.WHITE.index);

			// Editando formata��o do cabecalho
			HSSFCellStyle estiloCabecalho = workbook.createCellStyle();
			estiloCabecalho.setAlignment(HSSFCellStyle.ALIGN_CENTER_SELECTION);
			estiloCabecalho.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
			estiloCabecalho.setFillPattern(CellStyle.SOLID_FOREGROUND);
			estiloCabecalho.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			estiloCabecalho.setFont(fonteCabecalho);
			// Configurando borda
			estiloCabecalho.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloCabecalho.setBorderTop(HSSFCellStyle.BORDER_THIN);

			// Primeira linha da planilha
			HSSFRow cabecalho = planilha.createRow((short) 0);
			cabecalho.createCell(0).setCellValue("Nome do Funcion�rio");
			cabecalho.createCell(1).setCellValue("Email do Funcion�rio");
			cabecalho.createCell(2).setCellValue("Opera��o");
			cabecalho.createCell(3).setCellValue("Programa");
			cabecalho.createCell(4).setCellValue("Carga Hor�ria do Programa");
			cabecalho.createCell(5).setCellValue("Status do Programa");
			cabecalho.setHeight((short) 600);

			for (int cnt = 0; cnt <= 5; cnt++) {
				cabecalho.getCell(cnt).setCellStyle(estiloCabecalho);
			}

			// Lista de programas usada para saber quantos programas tem
			List<Programa> listaProgramas = new ArrayList<>();
			int i = 0;

			HSSFFont fontePlanilha = workbook.createFont();
			fontePlanilha.setFontName("Calibri");
			fontePlanilha.setFontHeightInPoints((short) 12);

			HSSFCellStyle estiloPlanilha = workbook.createCellStyle();
			estiloPlanilha.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			estiloPlanilha.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
			estiloPlanilha.setFillPattern(CellStyle.SOLID_FOREGROUND);
			estiloPlanilha.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			estiloPlanilha.setFont(fontePlanilha);
			// Editando Bordas
			estiloPlanilha.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			estiloPlanilha.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			estiloPlanilha.setBorderRight(HSSFCellStyle.BORDER_THIN);
			estiloPlanilha.setBorderTop(HSSFCellStyle.BORDER_THIN);

			// Lista de Funcion�rios
			for (Funcionario funcionario : relatorio.getOperacao().getFuncionarios()) {
				for (Programa programa : daoPrograma.funcionarioProgramaParticipa(funcionario.getId())) {
					listaProgramas.add(programa);

					// Preenchendo cada linha da planilha
					i++;
					Historico historico = daoHistorico.historicoFuncionario(funcionario.getId(), programa.getId());

					HSSFRow linha = planilha.createRow((short) i);
					linha.createCell(0).setCellValue(funcionario.getNome());
					linha.createCell(1).setCellValue(funcionario.getEmail());
					linha.createCell(2).setCellValue(relatorio.getOperacao().getNome());
					linha.createCell(3).setCellValue(programa.getNome());
					int cargaHorariadoPrograma = 0;
					for (Treinamento treinamento : programa.getTreinamentos()) {
						cargaHorariadoPrograma += treinamento.getConteudo().getCargaHoraria();
					}
					linha.createCell(4).setCellValue(cargaHorariadoPrograma);
					// Teste para mostrar a situa��o
					if (historico != null && historico.isAtivo() && historico.getSituacao() == Aprovado.APROVADO) {
						linha.createCell(5).setCellValue("Aprovado");
					} else if (historico != null && historico.isAtivo()
							&& historico.getSituacao() == Aprovado.REPROVADO) {
						linha.createCell(5).setCellValue("Reprovado");
					} else {
						linha.createCell(5).setCellValue("Pendente");
					}

					for (int j = 0; j <= 5; j++) {
						linha.getCell(j).setCellStyle(estiloPlanilha);
					}
				}
				for (int j = 0; j <= 5; j++) {
					planilha.autoSizeColumn(j);
				}
			}
			// Local onde � salvo o xls
			FileOutputStream saida = new FileOutputStream(nomeArquivo);
			workbook.write(saida);
			saida.close();

			return "files" + nome;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
}
