package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.Operacao;

public interface DAOOperacao extends DAOGenerico<Operacao> {
	public Operacao buscarPeloNome(String nome);

	public List<Operacao> operacoesFuncionarioCoordena(Long idFuncionario);

	public Operacao operacoesFuncionarioParticipa(Long id);
	
	public List<Operacao> operacoesDesativadas();
	
	public List<Operacao> buscarTodos();
	
	public List<Operacao> funcionarioCoordena(Long idFuncionario);
	
	public Operacao buscarPeloNomeDesativos(String nome);
	
	public void ativar(Operacao operacao);
}
