package br.com.sondait.dao;

import java.util.Date;
import java.util.List;

import br.com.sondait.model.HistoricosAntigos;

public interface DAOHistoricosAntigos extends DAOGenerico<HistoricosAntigos>{
	
	public List<HistoricosAntigos> historicosFuncionario(Long id);
	
	public HistoricosAntigos buscar(Long idFuncionario, Date dataInicio);
	
	public void alterarHistorigoAntigo(HistoricosAntigos obj);
	
	public HistoricosAntigos historicoFuncionario(Long idFuncionario, Long idPrograma);

	public void alterar(HistoricosAntigos antigos);
}
