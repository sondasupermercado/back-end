package br.com.sondait.dao.JPA;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOAplicacao;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Programa;

@Repository
public class JPAAplicacao implements DAOAplicacao {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Aplicacao> buscarAtivos() {
		// Buscando todas as Aplicações no bando
		TypedQuery<Aplicacao> tq = manager.createQuery("FROM Aplicacao a", Aplicacao.class);

		// Retornando a lista de aplicações
		return tq.getResultList();
	}

	@Override
	public Aplicacao buscar(Long id) {
		// Busca a aplicação no banco
		return manager.find(Aplicacao.class, id);

	}

	@Transactional
	@Override
	public void inserir(Aplicacao obj) {
		// Inserindo o objeto no banco
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Aplicacao obj) {
		// Alterando o objeto
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Long id) {

	}

	@Transactional
	@Override
	public List<Aplicacao> aplicacoesPrograma(Long idPrograma) {
		// Buscando todas as Aplicações no bando
		TypedQuery<Aplicacao> query = manager
				.createQuery("Select a from Aplicacao a where a.treinamento.programa.id =:id", Aplicacao.class);

		query.setParameter("id", idPrograma);

		// Retornando a lista de aplicações
		return query.getResultList();

	}

	@Override
	public List<Aplicacao> buscarTodos(Long idFuncionario) {
		// Buscando todas as Aplicações no bando
		TypedQuery<Aplicacao> tq = manager.createQuery("FROM Aplicacao a WHERE a.funcionario.id = :idFuncionario",
				Aplicacao.class);

		tq.setParameter("idFuncionario", idFuncionario);

		// Retornando a lista de aplicações
		return tq.getResultList();
	}

	@Override
	public List<Programa> funcionarioParticipa(Long id) {
		TypedQuery<Programa> query = manager
				.createQuery("Select p from Programa p join p.funcionarios f where f.id=:id", Programa.class);
		query.setParameter("id", id);
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Aplicacao> aplicacoesAprovadas(Long idPrograma, Long idFuncionario, Date dataInicio) {
		TypedQuery<Aplicacao> query = manager.createQuery(
				"Select a from Aplicacao a where a.dataInicio <:data and a.funcionario.id =:idFuncionario and a.treinamento.programa.id = :idPrograma",
				Aplicacao.class);
		query.setParameter("data", dataInicio);
		query.setParameter("idFuncionario", idFuncionario);
		query.setParameter("idPrograma", idPrograma);

		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public Aplicacao aplicacoesTreinamento(Long idFuncionario, Long idTreinamento) {
		TypedQuery<Aplicacao> query = manager.createQuery(
				"Select a from Aplicacao a where a.funcionario.id = :idFuncionario and a.treinamento.id =:idTreinamento",
				Aplicacao.class);
		query.setParameter("idFuncionario", idFuncionario);
		query.setParameter("idTreinamento", idTreinamento);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
		e.printStackTrace();
			return null;
		}

	}
	
	@Transactional
	@Override
	public void remover(Long id) {
		Aplicacao aplicacao = manager.find(Aplicacao.class, id);
		manager.remove(aplicacao);
	}

	@Override
	public Aplicacao aplicacaoPorProgramaEFuncionarios(Long idPrograma, Long idFuncionario) {
		TypedQuery<Aplicacao> query = manager.createQuery("Select new Aplicacao(id) from Aplicacao a where a.funcionario.id =:idFuncionario and a.treinamento.programa.id =:idPrograma ", Aplicacao.class);
		
		query.setMaxResults(1);
		query.setParameter("idPrograma", idPrograma);
		query.setParameter("idFuncionario", idFuncionario);
		
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Aplicacao> aplicacacoesFuncionario(Long id) {
		TypedQuery<Aplicacao> query = manager.createQuery("Select a from Aplicacao a where a.funcionario.id = :id", Aplicacao.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}


}
