package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOTurma;
import br.com.sondait.model.Turma;

@Repository
public class JPATurma implements DAOTurma {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Turma> buscarAtivos() {
		// Busca a lista de turmas no banco
		TypedQuery<Turma> tq = manager.createQuery("SELECT t FROM Turma t", Turma.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	@Override
	public Turma buscar(Long id) {
		// Busca e retorna o objeto requisitdo
		return manager.find(Turma.class, id);
	}

	@Transactional
	@Override
	public void inserir(Turma obj) {
		// Insere classe no banco
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Turma obj) {
		// Altera o objeto do banco
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Long id) {
		Turma turma = manager.find(Turma.class, id);
		manager.remove(turma);
	}
}
