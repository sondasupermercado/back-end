package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOOperacao;
import br.com.sondait.model.Operacao;

@Repository
public class JPAOperacao implements DAOOperacao {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Operacao> buscarAtivos() {
		// Busca a lista de classes no banco
		TypedQuery<Operacao> tq = manager.createQuery("FROM Operacao op WHERE op.ativo = true", Operacao.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	@Override
	public Operacao buscar(Long id) {
		// Busca e retorna o objeto requisitdo
		return manager.find(Operacao.class, id);
	}

	@Transactional
	@Override
	public void inserir(Operacao obj) {
		// Insere classe no banco
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Operacao obj) {
		// Altera o objeto do banco
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Long id) {
		// Busca o objeto pelo id
		Operacao operacao = manager.find(Operacao.class, id);

		// Altera a classe
		operacao.setAtivo(false);

		// Altera a classe no banco
		manager.merge(operacao);
	}

	@Override
	public Operacao buscarPeloNome(String nome) {
		TypedQuery<Operacao> query = manager
				.createQuery("Select o from Operacao o where o.nome =:nome and o.ativo = true", Operacao.class);
		query.setParameter("nome", nome);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Operacao> operacoesFuncionarioCoordena(Long idFuncionario) {
		TypedQuery<Operacao> query = manager.createQuery(
				"Select new Operacao(id,nome,coordenador)from Operacao o where o.coordenador.id=:idFuncionario and o.ativo=true",
				Operacao.class);
		query.setParameter("idFuncionario", idFuncionario);

		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public Operacao operacoesFuncionarioParticipa(Long id) {
		TypedQuery<Operacao> query = manager.createQuery(
				"SELECT o FROM Operacao o JOIN o.funcionarios f WHERE f.id = :idFuncionario", Operacao.class);
		query.setParameter("idFuncionario", id);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Operacao> operacoesDesativadas() {

		TypedQuery<Operacao> query = manager.createQuery("Select o from Operacao o where o.ativo = false",
				Operacao.class);
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Operacao> buscarTodos() {
		TypedQuery<Operacao> query = manager.createQuery("Select o from Operacao o", Operacao.class);
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Operacao> funcionarioCoordena(Long idFuncionario) {
		TypedQuery<Operacao> query = manager.createQuery(
				"Select o from Operacao o where o.coordenador.id=:idFuncionario and o.ativo=true", Operacao.class);
		query.setParameter("idFuncionario", idFuncionario);

		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Operacao buscarPeloNomeDesativos(String nome) {
		TypedQuery<Operacao> query = manager
				.createQuery("Select o from Operacao o where o.nome =:nome and o.ativo = false", Operacao.class);
		query.setParameter("nome", nome);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Transactional
	@Override
	public void ativar(Operacao operacao) {
		operacao.setAtivo(true);
		manager.merge(operacao);
	}

}
