package br.com.sondait.dao.JPA;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.sondait.dao.DAOAprovacao;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.util.Aprovacao;

@Repository
public class JPAAprovacao implements DAOAprovacao {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Aplicacao> aplicacoesDataInicio(Date dataInicio, Long id) {
		TypedQuery<Aplicacao> query = manager.createQuery("Select a from Aplicacao a where a.dataInicio < :data and a.funcionario.id =:id",
				Aplicacao.class);
		query.setParameter("id", id);
		query.setParameter("data", dataInicio);

		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public List<Aprovacao> buscarAtivos() {
		TypedQuery<Aprovacao> query = manager.createQuery("Select a from Aprovacao a", Aprovacao.class);
		return query.getResultList();
	}

	@Override
	public Aprovacao buscar(Long id) {
		return manager.find(Aprovacao.class, id);
	}

	@Override
	public void inserir(Aprovacao obj) {
		manager.persist(obj);
	}

	@Override
	public void alterar(Aprovacao obj) {
		manager.merge(obj);
	}

	@Override
	public void deletar(Long id) {
	}

	@Override
	public Aprovacao registroAprovacao(Long idFuncionario, Long idPrograma) {
		TypedQuery<Aprovacao> query = manager.createQuery("Select a from Aprovacao a join a.aplicacoes ap where a.funcionario.id = :idFuncionario and a.avaliacao is null and ap.treinamento.programa.id = :idPrograma", Aprovacao.class);
		query.setParameter("idFuncionario", idFuncionario);
		query.setParameter("idPrograma", idPrograma);
		
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	

}
