package br.com.sondait.dao.JPA;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOAvaliacao;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Avaliacao;

@Repository
public class JPAAvaliacao implements DAOAvaliacao {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Avaliacao> buscarAtivos() {
		// Busca no banco pelas avaliações ativas
		TypedQuery<Avaliacao> tq = manager.createQuery("FROM Avaliacao a WHERE a.ativo = true", Avaliacao.class);

		// Retorno
		return tq.getResultList();
	}

	@Override
	public Avaliacao buscar(Long id) {
		return manager.find(Avaliacao.class, id);
	}

	@Override
	@Transactional
	public void inserir(Avaliacao avaliacao) {
		manager.persist(avaliacao);
	}

	@Override
	@Transactional
	public void alterar(Avaliacao obj) {
		manager.merge(obj);
	}

	@Override
	@Transactional
	public void deletar(Long id) {
		Avaliacao avaliacao = manager.find(Avaliacao.class, id);
		avaliacao.setAtivo(false);
		manager.merge(avaliacao);
	}

	@Override
	public List<Aplicacao> aplicacoesPrograma(Long id) {
		TypedQuery<Aplicacao> query = manager.createQuery("Select a from Aplicacao a where a.treinamento.programa.id=:id", Aplicacao.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<Avaliacao> buscarAtivosPorPrograma(Long id) {		
		TypedQuery<Avaliacao> query = manager.createQuery("Select a from Avaliacao a where a.ativo = true and a.programa.id=:id", Avaliacao.class);
		query.setParameter("id", id);		
		return query.getResultList();
	}

	@Override
	public Avaliacao avaliacaoEntreDatas(Date dataEficacia , Date dataReciclagem, Long idPrograma, Long idFuncionario) {
		TypedQuery<Avaliacao> query = manager.createQuery("Select a from Avaliacao a where a.dataAtual > :dataEficacia and a.dataAtual < :dataReciclagem and a.funcionarioAvaliado.id = :idFuncionario and a.programa.id = :idPrograma", Avaliacao.class);
		query.setParameter("dataEficacia", dataEficacia);
		query.setParameter("dataReciclagem", dataReciclagem);
		query.setParameter("idFuncionario", idFuncionario);
		query.setParameter("idPrograma", idPrograma);
		
		
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Avaliacao> buscarAtivosPorProgramasEFuncionario(Long idPrograma, Long idFuncionario) {
		TypedQuery<Avaliacao> query = manager.createQuery("Select a from Avaliacao a where a.ativo = true and a.programa.id=:idPrograma and a.funcionarioAvaliado.id = :idFuncionario", Avaliacao.class);
		query.setParameter("idPrograma", idPrograma);		
		query.setParameter("idFuncionario", idFuncionario);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
