package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOConteudo;
import br.com.sondait.model.Conteudo;
import br.com.sondait.model.Treinamento;

@Repository
public class JPAConteudo implements DAOConteudo {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Conteudo> buscarAtivos() {
		// Busca a lista de conteudo no banco
		TypedQuery<Conteudo> tq = manager.createQuery("FROM Conteudo c WHERE c.ativo = true", Conteudo.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}
	
	@Override
	public List<Conteudo> buscarTodos() {
		// Busca a lista de conteudo no banco
		TypedQuery<Conteudo> tq = manager.createQuery("FROM Conteudo c", Conteudo.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}
	
	@Override
	public List<Conteudo> buscarTodosDesativos() {
		// Busca a lista de conteudo no banco
		TypedQuery<Conteudo> tq = manager.createQuery("FROM Conteudo c WHERE c.ativo = false", Conteudo.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	@Override
	public Conteudo buscar(Long id) {
		// Buscando e retornando a chamada requisitada
		return manager.find(Conteudo.class, id);
	}

	@Transactional
	@Override
	public void inserir(Conteudo obj) {
		// Incluindo o objeto no banco
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Conteudo obj) {
		// Busca e altera o objeto no banco
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Long id) {
		// Busca a chamada no banco
		Conteudo conteudo = manager.find(Conteudo.class, id);
		conteudo.setAtivo(false);
		manager.merge(conteudo);
	}

	@Override
	public List<Treinamento> treinamentosConteudo(Long id) {
		TypedQuery<Treinamento> query = manager.createQuery("Select t from Treinamento t where t.conteudo.id = :id", Treinamento.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
		
		
	}
}
