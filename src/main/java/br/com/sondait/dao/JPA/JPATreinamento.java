package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOTreinamento;
import br.com.sondait.model.Treinamento;

@Repository
public class JPATreinamento implements DAOTreinamento {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Treinamento> buscarAtivos() {
		// Busca a lista de classes no banco
		TypedQuery<Treinamento> tq = manager.createQuery("FROM Treinamento tr ", Treinamento.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	@Override
	public Treinamento buscar(Long id) {
		// Busca e retorna o objeto requisitdo
		return manager.find(Treinamento.class, id);
	}

	@Transactional
	@Override
	public void inserir(Treinamento obj) {
		// Insere classe no banco
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Treinamento obj) {
		// Altera o objeto do banco
		manager.merge(obj);
	}

	@Override
	public void deletar(Long id) {
	}

	@Override
	public List<Treinamento> treinamentosReciclagem(Long id) {
		TypedQuery<Treinamento> query = manager.createQuery("Select t from Treinamento t where t.reciclagem = true and t.programa.id = :id",
				Treinamento.class);
		
		query.setParameter("id", id);

		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<Treinamento> treinamentoPrograma(Long id) {
		TypedQuery<Treinamento> query = manager.createQuery("Select t from Treinamento t where t.programa.id = :id",
				Treinamento.class);

		query.setParameter("id", id);

		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			;
			return null;
		}
	}
}
