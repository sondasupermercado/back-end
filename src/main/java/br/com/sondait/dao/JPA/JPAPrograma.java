package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOPrograma;
import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Aprovado;
import br.com.sondait.model.Programa;

@Repository
public class JPAPrograma implements DAOPrograma {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Programa> buscarAtivos() {
		// Busca a lista de classes no banco
		TypedQuery<Programa> tq = manager.createQuery("FROM Programa prog WHERE prog.ativo = true", Programa.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	@Override
	public Programa buscar(Long id) {
		// Busca e retorna o objeto requisitdo
		return manager.find(Programa.class, id);
	}

	@Transactional
	@Override
	public void inserir(Programa obj) {
		// Insere classe no banco
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Programa obj) {
		if (obj.isAtivo() == false) {
			obj.setAtivo(true);
		}
		// Altera o objeto do bancos
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Long id) {
		// Busca o objeto pelo id
		Programa programa = manager.find(Programa.class, id);

		// Altera a classe
		programa.setAtivo(false);
		
		//Remover funcionarios 
		programa.getFuncionarios().clear();

		// Altera a classe no banco
		manager.merge(programa);
	}

	@Override
	public List<Aplicacao> aplicacoesPrograma(Long id) {
		TypedQuery<Aplicacao> query = manager.createQuery(
				"SELECT a FROM Aplicacao a WHERE a.treinamento.programa.id =:id and a.aprovado =:aprovado or a.aprovado =:pendente and a.avaliacaoRealizada = false",
				Aplicacao.class);
		query.setParameter("id", id);
		query.setParameter("aprovado", Aprovado.APROVADO);
		query.setParameter("pendente", Aprovado.PENDENTE);

		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	@Transactional
	public void alterarAplicacao(Aplicacao aplicacao) {
		manager.merge(aplicacao);
	}

	@Override
	public List<Programa> buscarTodos(Long idFuncionario) {
		// Busca a lista de classes no banco
		TypedQuery<Programa> tq = manager.createQuery(
				"SELECT prog FROM Programa prog WHERE prog.coordenador.id = :idFuncionario", Programa.class);

		// Definindo o parametro
		tq.setParameter("idFuncionario", idFuncionario);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	@Override
	public List<Programa> buscarDesativados() {
		// Busca a lista de conteudo no banco
		TypedQuery<Programa> tq = manager.createQuery("FROM Programa p WHERE p.ativo = false", Programa.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	@Override
	public List<Programa> buscarTodos() {
		TypedQuery<Programa> query = manager.createQuery("Select p From Programa p", Programa.class);
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<Programa> funcionarioProgramaParticipa(Long id) {
		TypedQuery<Programa> query = manager.createQuery("SELECT p FROM Programa p JOIN p.funcionarios f WHERE f.id = :id AND p.ativo = true",
				Programa.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	@Override
	public List<Programa> funcionarioProgramaParticipaAtivoEDesativos(Long id) {
		TypedQuery<Programa> query = manager.createQuery("SELECT p FROM Programa p JOIN p.funcionarios f WHERE f.id = :id",
				Programa.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public List<Programa> funcionarioCoordena(Long id) {
		TypedQuery<Programa> query = manager
				.createQuery("Select new Programa(p.id,p.nome) from Programa p where p.coordenador.id = :id and p.operacao is null and p.ativo=true ", Programa.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<Programa> funcionarioCoordenaGeral(Long id) {
		TypedQuery<Programa> query = manager
				.createQuery("Select p from Programa p where p.coordenador.id = :id and p.ativo=true ", Programa.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Programa> BuscarProgramasParaTodos() {
		TypedQuery<Programa> query = manager
				.createQuery("Select p from Programa p where p.operacao is null and p.cargo is null and p.ativo=true ", Programa.class);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Programa> programasOperacao(Long id) {
		TypedQuery<Programa> query = manager.createQuery("Select p from Programa p where p.operacao.id = :id and p.ativo = true", Programa.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	
	@Override
	public List<Programa> ProgramasDoCargo(Long id) {
		TypedQuery<Programa> query = manager.createQuery("Select p from Programa p where p.cargo.id = :id and p.ativo = true", Programa.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@Override
	public List<Programa> ProgramasDoFuncionarioporCargo(Long id) {
		TypedQuery<Programa> query = manager.createQuery("Select p from Programa p JOIN p.funcionarios f WHERE f.id = :id and p.cargo is not null and p.ativo = true", Programa.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
		
	}

	@Override
	public void remover(Programa programa) {
		manager.remove(programa);
		
	}
	@Override
	public List<Programa> programasReciclagem() {
		TypedQuery<Programa> query = manager.createQuery("Select p from Programa p where p.reciclagem = true",
				Programa.class);

		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}
}
