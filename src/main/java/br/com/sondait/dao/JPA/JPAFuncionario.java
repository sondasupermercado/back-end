package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOFuncionario;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.RedefinirSenha;

@Repository
public class JPAFuncionario implements DAOFuncionario {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Funcionario> buscarAtivos() {
		// Busca a lista de turmas no banco
		TypedQuery<Funcionario> tq = manager.createQuery("select func from Funcionario func WHERE func.ativo = true order by func.nome asc",
				// TypedQuery<Funcionario> tq = manager.createQuery("Select new
				// Funcionario(id, email) FROM Funcionario WHERE ativo = true",
				Funcionario.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	public List<Funcionario> buscarDesativos() {
		// Busca a lista de turmas no banco
		TypedQuery<Funcionario> tq = manager.createQuery("select func from Funcionario func WHERE func.ativo = false order by func.nome asc ",
				Funcionario.class);

		// Retorna a lista buscada no banco
		return tq.getResultList();
	}

	@Override
	public Funcionario buscar(Long id) {
		// Busca e retorna o objeto requisitdo
		return manager.find(Funcionario.class, id);
	}

	@Transactional
	@Override
	public void inserir(Funcionario obj) {
		// Insere classe no banco
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Funcionario obj) {
		if (obj.isAtivo() == false) {
			obj.setAtivo(true);
		}

		// Altera o objeto do banco
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Long id) {
		// Busca o objeto pelo id
		Funcionario func = manager.find(Funcionario.class, id);

		// Altera a classe
		func.setAtivo(false);

		// Altera a classe no banco
		manager.merge(func);
	}

	@Override
	public Funcionario logar(Funcionario func) {
		TypedQuery<Funcionario> query = manager.createQuery("select func from Funcionario func WHERE func.senha = :senha AND func.email = :email AND func.ativo = TRUE", Funcionario.class);

		// Pega as informações necessárias no objeto
		query.setParameter("email", func.getEmail().toLowerCase());
		query.setParameter("senha", func.getSenha());
		
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public Funcionario emailCadastrado(String email) {
		TypedQuery<Funcionario> query = manager.createQuery("Select f from Funcionario f where f.email=:email",
				Funcionario.class);
		query.setParameter("email", email);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Transactional
	@Override
	public void inserirCodigo(RedefinirSenha redefinir) {
		manager.persist(redefinir);
	}

	@Override
	public RedefinirSenha buscarCodigo(String email) {
		TypedQuery<RedefinirSenha> query = manager
				.createQuery("Select r from RedefinirSenha r where r.funcionario.email = :email", RedefinirSenha.class);
		query.setParameter("email", email);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public RedefinirSenha buscarCodigoRedefinir(String codigo) {
		TypedQuery<RedefinirSenha> query = manager.createQuery("Select r from RedefinirSenha r where r.codigo=:codigo",
				RedefinirSenha.class);
		query.setParameter("codigo", codigo);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Transactional
	@Override
	public void alterarCodigo(RedefinirSenha redefinir) {
		manager.merge(redefinir);
	}

	@Override
	@Transactional
	public void atualizandoToken(Funcionario func) {
		String hql = "UPDATE Funcionario SET apiToken = :apiToken WHERE id = :id";

		Query query = manager.createQuery(hql);
		query.setParameter("apiToken", func.getApiToken());
		query.setParameter("id", func.getId());
		query.executeUpdate();
	}

	@Override
	public List<Funcionario> funcioriosCargo(Long id) {
		TypedQuery<Funcionario> query = manager.createQuery("Select f from Funcionario f where f.cargo.id = :id",
				Funcionario.class);
		query.setParameter("id", id);

		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Funcionario> buscarTodos() {
		TypedQuery<Funcionario> query = manager.createQuery("Select f from Funcionario f", Funcionario.class);
		return query.getResultList();
	}

	@Override
	public Funcionario buscarEmail(String email) {
		// Busca a lista de turmas no banco
		TypedQuery<Funcionario> tq = manager.createQuery("FROM Funcionario func WHERE func.ativo = true and func.email = :email",
				Funcionario.class);
		tq.setParameter("email", email);
		// Retorna a lista buscada no banco		
		try {
			return tq.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
}
