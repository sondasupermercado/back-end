package br.com.sondait.dao.JPA;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOHistoricosAntigos;
import br.com.sondait.model.HistoricosAntigos;

@Repository
public class JPAHistoricosAntigos implements DAOHistoricosAntigos {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<HistoricosAntigos> buscarAtivos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HistoricosAntigos buscar(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	@Override
	public void inserir(HistoricosAntigos obj) {
		manager.persist(obj);

	}

	@Transactional
	@Override
	public void alterar(HistoricosAntigos obj) {
		manager.merge(obj);

	}

	@Transactional
	@Override
	public void alterarHistorigoAntigo(HistoricosAntigos obj) {

		if (obj.isAtivo() == false) {
			obj.setAtivo(true);
		}
		manager.merge(obj);
	}

	@Override
	public void deletar(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<HistoricosAntigos> historicosFuncionario(Long id) {
		TypedQuery<HistoricosAntigos> query = manager.createQuery(
				"Select h from HistoricosAntigos h where h.funcionario.id = :idFuncionario and h.ativo =true ",
				HistoricosAntigos.class);
		query.setParameter("idFuncionario", id);

		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public HistoricosAntigos buscar(Long idFuncionario, Date dataInicio) {
		TypedQuery<HistoricosAntigos> query = manager.createQuery(
				"Select h from HistoricosAntigos h where h.funcionario.id = :idFuncionario and h.dataInicio =:data",
				HistoricosAntigos.class);
		query.setParameter("idFuncionario", idFuncionario);
		query.setParameter("data", dataInicio);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public HistoricosAntigos historicoFuncionario(Long idFuncionario, Long idPrograma) {
		TypedQuery<HistoricosAntigos> query = manager.createQuery("Select h from HistoricosAntigos h where h.funcionario.id = :idFuncionario and h.programa.id = :idPrograma and h.ativo = false",
				HistoricosAntigos.class);
		query.setParameter("idFuncionario", idFuncionario);
		query.setParameter("idPrograma", idPrograma);
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
