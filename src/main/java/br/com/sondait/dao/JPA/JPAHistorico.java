package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOHistorico;
import br.com.sondait.model.Aprovado;
import br.com.sondait.model.Historico;

@Repository
public class JPAHistorico implements DAOHistorico {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Historico> buscarAtivos() {
		TypedQuery<Historico> query = manager.createQuery("Select h From Historico h where h.ativo = true",
				Historico.class);
		return query.getResultList();
	}

	@Override
	public Historico buscar(Long id) {
		return manager.find(Historico.class, id);

	}

	@Transactional
	@Override
	public void inserir(Historico obj) {
		manager.persist(obj);

	}

	@Transactional
	@Override
	public void alterar(Historico obj) {
		manager.merge(obj);

	}

	@Transactional
	@Override
	public void alterarHistorico(Historico obj) {
		if (obj.isAtivo() == false) {
			obj.setAtivo(true);
		}
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Long id) {
		Historico historico = manager.find(Historico.class, id);
		historico.setAtivo(true);
		manager.merge(historico);

	}

	@Transactional
	@Override
	public void remover(Long id) {
		Historico historico = manager.find(Historico.class, id);
		manager.remove(historico);
	}

	@Override
	public Historico historicoFuncionario(Long idFuncionario, Long idPrograma) {
		TypedQuery<Historico> query = manager.createQuery(
				"Select h from Historico h where h.funcionario.id = :idFuncionario and h.programa.id = :idPrograma",
				Historico.class);
		query.setParameter("idFuncionario", idFuncionario);
		query.setParameter("idPrograma", idPrograma);
	
		try {
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public List<Historico> historico(Long idFuncionario) {
		TypedQuery<Historico> query = manager
				.createQuery("Select h from Historico h where h.funcionario.id = :idFuncionario", Historico.class);
		query.setParameter("idFuncionario", idFuncionario);

		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<Historico> historicoPrograma(Long id) {
		TypedQuery<Historico> query = manager.createQuery("Select h from Historico h where h.programa.id = :id",
				Historico.class);
		query.setParameter("id", id);
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Historico> historicosPendentesPrograma(Long idPrograma) {
		TypedQuery<Historico> query = manager.createQuery(
				"Select h from Historico h where h.programa.id = :idPrograma and h.situacao = :situacao",
				Historico.class);
		query.setParameter("idPrograma", idPrograma);
		query.setParameter("situacao", Aprovado.PENDENTE);
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Historico> historicosExcluirFuncionario(Long idFuncionario, Long idOperacao) {
		TypedQuery<Historico> query = manager.createQuery(
				"Select h from Historico h where h.funcionario.id = :idFuncionario and h.programa.operacao.id = :idOperacao",
				Historico.class);
		query.setParameter("idFuncionario", idFuncionario);
		query.setParameter("idOperacao", idOperacao);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
