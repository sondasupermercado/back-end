package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOAvaliacaoReacao;
import br.com.sondait.model.AvaliacaoDeReacao;

@Repository
public class JPAAvaliacaoReacao implements DAOAvaliacaoReacao{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<AvaliacaoDeReacao> buscarAtivos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AvaliacaoDeReacao buscar(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transactional
	@Override
	public void inserir(AvaliacaoDeReacao obj) {
		manager.persist(obj);
		
	}

	@Override
	public void alterar(AvaliacaoDeReacao obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletar(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<AvaliacaoDeReacao> avaliacoesTurma(Long id) {
		TypedQuery<AvaliacaoDeReacao> query = manager.createQuery("Select a from AvaliacaoDeReacao a where a.turma.id =:id", AvaliacaoDeReacao.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
