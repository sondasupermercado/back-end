package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOChamada;
import br.com.sondait.model.Chamada;

@Repository
public class JPAChamada implements DAOChamada {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Chamada> buscarAtivos() {
		// Buscando a lista de chamadas no banco
		TypedQuery<Chamada> tq = manager.createQuery("FROM Chamada c WHERE c.ativo = true", Chamada.class);

		// Retornando a lista
		return tq.getResultList();
	}

	@Override
	public Chamada buscar(Long id) {
		// Buscando e retornando a chamada requisitada
		return manager.find(Chamada.class, id);
	}

	@Transactional
	@Override
	public void inserir(Chamada obj) {
		// Incluindo o objeto no banco
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Chamada obj) {
		// Busca e altera o objeto no banco
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Long id) {
		Chamada chamada = manager.find(Chamada.class, id);
		manager.remove(chamada);
	}

	@Override
	public List<Chamada> chamadasTurma(Long id) {
		TypedQuery<Chamada> query = manager.createQuery("Select c from Chamada c where c.turma.id = :id", Chamada.class);
		query.setParameter("id", id);
		
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}
}
