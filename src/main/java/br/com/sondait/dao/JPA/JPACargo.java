package br.com.sondait.dao.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.sondait.dao.DAOCargo;
import br.com.sondait.model.Cargo;

@Repository
public class JPACargo implements DAOCargo {

	/*
	 * Propriedades
	 */
	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<Cargo> buscarAtivos() {
		TypedQuery<Cargo> tq = manager.createQuery("Select c FROM Cargo c WHERE c.ativo = true", Cargo.class);
		return tq.getResultList();
	}

	@Override
	public Cargo buscar(Long id) {
		return manager.find(Cargo.class, id);
	}

	@Override
	@Transactional
	public void inserir(Cargo obj) {
		manager.persist(obj);
	}

	@Override
	@Transactional
	public void alterar(Cargo obj) {
		if (obj.isAtivo() == false) {
			obj.setAtivo(true);
		}		
			manager.merge(obj);
	}

	@Override
	@Transactional
	public void deletar(Long id) {
		Cargo cargo = manager.find(Cargo.class, id);
		cargo.setAtivo(false);
		manager.merge(cargo);
	}

	public Cargo buscarPeloNome(String nome) {
		TypedQuery<Cargo> query = manager.createQuery("Select c from Cargo c where c.nome =:nome and c.ativo = true",
				Cargo.class);
		query.setParameter("nome", nome);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Cargo buscarPeloNomeDesativo(String nome) {
		TypedQuery<Cargo> query = manager.createQuery("Select c from Cargo c where c.nome =:nome and c.ativo = false",
				Cargo.class);
		query.setParameter("nome", nome);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Cargo> buscarDesativos() {
		TypedQuery<Cargo> tq = manager.createQuery("FROM Cargo c WHERE c.ativo = false", Cargo.class);
		return tq.getResultList();
	}
	

	@Transactional
	@Override
	public void ativar(Long id) {
		Cargo cargo = manager.find(Cargo.class, id);
		cargo.setAtivo(true);
		manager.merge(cargo);
	}

	@Override
	public List<Cargo> buscarTodos() {
		TypedQuery<Cargo> tq = manager.createQuery("Select c FROM Cargo c", Cargo.class);
		return tq.getResultList();
	}
}
