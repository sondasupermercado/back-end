package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.Funcionario;
import br.com.sondait.model.RedefinirSenha;

public interface DAOFuncionario extends DAOGenerico<Funcionario> {
	
	public List<Funcionario> buscarDesativos();
	
	public List<Funcionario> buscarTodos();
	
	public Funcionario buscarEmail(String email);

	public Funcionario logar(Funcionario func);

	public void atualizandoToken(Funcionario func);

	public Funcionario emailCadastrado(String email);

	public void inserirCodigo(RedefinirSenha redefinir);

	public RedefinirSenha buscarCodigo(String email);

	public RedefinirSenha buscarCodigoRedefinir(String codigo);

	public void alterarCodigo(RedefinirSenha redefinir);

	public List<Funcionario> funcioriosCargo(Long id);

}
