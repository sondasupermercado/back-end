package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Programa;

public interface DAOPrograma extends DAOGenerico<Programa> {

	public List<Aplicacao> aplicacoesPrograma(Long id);

	public void alterarAplicacao(Aplicacao aplicacao);

	public List<Programa> buscarTodos(Long idFuncionario);
	
	public List<Programa> buscarTodos();
	
	public List<Programa> buscarDesativados();
	
	public List<Programa> funcionarioProgramaParticipa(Long id);
	
	public List<Programa> ProgramasDoCargo(Long id);
	
	public List<Programa> ProgramasDoFuncionarioporCargo(Long id);
	
	public List<Programa> funcionarioProgramaParticipaAtivoEDesativos(Long id);
	
	public List<Programa> funcionarioCoordena(Long id);
	
	public List<Programa> funcionarioCoordenaGeral(Long id);
	
	public List<Programa> BuscarProgramasParaTodos();
	
	public List<Programa> programasOperacao(Long id);
	
	public void remover (Programa programa);
	
	public List<Programa> programasReciclagem();
	
}
