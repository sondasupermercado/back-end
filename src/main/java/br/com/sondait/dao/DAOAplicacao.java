package br.com.sondait.dao;

import java.util.Date;
import java.util.List;

import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Programa;

public interface DAOAplicacao extends DAOGenerico<Aplicacao> {
	public List<Aplicacao> buscarTodos(Long idFuncionario);
	
	public List<Programa> funcionarioParticipa(Long id); 
	
	public List<Aplicacao> aplicacoesAprovadas(Long idFuncionario,Long idPrograma, Date dataInicio);
	
	public Aplicacao aplicacoesTreinamento (Long idFuncionario, Long idTreinamento);
	
	public List<Aplicacao> aplicacoesPrograma(Long idPrograma);
	
	public void remover(Long id);
	
	public List<Aplicacao> aplicacacoesFuncionario (Long id);
	
	public Aplicacao aplicacaoPorProgramaEFuncionarios(Long idPrograma, Long idFuncionario);
	
}
