package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.Treinamento;

public interface DAOTreinamento extends DAOGenerico<Treinamento> {
	
	public List<Treinamento> treinamentosReciclagem(Long id);
	
	public List<Treinamento> treinamentoPrograma(Long id);
}
