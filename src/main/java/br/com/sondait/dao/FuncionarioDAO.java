package br.com.sondait.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import java.sql.PreparedStatement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import br.com.sondait.model.Funcionario;

@Repository
public class FuncionarioDAO {
	private Connection conexao;

	@Autowired
	public FuncionarioDAO(DataSource dataSource) {
		try {
			this.conexao = dataSource.getConnection();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
	//Para tela de Alterar Opera��o
	public List<Funcionario> buscarFuncionariosDaOp(Long idOperacao) {
		List<Funcionario> funcionariosQueTemOp = new ArrayList<>();
		try {
			
			final String SQL_BUSCAR_OP_FUNC = "SELECT * FROM Funcionario f join Operacao_Funcionario op on f.id = op.funcionarios_id and op.Operacao_id <> ? and f.ativo = true order by f.nome asc";
			PreparedStatement smt = conexao.prepareStatement(SQL_BUSCAR_OP_FUNC);
			smt.setLong(1, idOperacao);
			ResultSet dadosDaTable = smt.executeQuery();
			
			
			
			while (dadosDaTable.next()) {
				Funcionario funcionario = new Funcionario();
				funcionario.setId(dadosDaTable.getLong("id"));
				funcionario.setNome(dadosDaTable.getString("nome"));
				funcionario.setEmail(dadosDaTable.getString("email"));

				funcionariosQueTemOp.add(funcionario);
			}
			smt.close();

			return funcionariosQueTemOp;
		} catch (Exception e) {
			return null;
		}
	}
	
	//Para tela de Criar Opera��o
	public List<Funcionario> buscarFuncionariosDaOp() {
		List<Funcionario> funcionariosQueTemOp = new ArrayList<>();
		try {
			
			final String SQL_BUSCAR_OP_FUNC = "SELECT * FROM Funcionario f join Operacao_Funcionario op on f.id = op.funcionarios_id and f.ativo = true  order by f.nome asc";
			PreparedStatement smt = conexao.prepareStatement(SQL_BUSCAR_OP_FUNC);
			ResultSet dadosDaTable = smt.executeQuery();
			
			
			
			while (dadosDaTable.next()) {
				Funcionario funcionario = new Funcionario();
				funcionario.setId(dadosDaTable.getLong("id"));
				funcionario.setNome(dadosDaTable.getString("nome"));
				funcionario.setEmail(dadosDaTable.getString("email"));

				funcionariosQueTemOp.add(funcionario);
			}
			smt.close();

			return funcionariosQueTemOp;
		} catch (Exception e) {
			return null;
		}
	}
	
}
