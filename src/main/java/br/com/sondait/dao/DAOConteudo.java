package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.Conteudo;
import br.com.sondait.model.Treinamento;

public interface DAOConteudo extends DAOGenerico<Conteudo> {

	public List<Conteudo> buscarTodosDesativos();
	
	public List<Conteudo> buscarTodos();
	
	public List<Treinamento> treinamentosConteudo(Long id);

}
