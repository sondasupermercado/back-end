package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.Historico;

public interface DAOHistorico extends DAOGenerico<Historico> {

	public Historico historicoFuncionario(Long idFuncionario, Long idPrograma);

	public List<Historico> historico(Long idFuncionario);

	public List<Historico> historicoPrograma(Long id);

	public void alterarHistorico(Historico obj);

	public void remover(Long id);

	public List<Historico> historicosPendentesPrograma(Long idPrograma);
	
	public List<Historico> historicosExcluirFuncionario(Long idFuncionario, Long idOperacao);

}
