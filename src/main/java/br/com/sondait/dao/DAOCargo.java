package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.Cargo;

public interface DAOCargo extends DAOGenerico<Cargo> {
	public Cargo buscarPeloNome(String nome);
	
	public Cargo buscarPeloNomeDesativo(String nome);
	
	public List<Cargo> buscarDesativos();
	
	public void ativar(Long id);
	
	
	public List<Cargo> buscarTodos();
}
