package br.com.sondait.dao;

import java.util.List;

public interface DAOGenerico<T> {

	public List<T> buscarAtivos();

	public T buscar(Long id);

	public void inserir(T obj);

	public void alterar(T obj);

	public void deletar(Long id);
}
