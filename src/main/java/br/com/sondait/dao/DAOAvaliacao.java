package br.com.sondait.dao;

import java.util.Date;
import java.util.List;

import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Avaliacao;

public interface DAOAvaliacao extends DAOGenerico<Avaliacao> {
	
	public List<Aplicacao> aplicacoesPrograma(Long id);
	
	public List<Avaliacao> buscarAtivosPorPrograma(Long id);
	
	public List<Avaliacao> buscarAtivosPorProgramasEFuncionario(Long idPrograma, Long idFuncionario);
	
	public Avaliacao avaliacaoEntreDatas(Date dataEficacia , Date dataReciclagem , Long idPrograma, Long idFuncionario);
	
	
}
