package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.AvaliacaoDeReacao;

public interface DAOAvaliacaoReacao extends DAOGenerico<AvaliacaoDeReacao> {
	
	public List<AvaliacaoDeReacao> avaliacoesTurma(Long id);

}
