package br.com.sondait.dao;

import java.util.Date;
import java.util.List;

import br.com.sondait.model.Aplicacao;
import br.com.sondait.util.Aprovacao;

public interface DAOAprovacao extends DAOGenerico<Aprovacao>{
	
	public List<Aplicacao> aplicacoesDataInicio(Date dataInicio, Long id);
	
	public Aprovacao registroAprovacao(Long idFuncionario, Long idPrograma);
	
}
