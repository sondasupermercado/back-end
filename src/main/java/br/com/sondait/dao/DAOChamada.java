package br.com.sondait.dao;

import java.util.List;

import br.com.sondait.model.Chamada;

public interface DAOChamada extends DAOGenerico<Chamada> {
	
	public List<Chamada> chamadasTurma(Long id);

}
