package br.com.sondait.util;

import java.util.Date;
import java.util.Set;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.sondait.model.Aplicacao;
import br.com.sondait.model.Funcionario;
import br.com.sondait.model.StatusAvaliacao;
//import br.com.sondait.model.StatusAvaliacao;

public class Aprovacao{
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataInicio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataConclusao;
	
	@JsonIgnoreProperties({"cargo","tipo","ativo","dataAdmissao"})
	private Funcionario funcionario;
	
	@JsonIgnoreProperties({"funcionario","nota","dataInicio","reciclagem","programa","conteudo"})
	private Set<Aplicacao> aplicacoes;
	
	private StatusAvaliacao status;

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(Date dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Set<Aplicacao> getAplicacoes() {
		return aplicacoes;
	}

	public void setAplicacoes(Set<Aplicacao> aplicacoes) {
		this.aplicacoes = aplicacoes;
	}

	public StatusAvaliacao getStatus() {
		return status;
	}

	public void setStatus(StatusAvaliacao status) {
		this.status = status;
	}

	
}
