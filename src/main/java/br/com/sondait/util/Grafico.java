package br.com.sondait.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.sondait.model.Operacao;
import br.com.sondait.model.Programa;

public class Grafico {
	
	@JsonIgnoreProperties({"coordenador", "funcionarios","ativo"})
	private Operacao operacao;
	private int quantFuncionarios;
	@JsonIgnoreProperties({"treinamentos", "duracao", "prazoReciclagem","prazoNotificacao", "prazoEficacia", "coordenador","operacao","funcionarios","dataInsercao","ativo"})
	private List<Programa> programas;
	
	public Operacao getOperacao() {
		return operacao;
	}
	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}
	public int getQuantFuncionarios() {
		return quantFuncionarios;
	}
	public void setQuantFuncionarios(int quantFuncionarios) {
		this.quantFuncionarios = quantFuncionarios;
	}
	public List<Programa> getProgramas() {
		return programas;
	}
	public void setProgramas(List<Programa> programas) {
		this.programas = programas;
	}
}
