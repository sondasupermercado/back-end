package br.com.sondait.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Avaliacao {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(optional = false)
	@JsonIgnoreProperties({ "prazoReciclagem", "prazoNotificacao", "prazoEficacia", "cargo", "operacao" })
	private Programa programa;

	@ManyToOne(optional = false)
	@JsonIgnoreProperties({ "tipo", "dataAdmissao" })
	private Funcionario funcionarioAvaliado;

	@ManyToOne(optional = false)
	@JsonIgnoreProperties({ "tipo", "dataAdmissao" })
	private Funcionario responsavelAvaliacao;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataAtual = new Date();

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataConclusao;


	@Column(nullable = false)
	@Min(value = 0)
	@Max(value = 10)
	private int notas1;
	
	@Column(nullable = false)
	@Min(value = 0)
	@Max(value = 10)
	private int notas2;
	
	@Column(nullable = false)
	@Min(value = 0)
	@Max(value = 10)
	private int notas3;

	@Column(nullable = false)
	private byte opcoesBaseAvaliacao;

	@Column
	@Lob
	private String comentariosBaseAvaliacao;

	@Column
	@Lob
	private String comentarios;

	@Column
	@Lob
	private String sugestoes;

	@Column
	private boolean ativo = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public Funcionario getFuncionarioAvaliado() {
		return funcionarioAvaliado;
	}

	public void setFuncionarioAvaliado(Funcionario funcionarioAvaliado) {
		this.funcionarioAvaliado = funcionarioAvaliado;
	}

	public Funcionario getResponsavelAvaliacao() {
		return responsavelAvaliacao;
	}

	public void setResponsavelAvaliacao(Funcionario responsavelAvaliacao) {
		this.responsavelAvaliacao = responsavelAvaliacao;
	}

	public Date getDataAtual() {
		return dataAtual;
	}

	public void setDataAtual(Date dataAtual) {
		this.dataAtual = dataAtual;
	}

	public Date getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(Date dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public int getNotas1() {
		return notas1;
	}

	public void setNotas1(int notas1) {
		this.notas1 = notas1;
	}

	public int getNotas2() {
		return notas2;
	}

	public void setNotas2(int notas2) {
		this.notas2 = notas2;
	}

	public int getNotas3() {
		return notas3;
	}

	public void setNotas3(int notas3) {
		this.notas3 = notas3;
	}

	public byte getOpcoesBaseAvaliacao() {
		return opcoesBaseAvaliacao;
	}

	public void setOpcoesBaseAvaliacao(byte opcoesBaseAvaliacao) {
		this.opcoesBaseAvaliacao = opcoesBaseAvaliacao;
	}

	public String getComentariosBaseAvaliacao() {
		return comentariosBaseAvaliacao;
	}

	public void setComentariosBaseAvaliacao(String comentariosBaseAvaliacao) {
		this.comentariosBaseAvaliacao = comentariosBaseAvaliacao;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getSugestoes() {
		return sugestoes;
	}

	public void setSugestoes(String sugestoes) {
		this.sugestoes = sugestoes;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
}
