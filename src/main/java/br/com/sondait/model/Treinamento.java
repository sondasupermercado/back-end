package br.com.sondait.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Transient;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude;
import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonInclude(Include.NON_NULL)
public class Treinamento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JsonIgnoreProperties({ "duracao", "prazoReciclagem", "prazoNotificacao", "prazoEficacia", "operacao", "cargo",
			"treinamentos", "avaliacoes", "funcionarios" })
	private Programa programa;

	@ManyToOne
	@JsonIgnoreProperties({ "ativo" })
	private Conteudo conteudo;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "treinamento")
	@JsonIgnoreProperties("treinamento")
	@OrderBy("nome")
	private Set<Turma> turmas;
	
	private boolean reciclagem;

	@Transient
	private Set<String> chamadas;

	public Set<String> getChamadas() {
		return chamadas;
	}

	public void setChamadas(Set<String> chamadas) {
		this.chamadas = chamadas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Programa getPrograma() {
		return programa;
	}

	public void setPrograma(Programa programa) {
		this.programa = programa;
	}

	public Conteudo getConteudo() {
		return conteudo;
	}

	public void setConteudo(Conteudo conteudo) {
		this.conteudo = conteudo;
	}

	public Set<Turma> getTurmas() {
		return turmas;
	}

	public void setTurmas(Set<Turma> turmas) {
		this.turmas = turmas;
	}
	
	public boolean isReciclagem() {
		return reciclagem;
	}

	public void setReciclagem(boolean reciclagem) {
		this.reciclagem = reciclagem;
	}
	
	

}
