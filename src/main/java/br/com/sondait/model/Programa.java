package br.com.sondait.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@JsonInclude(Include.NON_NULL)
public class Programa {

	public Programa(Long id, String nome) {
		this.id = id;
		this.nome = nome;
		this.dataInsercao = null;
	}

	public Programa() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	@NotBlank
	@Size(min = 1)
	private String nome;

	@JsonProperty(defaultValue="0")
	@Column(nullable = false)
	@Min(value = 1)
	private int duracao;
	
	@Column
	private int prazoReciclagem;

	@JsonProperty(defaultValue="0")
	@Column(nullable = false)
	@Min(value = 1)
	private int prazoNotificacao;
	
	@JsonProperty(defaultValue="0")
	@Column(nullable = false)
	@Min(value = 1)
	private int prazoEficacia;

	@ManyToOne
	@JsonIgnoreProperties({"coordenador" })
	private Operacao operacao;

	@ManyToOne
	private Cargo cargo;

	@OneToMany(mappedBy = "programa", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<Treinamento> treinamentos;

	@Column
	private boolean ativo = true;

	@ManyToOne
	@JsonIgnoreProperties({"email", "dataAdmissao", "tipo", "cargo" })
	private Funcionario coordenador;
	
	@JsonInclude(Include.NON_EMPTY)
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Funcionario> funcionarios = new HashSet<>();

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataInsercao = new Date();
	
	private boolean reciclagem;
	
	@Transient
	private int aprovados;
	
	@Transient
	private int reprovados;
	
	@Transient
	private int pendentes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	public int getPrazoReciclagem() {
		return prazoReciclagem;
	}

	public void setPrazoReciclagem(int prazoReciclagem) {
		this.prazoReciclagem = prazoReciclagem;
	}

	public int getPrazoNotificacao() {
		return prazoNotificacao;
	}

	public void setPrazoNotificacao(int prazoNotificacao) {
		this.prazoNotificacao = prazoNotificacao;
	}

	public int getPrazoEficacia() {
		return prazoEficacia;
	}

	public void setPrazoEficacia(int prazoEficacia) {
		this.prazoEficacia = prazoEficacia;
	}

	public Operacao getOperacao() {
		return operacao;
	}

	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Set<Treinamento> getTreinamentos() {
		return treinamentos;
	}

	public void setTreinamentos(Set<Treinamento> treinamentos) {
		this.treinamentos = treinamentos;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Funcionario getCoordenador() {
		return coordenador;
	}

	public void setCoordenador(Funcionario coordenador) {
		this.coordenador = coordenador;
	}

	public Set<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Set<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public Date getDataInsercao() {
		return dataInsercao;
	}

	public void setDataInsercao(Date dataInsercao) {
		this.dataInsercao = dataInsercao;
	}

	public boolean isReciclagem() {
		return reciclagem;
	}

	public void setReciclagem(boolean reciclagem) {
		this.reciclagem = reciclagem;
	}

	public int getAprovados() {
		return aprovados;
	}

	public void setAprovados(int aprovados) {
		this.aprovados = aprovados;
	}

	public int getReprovados() {
		return reprovados;
	}

	public void setReprovados(int reprovados) {
		this.reprovados = reprovados;
	}

	public int getPendentes() {
		return pendentes;
	}

	public void setPendentes(int pendentes) {
		this.pendentes = pendentes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Programa other = (Programa) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	

	
}
