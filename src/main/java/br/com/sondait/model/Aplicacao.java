package br.com.sondait.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.springframework.format.annotation.DateTimeFormat;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@JsonInclude(value = Include.NON_NULL)
@Table(name = "Aplicacao", uniqueConstraints = { @UniqueConstraint(columnNames = { "funcionario", "treinamento" }) })
public class Aplicacao {

	public Aplicacao() {

	}

	public Aplicacao(Long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JsonIgnoreProperties({ "dataAdmissao", "ativo", "tipo", "cargo" })
	@JoinColumn(name = "funcionario")
	private Funcionario funcionario;

	@Column
	private Aprovado aprovado = Aprovado.PENDENTE;

	@Column
	private byte nota;

	@ManyToOne
	@JsonIgnoreProperties("turmas")
	@JoinColumn(name = "treinamento")
	private Treinamento treinamento;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date dataInicio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Aprovado getAprovado() {
		return aprovado;
	}

	public void setAprovado(Aprovado aprovado) {
		this.aprovado = aprovado;
	}

	public byte getNota() {
		return nota;
	}

	public void setNota(byte nota) {
		this.nota = nota;
	}

	public Treinamento getTreinamento() {
		return treinamento;
	}

	public void setTreinamento(Treinamento treinamento) {
		this.treinamento = treinamento;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
}
