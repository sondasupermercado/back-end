package br.com.sondait.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@JsonInclude(Include.NON_NULL)
public class Operacao {

	public Operacao(Long id, String nome, Funcionario coordenador) {
		this.id = id;
		this.nome = nome;
		this.coordenador = coordenador;
	}

	public Operacao() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 64, nullable = false, unique = true)
	@Size(min = 2, max = 64)
	@NotBlank
	private String nome;

	@Column
	private boolean ativo = true;

	@Valid
	@ManyToOne(optional = true)
	@JsonIgnoreProperties({ "dataAdmissao", "ativo", "tipo", "cargo"})
	private Funcionario coordenador;

	@JsonInclude(Include.NON_EMPTY)
	@ManyToMany(fetch = FetchType.EAGER)
	@JsonIgnoreProperties({ "dataAdmissao", "ativo", "tipo", "cargo"})
	private Set<Funcionario> funcionarios = new HashSet<>();
	
	@Transient
	@JsonIgnoreProperties({"treinamentos","aprovados","reprovados","pendentes","coordenador", "duracao","prazoReciclagem", "prazoNotificacao","prazoEficacia","operacao", "funcionarios","ativo","dataInsercao"})
	private Set<Programa> programasOperacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Funcionario getCoordenador() {
		return coordenador;
	}

	public void setCoordenador(Funcionario coordenador) {
		this.coordenador = coordenador;
	}

	public Set<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Set<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	
	public boolean containsFuncionario(Long idFuncionario){
		if(funcionarios != null){			
			return funcionarios.stream().anyMatch(func -> func.getId() == idFuncionario);
		}
		return false;
	}

	public Set<Programa> getProgramasOperacao() {
		return programasOperacao;
	}

	public void setProgramasOperacao(Set<Programa> programasOperacao) {
		this.programasOperacao = programasOperacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operacao other = (Operacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
